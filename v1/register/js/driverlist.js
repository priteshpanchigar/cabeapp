var searchVal = '';
$(document).ready(function() {
    getDriverList();

});

function getDriverList() {
    document.getElementById('driverList').innerHTML = "";
    var parameter = '';
    searchVal = document.getElementById('value').value;
    if (searchVal !== '')
        parameter = 'searchvalue=' + searchVal;
    var getDriverListUrl = getPhpScriptsPath() +
        'get_driver.php?' + parameter;

    $.ajax({
        url: getDriverListUrl,
        type: "GET",
        success: function(data) {
            var jsonData = JSON.parse(data);
            var sHTML = '';   
            for (i = 0; i < jsonData.length; i = i + 1) {
                var row = jsonData[i];

                sHTML = sHTML +
                    '<div class="panel panel-info" onclick=""><div class="panel-heading"><label>' +
                    row.driver_firstname + ' ' + row.driver_lastname +
                    '</label></div>' +
                    '<a href="' + getHtmlScriptsPath() + 'driverregister.html?phoneno=' + row.driver_phoneno + '" style="text-decoration: none"><div style="margin:5px 5px 5px 5px;" >' +
                    'Phone no  ' + row.driver_phoneno +
                    '<br/>Unique Id  ' + row.unique_id +
                    '<br/>License No  ' + row.driver_license_no + '</div></a></div>';
            }
            if (data == -1) {
                var sCriteria = "";

                sHTML = sHTML + "No list found. Please try again";
            }
            $('#driverList').append(sHTML);

        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}