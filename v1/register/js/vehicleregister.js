 


function addVehicle( successCallBack) {
	
	var vehicleno_state = document.getElementById("vehicleno_state").value;
	 if (vehicleno_state === '') {
        alert("Vehicle state is required");
        return false;
    }
	var vehicleno_city = document.getElementById("vehicleno_city").value;
	if (vehicleno_city === '') {
        alert("Vehicle city is required");
        return false;
    }
	var vehicleno_code = document.getElementById("vehicleno_code").value;
	if (vehicleno_code === '') {
        alert("Vehicle code is required");
        return false;
    }
	var vehicle_no = document.getElementById("vehicle_no").value;
	if (vehicle_no === '') {
        alert("Vehicle no is required");
        return false;
    }
	var vehicle_company = document.getElementById("vehicle_company").value;
	if (vehicle_company === '') {
        alert("Vehicle company is required");
        return false;
    }  
	/*var vehicle_category = document.getElementById("vehicle_category").value;
	if (vehicle_category === '') {
        alert("Vehicle category is required");
        return false;
    }*/
	var vehicle_model = document.getElementById("vehicle_model").value;
	if (vehicle_model === '') {
        alert("Vehicle model is required");
        return false;
    }
	var vehicle_type = document.getElementById("vehicle_type").value;
	if (vehicle_type === '') {
        alert("Vehicle type is required");
        return false;
    }
	var vehicle_color = document.getElementById("vehicle_color").value;
	if (vehicle_color === '') {
        alert("Vehicle color is required");
        return false;
    }
	var servicecode = document.getElementById("servicecode").value;
	if (servicecode === '') {
        alert("Vehicle service code is required");
        return false;
    }
	
	var uniqueid = document.getElementById("uniqueid").value;
	if (uniqueid === '') {
        alert("Vehicle Uniqueid is required");
        return false;
    }
	var selection='';
	var vehicleownership =  document.getElementsByName("driveraccess");
	for (var i = 0; i < vehicleownership.length; i++) {
        if (vehicleownership[i].checked == true) {
            selection = vehicleownership[i].value;
        }
    }
    if (selection === '') {
        alert("Driver owns the car?");
        return false;
    }
  
	var addVehicleUrl = getPhpScriptsPath() + 
        'insert_driver_vehicle_data.php?uniqueid=' + uniqueid + '&vehiclenostate='+vehicleno_state
		+'&vehiclenocity='+ vehicleno_city +'&vehiclenocode='+ vehicleno_code
		+'&vehicleno='+ vehicle_no +'&vehiclecompany='+vehicle_company
		+'&vehiclemodel='+vehicle_model+'&vehicletype='+ vehicle_type
		+ '&vehiclecolor='+vehicle_color + '&servicecode='+servicecode
		+'&vehicleownership='+selection;
    $.ajax({
        type: 'GET',
        url: addVehicleUrl,
        dataType: 'json',  
        success: function(data) {
			alert("Registration done");
               window.location.reload();
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
        
    }); 
}

function getDriverInfo() {
    var uniqueid = document.getElementById("uniqueid").value;
	if (uniqueid === '') {
        alert("Vehicle Uniqueid is required");
        return false;
    }
  
	var getDriverUrl = getPhpScriptsPath() + 
        'get_driver.php?uniqueid=' + uniqueid;

    $.ajax({
        url: getDriverUrl,
        type: "GET",
        success: function(data) {
			if(data ==="-1")
				{       
					document.getElementById('driverinfo').innerHTML = 'Driver not found';
					return false;   
				}  
            var jsonData = JSON.parse(data);
            for (i = 0; i < jsonData.length; i = i + 1) {
                var row = jsonData[i];
               
                var sHtml = 'Driver Name: '+row.driver_firstname+' '+ row.driver_lastname+'<br/>';
				sHtml = sHtml +'Driver Phone Number: '+row.driver_phoneno+'<br/>';
				sHtml = sHtml +'Driver License Number: '+row.driver_license_no+'<br/>';
				sHtml = sHtml + ' Driver Unique ID: '+row.unique_id;
				document.getElementById('driverinfo').innerHTML = sHtml;
                
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}