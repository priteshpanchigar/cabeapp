var driver_profile_id = '';
var phoneno = '';
var driver_image_name = '';
var driver_image_string = '';
var pan_image_name = '';
var pan_image_string = '';
var aadhar_image_name = '';
var aadhar_image_string = '';
var license_image_name = '';
var license_image_string = '';
var clientdatetime = '';
var fileChooser = document.getElementById('uploadDriver');
var content = document.getElementById('content');
var fileChooserPan = document.getElementById('uploadPan');
var contentPan = document.getElementById('contentPan');
var fileChooserAadhar = document.getElementById('uploadAadhar');
var contentAadhar = document.getElementById('contentAadhar');
var fileChooserLicence = document.getElementById('uploadLicence');
var contentLicence = document.getElementById('contentLicence');
var uniquekey = '';
var uniqueid = '';
var selectedGender = '';
var selectedPermit = '';
var selectedBadge = '';
var selectedStatus = '';
var selectedCab = '';
var phnNo='';
var driverlist;
$(document).ready(function() {

    phnNo = getParameter("phoneno") ? getParameter("phoneno") : '';
    if (phnNo !== '') {
        getDriverData(phnNo);
    }
	driverlist =  document.getElementById('driverlist');
	driverlist.href = getHtmlScriptsPath()+"driverlist.html";
});

function getParameter(theParameter) {
    var params = window.location.search.substr(1).split('&');

    for (var i = 0; i < params.length; i++) {
        var p = params[i].split('=');
        if (p[0] == theParameter) {
            return decodeURIComponent(p[1]);
        }
    }
    return false;
}
if (typeof window.FileReader === 'undefined') {
    content.className = 'fail';
    content.innerHTML = 'File API &amp; FileReader API are not supported in your browser.  Try on a new-ish Android phone.';
}
if (typeof window.FileReader === 'undefined') {
    contentPan.className = 'fail';
    contentPan.innerHTML = 'File API &amp; FileReader API are not supported in your browser.  Try on a new-ish Android phone.';
}
if (typeof window.FileReader === 'undefined') {
    contentAadhar.className = 'fail';
    contentAadhar.innerHTML = 'File API &amp; FileReader API are not supported in your browser.  Try on a new-ish Android phone.';
}
if (typeof window.FileReader === 'undefined') {
    contentLicence.className = 'fail';
    contentLicence.innerHTML = 'File API &amp; FileReader API are not supported in your browser.  Try on a new-ish Android phone.';
}
fileChooser.onchange = function(e) {
    //e.preventDefault();

    var max_width = 180;
    var max_height = 200;
    var file = fileChooser.files[0],
        reader = new FileReader();
    driver_image_name = file.name;
    reader.onerror = function(event) {
        content.innerHTML = "Error reading file";
    }
    reader.onload = function(event) {
        var img = new Image();
        // files from the Gallery need the URL adjusted
        if (event.target.result && event.target.result.match(/^data:base64/)) {
            img.src = event.target.result.replace(/^data:base64/, 'data:image/jpeg;base64');

        } else {
            img.src = event.target.result;

        }
        img.height = 150;
        img.width = 180;
        content.innerHTML = '';
        content.appendChild(img);
        img.onload = function() {

            var canvas = document.createElement('canvas');

            var width = img.width;
            var height = img.height;

            // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                    //height *= max_width / width;
                    height = Math.round(height *= max_width / width);
                    width = max_width;
                }
            } else {
                if (height > max_height) {
                    //width *= max_height / height;
                    width = Math.round(width *= max_height / height);
                    height = max_height;
                }
            }

            // resize the canvas and draw the image data into it
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);
            content.innerHTML = '';
            content.appendChild(canvas);
            driver_image_string = canvas.toDataURL('image/png');
        };
        var processExif = function(exifObject) {
            console.log(exifObject);
        }
        try {
            processExif(img);
        } catch (e) {
            alert(e);
        }
    };
    reader.readAsDataURL(file);

    return false;
}



fileChooserPan.onchange = function(e) {
    //e.preventDefault();
    var max_width = 180;
    var max_height = 200;
    var file = fileChooserPan.files[0],
        reader = new FileReader();
    pan_image_name = file.name;

    reader.onerror = function(event) {
        contentPan.innerHTML = "Error reading file";
    }

    reader.onload = function(event) {
        var img = new Image();

        // files from the Gallery need the URL adjusted
        if (event.target.result && event.target.result.match(/^data:base64/)) {
            img.src = event.target.result.replace(/^data:base64/, 'data:image/jpeg;base64');
        } else {
            img.src = event.target.result;
        }
        img.height = 150;
        img.width = 180;
        contentPan.innerHTML = '';
        contentPan.appendChild(img);
        img.onload = function() {

            var canvas = document.createElement('canvas');

            var width = img.width;
            var height = img.height;

            // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                    //height *= max_width / width;
                    height = Math.round(height *= max_width / width);
                    width = max_width;
                }
            } else {
                if (height > max_height) {
                    //width *= max_height / height;
                    width = Math.round(width *= max_height / height);
                    height = max_height;
                }
            }

            // resize the canvas and draw the image data into it
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);
            contentPan.innerHTML = '';
            contentPan.appendChild(canvas);
            pan_image_string = canvas.toDataURL('image/png');
        };
        var processExif = function(exifObject) {

            console.log(exifObject);

        }
        try {
            processExif(img);
        } catch (e) {
            alert(e);
        }

    };
    reader.readAsDataURL(file);
    return false;
}

fileChooserAadhar.onchange = function(e) {
    //e.preventDefault();
    var max_width = 180;
    var max_height = 200;
    var file = fileChooserAadhar.files[0],
        reader = new FileReader();
    aadhar_image_name = file.name;

    reader.onerror = function(event) {
        contentAadhar.innerHTML = "Error reading file";
    }

    reader.onload = function(event) {
        var img = new Image();

        // files from the Gallery need the URL adjusted
        if (event.target.result && event.target.result.match(/^data:base64/)) {
            img.src = event.target.result.replace(/^data:base64/, 'data:image/jpeg;base64');
        } else {
            img.src = event.target.result;
        }
        img.height = 150;
        img.width = 180;

        contentAadhar.innerHTML = '';
        contentAadhar.appendChild(img);
        img.onload = function() {

            var canvas = document.createElement('canvas');

            var width = img.width;
            var height = img.height;

            // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                    //height *= max_width / width;
                    height = Math.round(height *= max_width / width);
                    width = max_width;
                }
            } else {
                if (height > max_height) {
                    //width *= max_height / height;
                    width = Math.round(width *= max_height / height);
                    height = max_height;
                }
            }

            // resize the canvas and draw the image data into it
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);
            contentAadhar.innerHTML = '';
            contentAadhar.appendChild(canvas);
            aadhar_image_string = canvas.toDataURL('image/png');
        };
        var processExif = function(exifObject) {

            console.log(exifObject);
        }

        try {
            processExif(img);
        } catch (e) {
            alert(e);
        }


    };

    reader.readAsDataURL(file);

    return false;
}

fileChooserLicence.onchange = function(e) {
    //e.preventDefault();
    var max_width = 180;
    var max_height = 200;
    var file = fileChooserLicence.files[0],
        reader = new FileReader(),
        binaryReader = new FileReader();
    license_image_name = file.name;

    reader.onerror = function(event) {
        contentLicence.innerHTML = "Error reading file";
    }

    reader.onload = function(event) {
        var img = new Image();

        // files from the Gallery need the URL adjusted
        if (event.target.result && event.target.result.match(/^data:base64/)) {
            img.src = event.target.result.replace(/^data:base64/, 'data:image/jpeg;base64');
        } else {
            img.src = event.target.result;
        }
        img.height = 150;
        img.width = 180;

        contentLicence.innerHTML = '';
        contentLicence.appendChild(img);
        img.onload = function() {

            var canvas = document.createElement('canvas');

            var width = img.width;
            var height = img.height;

            // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                    //height *= max_width / width;
                    height = Math.round(height *= max_width / width);
                    width = max_width;
                }
            } else {
                if (height > max_height) {
                    //width *= max_height / height;
                    width = Math.round(width *= max_height / height);
                    height = max_height;
                }
            }

            // resize the canvas and draw the image data into it
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);
            contentLicence.innerHTML = '';
            contentLicence.appendChild(canvas);
            license_image_string = canvas.toDataURL('image/png');
        };
        var processExif = function(exifObject) {

            console.log(exifObject);

        }

        try {
            processExif(img);
        } catch (e) {
            alert(e);
        }
    };

    reader.readAsDataURL(file);
    return false;
}

function getDriverData(phnNo) {
    var getDriverUrl = getPhpScriptsPath() +
        'get_driver.php?driverphoneno=' + phnNo;

    $.ajax({
        url: getDriverUrl,
        type: "GET",
        success: function(data) {
            var jsonData = JSON.parse(data);
            for (i = 0; i < jsonData.length; i = i + 1) {
                var row = jsonData[i];
                driver_profile_id = row.driver_profile_id;
                uniquekey = row.unique_key;
                clientdatetime = row.clientdatetime;
                uniqueid = row.unique_id;
                var dob = row.driver_dob;
                var date = dob.split("-");
                var day = date[2];
                dob = date[1] + '/' + date[2] + '/' + date[0];
                document.getElementById('dob').value = dob;
                document.getElementById('driverfirstname').value = row.driver_firstname;
                document.getElementById('driverlastname').value = row.driver_lastname;
                document.getElementById('emailid').value = row.driver_email;
                document.getElementById('phoneno').value = row.driver_phoneno;
                document.getElementById('taddrone').value = row.t_address_1;
                document.getElementById('taddrtwo').value = row.t_address_2;
                document.getElementById('tcity').value = row.t_city_town;
                document.getElementById('tstate').value = row.t_state;
                document.getElementById('tpincode').value = row.t_pincode;
                document.getElementById('tlandmark').value = row.t_landmark;
                document.getElementById('paddrone').value = row.p_address_1;
                document.getElementById('paddrtwo').value = row.p_address_2;
                document.getElementById('pcity').value = row.p_city_town;
                document.getElementById('pstate').value = row.p_state;
                document.getElementById('ppincode').value = row.p_pincode;  
                document.getElementById('plandmark').value = row.p_landmark;
                document.getElementById('vehicle').value = row.driver_vehicle_no;
                document.getElementById('amount').value = row.payment_amount;
                document.getElementById('receiptno').value = row.receipt_no;
                document.getElementById('remark').value = row.remark;
                document.getElementById('comment').value = row.internal_comment;
                document.getElementById('permitno').value = row.driver_permit_no;
                document.getElementById('badgeno').value = row.driver_badge_no;
                document.getElementById('year').value = row.driver_incity_since;
                document.getElementById('pan').value = row.driver_pancard;
                document.getElementById('aadhar').value = row.driver_aadhar;
                document.getElementById('licence').value = row.driver_license_no;
                selectedGender = row.driver_gender;
                if (selectedGender === 'Male') {
                    document.getElementById('male').checked = true;
                } else {
                    document.getElementById('female').checked = true;
                }
                selectedPermit = row.driver_permit_flag;
                if (selectedPermit === 'Y') {
                    document.getElementById('permitY').checked = true;
                    document.getElementById('divPermit').setAttribute('style', 'display:block');
                } else {
                    document.getElementById('permitN').checked = true;
                    document.getElementById('divPermit').setAttribute('style', 'display:none');
                }
                selectedBadge = row.driver_badge_flag;
                if (selectedBadge === 'Y') {
                    document.getElementById('badgeY').checked = true;
                    document.getElementById('divBadgeNo').setAttribute('style', 'display:block');
                    document.getElementById('divBadgeYear').setAttribute('style', 'display:none');
                } else {
                    document.getElementById('badgeN').checked = true;
                    document.getElementById('divBadgeNo').setAttribute('style', 'display:none');
                    document.getElementById('divBadgeYear').setAttribute('style', 'display:block');
                }
                selectedCab = row.cab_needed_flag;  
                if (selectedCab === 'Y') {
                    document.getElementById('cabY').checked = true;
                } else {
                    document.getElementById('cabN').checked = true;
                }
                selectedStatus = row.payment_status
                if (selectedStatus === '1') {
                    document.getElementById('paidY').checked = true;
                } else {
                    document.getElementById('paidN').checked = true;
                }
                var receiptdate = row.receipt_date;
                if (receiptdate) {
                    var dateR = receiptdate.split("-");
                    var day = dateR[2];
                    var iday = day.split(" ");
                    receiptdate = dateR[1] + '/' + iday[0] + '/' + dateR[0];
                    document.getElementById("receiptdate").value = receiptdate
                }
                var driver_image_name = row.driver_image_name;
                if (driver_image_name !== '' && driver_image_name !== null) {
                    var img = new Image();
                    img.widht = 150;
                    img.height = 180;
                    img.src = getImageScriptsPath() + row.driver_image_name;
                    content.innerHTML = '';
                    content.appendChild(img);
                }
                var pan_image_name = row.pan_image_name;
                if (pan_image_name !== '' && pan_image_name !== null) {
                    var img = new Image();
                    img.widht = 150;
                    img.height = 180;
                    img.src = getImageScriptsPath() + pan_image_name;
                    contentPan.innerHTML = '';
                    contentPan.appendChild(img);
                }
                var aadhar_image_name = row.aadhar_image_name;
                if (aadhar_image_name !== '' && aadhar_image_name !== null) {
                    var img = new Image();
                    img.widht = 150;
                    img.height = 180;
                    img.src = getImageScriptsPath() + aadhar_image_name;
                    contentAadhar.innerHTML = '';
                    contentAadhar.appendChild(img);
                }
                var license_image_name = row.license_image_name;
                if (license_image_name !== '' && license_image_name !== null) {
                    var img = new Image();
                    img.widht = 150;
                    img.height = 180;
                    img.src = getImageScriptsPath() + license_image_name;
                    contentLicence.innerHTML = '';
                    contentLicence.appendChild(img);
                }
                getOtherCompanyMember(driver_profile_id);
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}

function getOtherCompanyMember(driver_profile_id) {
    var getOtherCompanyMemberUrl = getPhpScriptsPath() +
        'get_other_company_member.php?driverprofileid=' + driver_profile_id;
    $.ajax({
        type: 'GET',
        url: getOtherCompanyMemberUrl,
        success: function(data) {
            var jsonData = JSON.parse(data);
            for (i = 0; i < jsonData.length; i = i + 1) {
                var row = jsonData[i];
                if (row.other_company_code === 'OLA') {
                    document.getElementById('chkOla').checked = true;
                    document.getElementById('olaid').value = row.driver_other_company_id;
                }

                if (row.other_company_code === 'UBR') {
                    document.getElementById('chkUbr').checked = true;
                    document.getElementById('ubrid').value = row.driver_other_company_id;
                }
                if (row.other_company_code === 'MRU') {
                    document.getElementById('chkMru').checked = true;
                    document.getElementById('mruid').value = row.driver_other_company_id;
                }
                if (row.other_company_code === 'BMC') {
                    document.getElementById('chkBmc').checked = true;
                    document.getElementById('bmcid').value = row.driver_other_company_id;
                }
                if (row.other_company_code === 'ECA') {
                    document.getElementById('chkEca').checked = true;
                    document.getElementById('ecaid').value = row.driver_other_company_id;
                }
                if (row.other_company_code === 'TAB') {
                    document.getElementById('chkTab').checked = true;
                    document.getElementById('tabid').value = row.driver_other_company_id;
                }
                if (row.other_company_code === 'TFS') {
                    document.getElementById('chkTfs').checked = true;
                    document.getElementById('tfsid').value = row.driver_other_company_id;
                }
                if (row.other_company_code === 'VIR') {
                    document.getElementById('chkVir').checked = true;
                    document.getElementById('virid').value = row.driver_other_company_id;
                }
                if (row.other_company_code === 'PRI') {
                    document.getElementById('chkPri').checked = true;
                    document.getElementById('priid').value = row.driver_other_company_id;
                }

                if (row.other_company_code === 'ARY') {
                    document.getElementById('chkAry').checked = true;
                    document.getElementById('aryid').value = row.driver_other_company_id;
                }

                if (row.other_company_code === 'BNY') {
                    document.getElementById('chkBny').checked = true;
                    document.getElementById('bnyid').value = row.driver_other_company_id;
                }
            }
        }

    });
}

function addDriver() {

    var d = new Date();
    if (uniquekey === '') {
        uniquekey = d.getTime();
    }
    if (clientdatetime === '' ) {
        clientdatetime = d.getFullYear() + "-" +
            (d.getMonth() + 1) + "-" +
            d.getDate() + " " +
            d.getHours() + ":" +
            d.getMinutes() + ":" +
            d.getSeconds();
    }
    var driverfirstname = document.getElementById("driverfirstname").value;
    if (driverfirstname === '') {
        alert("Driver First Name is required");
        return false;
    }
    var driverlastname = document.getElementById("driverlastname").value;
    if (driverlastname === '') {
        alert("Driver last Name is required");
        return false;
    }
    var dob = document.getElementById("dob").value;
    var date = dob.split("/");
    dob = date[2] + '-' + date[0] + '-' + date[1];


    if (dob === '') {
        alert("Driver Birth Date is required");
        return false;
    }
    phoneno = document.getElementById("phoneno").value;
    if (phoneno === '') {
        alert("Driver Phone Number is required");
        return false;
    }
    if (uniqueid === '' ) {
        uniqueid = driverfirstname.charAt(0).toUpperCase() + driverlastname.charAt(0).toUpperCase() + "_" +
            (("0" + (d.getMonth() + 1)).slice(-2)) + "_" + d.getFullYear() + "_" + phoneno;
    }
    var taddrone = document.getElementById("taddrone").value;
    var taddrtwo = document.getElementById("taddrtwo").value;
    var tcity = document.getElementById("tcity").value;
    var tstate = document.getElementById("tstate").value;
    var tpincode = document.getElementById("tpincode").value;
    var tlandmark = document.getElementById("tlandmark").value;
    var paddrone = document.getElementById("paddrone").value;
    if (paddrone === '') {
        alert("Permenant address is required");
        return false;
    }
    var paddrtwo = document.getElementById("paddrtwo").value;
    if (paddrtwo === '') {
        alert("Permenant address is required");
        return false;
    }
    var pcity = document.getElementById("pcity").value;
    if (pcity === '') {
        alert("Permenant city is required");
        return false;
    }
    var pstate = document.getElementById("pstate").value;
    if (pstate === '') {
        alert("Permenant state is required");
        return false;
    }
    var ppincode = document.getElementById("ppincode").value;
    if (ppincode === '') {
        alert("Permenant pincode is required");
        return false;
    }
    var plandmark = document.getElementById("plandmark").value;
    var genders = document.getElementsByName("gender");

    for (var i = 0; i < genders.length; i++) {
        if (genders[i].checked == true) {
            selectedGender = genders[i].value;
        }
    }
    if (selectedGender === '') {
        alert("Gender is required");
        return false;
    }


    var emailid = document.getElementById("emailid").value;
    var vehicle = document.getElementById("vehicle").value;
    var permit = document.getElementsByName("permit");

    for (var i = 0; i < permit.length; i++) {
        if (permit[i].checked == true) {
            selectedPermit = permit[i].value;
        }
    }
    if (selectedPermit === '') {
        alert("Permit Flag is required");
        return false;
    }

    var permitno = document.getElementById("permitno").value;
    if (selectedPermit === 'Y' && permitno === '') {
        alert("Permit Number is required");
        return false;
    }
    var badge = document.getElementsByName("badge");

    for (var i = 0; i < badge.length; i++) {
        if (badge[i].checked == true) {
            selectedBadge = badge[i].value;
        }
    }
    if (selectedBadge === '') {
        alert("Badge Flag is required");
        return false;
    }
    var badgeno = document.getElementById("badgeno").value;
    if (selectedBadge === 'Y' && badgeno === '') {
        alert("Badge Number is required");
        return false;
    }

    var year = document.getElementById("year").value;
    if (selectedBadge === 'N' && year === '') {
        alert("Year is required");
        return false;
    }
    var cab = document.getElementsByName("cab");

    for (var i = 0; i < cab.length; i++) {
        if (cab[i].checked == true) {
            selectedCab = cab[i].value;
        }
    }
    if (selectedBadge === '') {
        alert("Is Cab requird or not?");
        return false;
    }
    var remark = document.getElementById("remark").value;
    var comment = document.getElementById("comment").value;
    var pan = document.getElementById("pan").value;
    var aadhar = document.getElementById("aadhar").value;
    var licence = document.getElementById("licence").value;
    var paymentstatus = document.getElementsByName("status");

    for (var i = 0; i < paymentstatus.length; i++) {
        if (paymentstatus[i].checked == true) {
            selectedStatus = paymentstatus[i].value;
        }
    }
    if (selectedStatus === '') {
        alert("Payment status is required");
        return false;
    }
    var paymentamount = document.getElementById("amount").value;
    var receiptno = document.getElementById("receiptno").value;
    var receiptdate = document.getElementById("receiptdate").value;
    if (receiptdate !== '' && receiptdate !== "undefined"){
    var dateR = receiptdate.split("/");
    receiptdate = dateR[2] + '-' + dateR[0] + '-' + dateR[1];   
}
if (driver_image_string === '' && phnNo === '') {
    alert("Driver Image required");   
    return false;
}
if (license_image_string === '' && phnNo === '') {
    alert("Licence Image required");
    return false;
}
if (licence === '') {
    alert("Licence Number required");
    return false;
}

var insertDriverProfileUrl = getPhpScriptsPath() +
    'insert_driver_profile_data.php?driverfirstname=' + driverfirstname +
    '&driverlastname=' + driverlastname + '&driverdob=' + dob +
    '&taddress1=' + taddrone + '&taddress2=' + taddrtwo +
    '&tcitytown=' + tcity + '&tstate=' + tstate + '&tpincode=' + tpincode +
    '&tlandmark=' + tlandmark + '&paddress1=' + paddrone + '&paddress2=' + paddrtwo +
    '&pcitytown=' + pcity + '&pstate=' + pstate + '&ppincode=' + ppincode +
    '&plandmark=' + plandmark + '&drivergender=' + selectedGender + '&driverphoneno=' + phoneno +
    '&driveremail=' + emailid + '&drivervehicleno=' + vehicle + '&driverpermiflag=' + selectedPermit +
    '&driverpermitno=' + permitno + '&driverbadgeflag=' + selectedBadge + '&driverbadgeno=' + badgeno +
    '&driverincitysince=' + year + '&cabneededflag=' + selectedCab + '&remark=' + remark + '&internalcomment=' + comment +
    '&driverpancard=' + pan + '&driveraadhar=' + aadhar + '&driverlicenseno=' + licence +
    '&uniquekey=' + uniquekey + '&paymentstatus=' + selectedStatus + '&paymentamount=' + paymentamount +
    '&receiptno=' + receiptno + '&receiptdate=' + receiptdate + '&clientdatetime=' + clientdatetime +
    '&uniqueid=' + uniqueid;
var data = [];
for (var i = 0; i < 100000; i++) {
    var tmp = [];
    for (var i = 0; i < 100000; i++) {
        tmp[i] = 'hue';
    }
    data[i] = tmp;
};
$.ajax({
    xhr: function() {
        var xhr = new window.XMLHttpRequest();
        xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                console.log(percentComplete);
                $('.progress').css({
                    width: percentComplete * 100 + '%'
                });
                if (percentComplete === 1) {
                    $('.progress').addClass('hide');
                }
            }
        }, false);
        xhr.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                console.log(percentComplete);
                $('.progress').css({
                    width: percentComplete * 100 + '%'
                });
            }
        }, false);
        return xhr;
    },
    type: 'GET',
    url: insertDriverProfileUrl,
    dataType: 'json',
    success: function(data) {
        driver_profile_id = data.driver_profile_id;
        phoneno = data.driver_phoneno;
        
        sendDriverImage();
        sendPanCardImage();
        sendAadharImage();
        sendLicenseImage();
        var olaJson = '';
        if (document.getElementById('chkOla').checked) {
            var id = document.getElementById('olaid').value;
            olaJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + id + '","other_company_code":"OLA"}';
        }
        var ubrJson = '';
        if (document.getElementById('chkUbr').checked) {
            var id = document.getElementById('ubrid').value;
            ubrJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"UBR"}';
        }
        var mruJson = '';
        if (document.getElementById('chkMru').checked) {
            var id = document.getElementById('mruid').value;
            mruJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"MRU"}';
        }
        var bmcJson = '';
        if (document.getElementById('chkBmc').checked) {
            var id = document.getElementById('bmcid').value;
            bmcJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"BMC"}';
        }
        var ecaJson = '';
        if (document.getElementById('chkEca').checked) {
            var id = document.getElementById('ecaid').value;
            ecaJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"ECA"}';
        }
        var tabJson = '';
        if (document.getElementById('chkTab').checked) {
            var id = document.getElementById('tabid').value;
            tabJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"TAB"}';
        }
        var tfsJson = '';
        if (document.getElementById('chkTfs').checked) {
            var id = document.getElementById('tfsid').value;
            tfsJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"TFS"}';
        }
        var virJson = '';
        if (document.getElementById('chkVir').checked) {
            var id = document.getElementById('virid').value;
            virJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"VIR"}';
        }
        var priJson = '';
        if (document.getElementById('chkPri').checked) {
            var id = document.getElementById('priid').value;
            priJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"PRI"}';
        }
        var aryJson = '';
        if (document.getElementById('chkAry').checked) {
            var id = document.getElementById('aryid').value;
            aryJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"ARY"}';
        }
        var bnyJson = '';
        if (document.getElementById('chkBny').checked) {
            var id = document.getElementById('bnyid').value;
            bnyJson = '{"driver_other_company_id":"' + id +
                '","driver_profile_id":"' + driver_profile_id + '","other_company_code":"BNY"}';
        }
        var otherCabRows = '[';
        if (olaJson !== '') {
            otherCabRows = otherCabRows + olaJson + ',';
        }
        if (ubrJson !== '') {
            otherCabRows = otherCabRows + ubrJson + ',';
        }
        if (mruJson !== '') {
            otherCabRows = otherCabRows + mruJson + ',';
        }
        if (bmcJson !== '') {
            otherCabRows = otherCabRows + bmcJson + ',';
        }
        if (ecaJson !== '') {
            otherCabRows = otherCabRows + ecaJson + ',';
        }
        if (tabJson !== '') {
            otherCabRows = otherCabRows + tabJson + ',';
        }
        if (tfsJson !== '') {
            otherCabRows = otherCabRows + tfsJson + ',';
        }
        if (virJson !== '') {
            otherCabRows = otherCabRows + virJson + ',';
        }
        if (priJson !== '') {
            otherCabRows = otherCabRows + priJson + ',';
        }
        if (aryJson !== '') {
            otherCabRows = otherCabRows + aryJson + ',';
        }
        if (bnyJson !== '') {  
            otherCabRows = otherCabRows + bnyJson + ',';
        }
        otherCabRow = otherCabRows + '{"driver_other_company_id":"","driver_profile_id":"","other_company_code":""}]';
        sendOtherCompanyInfo(otherCabRow);
        var el = document.getElementById("overlay");
        el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
        document.getElementById('serverMessage').innerHTML = 'Data inserted successfully.<br/>Your id is ' + uniqueid;
    },
    error: function(xhr, textStatus, errorThrown) {
        var err = textStatus + ', ' + errorThrown;

        var el = document.getElementById("overlay");
        el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
        document.getElementById('serverMessage').innerHTML = 'Failed';
    }

});
}

function sendOtherCompanyInfo(otherCabRow) {
    var insertOtherCompInfoUrl = getPhpScriptsPath() +
        'insert_other_company_code.php?othercabrows=' + otherCabRow + '&verbose=Y';
    $.ajax({
        type: 'GET',
        url: insertOtherCompInfoUrl,
        success: function(data) {

        }

    });
}

function sendDriverImage() {
    var imagetype = 'Driver';
    var insertDriverImageUrl = getPhpScriptsPath() +
        'driver_image_browser.php';

    $.ajax({
        url: insertDriverImageUrl,
        type: "post",
        async: false,
        data: {
            'image': driver_image_string,
            'imagefilename': driver_image_name,
            'clientdatetime': clientdatetime,
            'imagetype': imagetype,
            'driverprofileid': driver_profile_id
        },
        success: function(data) {
            'success';
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}

function sendPanCardImage() {
    var imagetype = 'PanCard';
    var insertDriverImageUrl = getPhpScriptsPath() +
        'driver_image_browser.php';

    $.ajax({
        url: insertDriverImageUrl,
        type: "post",
        async: false,
        data: {
            'image': pan_image_string,
            'imagefilename': pan_image_name,
            'clientdatetime': clientdatetime,
            'imagetype': imagetype,
            'driverprofileid': driver_profile_id
        },
        success: function(data) {
            'success';
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}

function sendAadharImage() {
    var imagetype = 'AadharCard';
    var insertDriverImageUrl = getPhpScriptsPath() +
        'driver_image_browser.php';

    $.ajax({
        url: insertDriverImageUrl,
        type: "post",
        async: false,
        data: {
            'image': aadhar_image_string,
            'imagefilename': aadhar_image_name,
            'clientdatetime': clientdatetime,
            'imagetype': imagetype,
            'driverprofileid': driver_profile_id
        },
        success: function(data) {
            'success';
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}

function sendLicenseImage() {
    var imagetype = 'License';
    var insertDriverImageUrl = getPhpScriptsPath() +
        'driver_image_browser.php';

    $.ajax({
        url: insertDriverImageUrl,
        type: "post",
        async: false,
        data: {
            'image': license_image_string,
            'imagefilename': license_image_name,
            'clientdatetime': clientdatetime,
            'imagetype': imagetype,
            'driverprofileid': driver_profile_id
        },
        success: function(data) {
            'success';
        },
        error: function(xhr, textStatus, errorThrown) {
            var err = textStatus + ', ' + errorThrown;
        }
    });
}