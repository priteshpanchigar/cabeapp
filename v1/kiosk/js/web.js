var pos;
function getForHireWeb(lat, lng, clientdatetime, successCallback, failureCallBack) {
    var sUrl = getWebPhpScriptsPath() + 'get_for_hire_web.php';
    var keyvalues = {
        l: lat,
        o: lng,
        ct: clientdatetime
    };
    $.ajax({
        type: 'post',
        url: sUrl,
        data: keyvalues,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
            sUrl = settings.url + "?" + settings.data;
            //console.log('getForHire: ' + sUrl);
        },
        success: function(response) {
            successCallback(response);
        },
        error: function() {
            console.log('error');
            failureCallBack();
        }
    });
}


function confirmBooking() {
    console.log("confirmBooking()");
    var number = $('#number').val();
    var numberConfirm = $('#numberConfirm').val();
    var serviceCode = $('#serviceCode').text();
    if (number !== numberConfirm) {
        $('#modal-footer').html('Phone number does not match. Please re-enter.');
        return false;
    }
    var isValid = true;
    if (isNaN(number)) {
        isValid = false;
    }
    if (number.length !== 10) {
        isValid = false;
    }



    if (!isValid) {
        $('#modal-footer').html('Phone number is invalid. Please re-enter.');
        return;
    }
    var clientdatetime = formatDate(new Date());
    var sUrl = getWebPhpScriptsPath() + 'request_cab_web.php';
    var keyvalues = {
        nationalno: number,
        l: pos.coords.latitude,
        o: pos.coords.longitude,
        ct: clientdatetime,
        servicecode: serviceCode
    };
    $("#modalProgress").show();
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with upload progress
                    $('.progressbar .bar').css('width', '' + (100 * evt.loaded / evt.total) + '%');
                    console.log("up", percentComplete);
                }
            }, false);
            //Download progress
            xhr.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with download progress
                    console.log("down", percentComplete);
                } else {
                    // the way jsFiddle's ajax echo service works the length
                    // is not computable, so just adding this here:
                    console.log("down", evt.loaded / 600000);
                }
            }, false);
            return xhr;
        },
        type: 'post',
        url: sUrl,
        data: keyvalues,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
            sUrl = settings.url + "?" + settings.data;
            console.log(sUrl);
        },
        success: function(response) {
            console.log("confirmBooking response: " + response);

            if (!response || response == -1) {
                alert("Something went wrong\n Please try again.");
                return;
            }
            acceptBooking(response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#modalProgress").hide();
            console.log("Error: " + errorThrown.toString);
        }
    });

    return isValid;
}
var timeoutId;
$(document).ready(function() {
    if (screenfull.enabled) {
        screenfull.request();
    }
    getLocation();

});

function updateScreen(lat, lng) {


    getForHireWeb(lat, lng, formatDate(new Date()),
        function(data) {
            var len = data.length;
            var i, row, eta, sEta;
            var nocabs = "No cabs";
            $('#byEta').html(nocabs).addClass("nocabborder");
            $('#ccEta').html(nocabs).addClass("nocabborder");
            $('#ftEta').html(nocabs).addClass("nocabborder");
            $('#tnpEta').html(nocabs).addClass("nocabborder");
            $('#ttpEta').html(nocabs).addClass("nocabborder");
            $('#sgEta').html(nocabs).addClass("nocabborder");
            $('#stEta').html(nocabs).addClass("nocabborder");
            $('#arEta').html(nocabs).addClass("nocabborder");
            $('#saEta').html(nocabs).addClass("nocabborder");
            $('#BY').click(function() {
                console.log('No Cabs Available');
            });
            $('#CC').click(function() {
                console.log('No Cabs Available');
            });
            $('#FT').click(function() {
                console.log('No Cabs Available');
            });
            $('#TNP').click(function() {
                console.log('No Cabs Available');
            });
            $('#TTP').click(function() {
                console.log('No Cabs Available');
            });
            $('#SG').click(function() {
                console.log('No Cabs Available');
            });
            $('#ST').click(function() {
                console.log('No Cabs Available');
            });
            $('#AR').click(function() {
                console.log('No Cabs Available');
            });
            $('#SA').click(function() {
                console.log('No Cabs Available');
            });
            var foundBY = false,
                foundCC = false,
                foundFT = false,
                foundTNP = false,
                foundTTP = false,
                foundSG = false,
                foundST = false,
                foundAR = false,
                foundSA = false;
            for (i = 0; i < len; i++) {
                row = data[i];
                //console.log("row: " + row.service_code);
                if (row.service_code == 'BY') {
                    $('#byFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else
                if (row.service_code == 'CC') {
                    $('#ccFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else
                if (row.service_code == 'FT') {
                    $('#ftFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else
                if (row.service_code == 'TNP') {
                    $('#tnpFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else
                if (row.service_code == 'TTP') {
                    $('#ttpFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else
                if (row.service_code == 'SG') {
                    $('#sgFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else
                if (row.service_code == 'ST') {
                    $('#stFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else
                if (row.service_code == 'AR') {
                    $('#arFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                } else {
                    $('#saFare').html(row.metered == 1 ? "As per Meter" : "₹ " + row.rate_per_km + " per km");
                }

                eta = Math.round(row.eta);
                if (!row.eta)
                    continue;
                if (eta < 1) {
                    eta = 1;
                }
                sEta = "<b>" + eta + "<b> min.";
                if (row.service_code == 'BY' && !foundBY) {
                    foundBY = true;
                    $('#byEta').html(sEta).addClass("btn-primary");
                    console.log("Found BY cab.");
                    document.getElementById("BY").onclick = function() {
                        clearTimeout(timeoutId);
                        console.log("Booking BY");
                        bookCab("BY");
                    };
                }
                if (row.service_code == 'CC' && !foundCC) {
                    foundCC = true;
                    $('#ccEta').html(sEta).addClass("btn-primary");
                    document.getElementById("CC").onclick = function() {
                        clearTimeout(timeoutId);
                        bookCab("CC");
                    };
                }
                if (row.service_code == 'FT' && !foundFT) {
                    foundFT = true;
                    $('#ftEta').html(sEta).addClass("btn-primary");
                    $('#FT').click(function() {
                        clearTimeout(timeoutId);
                        bookCab("FT");
                    });
                }
                if (row.service_code == 'TNP' && !foundTNP) {
                    foundTNP = true;
                    $('#tnpEta').html(sEta).addClass("btn-primary");
                    $('#TNP').click(function() {
                        clearTimeout(timeoutId);
                        bookCab("TNP");
                    });

                }
                if (row.service_code == 'TTP' && !foundTTP) {
                    foundTTP = true;
                    $('#ttpEta').html(sEta).addClass("btn-primary");
                    $('#TTP').click(function() {
                        clearTimeout(timeoutId);
                        bookCab("TTP");
                    });
                }
                if (row.service_code == 'SG' && !foundSG) {
                    foundSG = true;
                    $('#sgEta').html(sEta).addClass("btn-primary");
                    $('#SG').click(function() {
                        clearTimeout(timeoutId);
                        bookCab("SG");
                    });
                }
                if (row.service_code == 'ST' && !foundST) {
                    foundST = true;
                    $('#stEta').html(sEta).addClass("btn-primary");
                    $('#ST').click(function() {
                        clearTimeout(timeoutId);
                        bookCab("ST");
                    });
                }
                if (row.service_code == 'AR' && !foundAR) {
                    foundAR = true;
                    $('#arEta').html(sEta).addClass("btn-primary");
                    $('#AR').click(function() {
                        clearTimeout(timeoutId);
                        bookCab("AR");
                    });
                }
                if (row.service_code == 'SA' && !foundSA) {
                    foundSA = true;
                    $('#saEta').html(sEta).addClass("btn-primary");
                    $('#SA').click(function() {
                        clearTimeout(timeoutId);
                        bookCab("SA");
                    });
                }
            }
            timeoutId = window.setTimeout(getLocation, 10 * 1000);
        },
        function() {
            console.log("Failed to get forhire info");
            timeoutId = window.setTimeout(getLocation, 2000);
        }
    );

}
// Called when user clicks on a particular cab picture on Web screen 
function bookCab(serviceCode) {
    // Get the modal
    $('#number').val('');
    $('#numberConfirm').val('');
    var modal = document.getElementById('myModal');
    $('#serviceCode').text(serviceCode);
    var sServiceCodeFull = "";
    sServiceCodeFull = serviceCode == "CC" ? "Cool Cab" :
        serviceCode == "BY" ? "Black & Yellow" :
        serviceCode == "FT" ? "Fleet Taxi" :
        serviceCode == "TNP" ? "Standard Taxi" :
        serviceCode == "TTP" ? "Prime Taxi" :
        serviceCode == "SG" ? "Sanghini Taxi" :
        serviceCode == "ST" ? "Shared Taxi" :
        serviceCode == "AR" ? "Auto Rickshaw" :
        serviceCode == "SA" ? "Stared Auto" : "";
    $("#cabtype").html(" " + sServiceCodeFull);
    // Get the button that opens the modal
    var btn = document.getElementById("divBook");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    //btn.onclick = function() {
    modal.style.display = "block";
    //}
    $("#confirmBooking").click(function() {
        if (confirmBooking())
            modal.style.display = "none";

    });

    $("#cancel").click(function() {
        modal.style.display = "none";
         updateScreen(pos.coords.latitude, pos.coords.longitude);
    });


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
         updateScreen(pos.coords.latitude, pos.coords.longitude);
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
             updateScreen(pos.coords.latitude, pos.coords.longitude);
        }
    };
}

// Called when user clicks on a particular cab picture on Web screen 
function acceptBooking(resp) {
    // Get the modal
    $("#modalProgress").hide();
    var modal = document.getElementById('modalBookingConfirmed');

    $('#driverNo').text(resp.driver_phoneno);

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    //btn.onclick = function() {
    modal.style.display = "block";
    //}
    $("#bookingDone").click(function() {
        modal.style.display = "none";
         updateScreen(pos.coords.latitude, pos.coords.longitude);
		// window.location.reload();
    });
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
         updateScreen(pos.coords.latitude, pos.coords.longitude);
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
           updateScreen(pos.coords.latitude, pos.coords.longitude);
        }
    };


}

function formatDate(d) {
    return d.getFullYear() + "-" +
        (d.getMonth() + 1) + "-" +
        d.getDate() + " " +
        d.getHours() + ":" +
        d.getMinutes() + ":" +
        d.getSeconds();

}

function toggleFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    } else {
        cancelFullScreen.call(doc);
    }
}


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        alert("Geolocation is not supported by this browser.");
        // x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    updateScreen(position.coords.latitude, position.coords.longitude);
	pos = position;

}

function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.");
            // x.innerHTML = "User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.");
            // x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            alert("The request to get user location timed out.");
            //  x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.");
            //  x.innerHTML = "An unknown error occurred."
            break;
    }
}