var map, gMarkers = [], user_emailId, id, tripid, marker;

function initMap() {
	user_emailId = gup("user_email");
	id = gup("id");
	tripid = gup("tripid");
	
    var myLatLng = {
        lat: 19.104,
        lng: 72.85
    };

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: myLatLng
    });

    window.setInterval(function() {
        showVehicles();
    }, 5000);

}


function getWalkWithmeLoc(successCallBack) {
    var walkwithmeurl = getPhpScriptsPath() + "walkwithmeloc.php?user_email=" + user_emailId + "&id=" + id+"&tripid="+tripid;
    $.ajax({
        type: 'GET',
        url: walkwithmeurl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus + ', ' + errorThrown);
        }
    });
}

function showVehicles() {
    var markerLatLng;


    getWalkWithmeLoc(function(oFriendLocation) {
        var len = oFriendLocation.length,
            row, html = "",
            options, i, iconColor;
			if(marker)
				marker.setMap(null);
            markerLatLng = new google.maps.LatLng(oFriendLocation.lat, oFriendLocation.lon);
            iconColor = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
           
            marker = new google.maps.Marker({
                position: markerLatLng,
                map: map,
                
                title: oFriendLocation.date
            });
           
            map.center(markerLatLng);
        

    });
}


