<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'addSharedPassenger';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
include("../variables.php");
include("../gcmSendMessage.php");
$resultrows  = array();
$basemessage = "";
if ($mysqli){
	
	$joinlat = empty($_REQUEST['joinlat']) || !isset($_REQUEST['joinlat']) ? 'NULL' : $_REQUEST['joinlat'];
	$joinlng = empty($_REQUEST['joinlng']) || !isset($_REQUEST['joinlng']) ? 'NULL' : $_REQUEST['joinlng'];
	$jumpinaddress = empty($_REQUEST['jumpinaddress']) || !isset($_REQUEST['jumpinaddress']) ? 'NULL' :
		"'" . $_REQUEST['jumpinaddress'] . "'" ;
	
	$sql = "call add_shared_passenger(".$appuserid .",".$tripid ."," .$joinlat .
									  "," .$joinlng .",".$jumpinaddress. 
									  ",".$clientdatetime.")";
	
	if ($verbose != 'N') {
		echo $sql . '<br>' ;
	}
	
   $result = $mysqli->query($sql);
    if (is_object($result)) {
        if ($result) {
            $rowcount = mysqli_num_rows($result);
            if ($rowcount == 0) {
                echo '';
            }
            while ($row = $result->fetch_assoc()) {
                array_push($resultrows, $row['gcm_registration_id']);
				$passenger_name    = $row['passenger_name'];
				
				echo json_encode($row);
				$basemessage = "\"passenger_name\":" ."'". $passenger_name . "'" .
							   ", \"message\":\"";
				if ($verbose != 'N') {	
					echo $basemessage;
				}

				$messagetopost = $passenger_name .' Has Joined the trip.';
				
				
				$jmessage = $basemessage . $messagetopost . "\"}";
				if ($verbose = 'Y') {
					echo "Message : $jmessage" . "<br>";
				}
				$title =  "Cab-e";
				
				gcmSendMessage($resultrows, $messagetopost, $title, '', 1, 1, "", "", "");	    
                break;
            }            
        } else {
            echo "-1"; // something went wrong, probably sql failed
        }
    }
    $mysqli->close();
} else {
    echo "-2"; // "Connection to db failed";
}