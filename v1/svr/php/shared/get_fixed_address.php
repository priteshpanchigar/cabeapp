<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getFixedAddress';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$fixedAddressRows = array();
	 	
    $sql = " call get_fixed_address()";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$fixedAddressRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$fixedAddressRows = array_filter($fixedAddressRows);
		if (!empty($fixedAddressRows)) {
			echo json_encode($fixedAddressRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}