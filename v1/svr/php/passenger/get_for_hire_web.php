<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getForHireWeb';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$getmyforhireWebRows = array();
	 
	$sql = " call get_for_hire_web(" . $lat . "," . $lng . "," . $clientdatetime . ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$getmyforhireWebRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$getmyforhireWebRows = array_filter($getmyforhireWebRows);
		if (!empty($getmyforhireWebRows)) {
			echo json_encode($getmyforhireWebRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}