<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'tripHistory';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$triphistoryRows = array();
	 
	$sql = " call trip_history(" . $appuserid . ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$triphistoryRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$triphistoryRows = array_filter($triphistoryRows);
		if (!empty($triphistoryRows)) {
			echo json_encode($triphistoryRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}