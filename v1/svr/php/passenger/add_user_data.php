<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'addUserData';
include("../dbconn_sar_apk.php"); 
include("../mobile_common_data_short.php");
include("../variables.php");

$logintype = empty($_REQUEST['lt']) || !isset($_REQUEST['lt']) ? 'NULL' :
		 $_REQUEST['lt'];		
		

if($logintype == "F") {
        
    $sql = "call add_facebook_data(".$appuserid .",".$facebookemail .
                                   ",".$firstname ."," .$lastname .
                                   "," .$gender .  ",".$userprofileimagefilename. 
								   ",".$dob. ")";
        if ($verbose != 'N') {
        echo $sql . '<br>' ;
    }
    
    if ($result = $mysqli->query($sql)) {    
        while ($row = $result->fetch_assoc()) {
            echo json_encode($row);
            break;
        } 
        $mysqli->close();
    }        else {
        echo json_encode((object) null); // something went wrong, probably sql failed
    }
        
} 
if ($logintype == "G") {

    $sql = "call add_googleplus_data(".$appuserid .",".$email .
                                   ",".$firstname ."," .$lastname .
                                   "," .$gender .  ",".$userprofileimagefilename.
								   ",".$dob. ")";
    if ($verbose != 'N') {
        echo $sql . '<br>' ;
    }
    
    if ($result = $mysqli->query($sql)) {    
        while ($row = $result->fetch_assoc()) {
            echo json_encode($row);
            break;
        } 
        $mysqli->close();
    }        else {
        echo json_encode((object) null); // something went wrong, probably sql failed
    }                               
        
}  