<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = "requestCab";
$resultrows = array();
$tripid = 0;
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) { 	
	date_default_timezone_set('Asia/Calcutta');
	$currentdate = date('m/d/Y h:i:s a', time());
	$timeinms = strtotime($currentdate) * 1000;
	$inpath      = "NULL";
	
	
	
	$sql = "call request_cab("  . $fromaddress . "," . $fromshortaddress . 
		"," .$toaddress . "," . $toshortaddress . "," . $fromsublocality . "," . $tosublocality . 
		"," .json_encode($trip_directions_polyline) ."," .$fromlat . "," . $fromlng . "," . $tolat . 
		"," .$tolng . "," . $timeinms ."," .$tripaction . "," . $triptime . 
		"," .$tripdistance . ","  . $appuserid .  "," .	$clientdatetime . "," . $email .  
		","  . $plannedstartdatetime . ","  . $servicecode .")";
		
	if ($verbose != 'N') {	
		echo "sql: " .$sql . "<br>";
	}
	if ($result = $mysqli->query($sql)) {
		if ($verbose != 'N') {
			echo "<br>sql result: ";
			var_dump($result);
			echo "<br>";
		}
	}
	if ($result && is_object($result))  {
		$rowcount=mysqli_num_rows($result);
		if ($verbose != 'N') {
			echo "rowcount: " .$rowcount . "<br>";
		}
		if ($rowcount > 0) {
			while ($row = $result->fetch_assoc()) {
				
				$resultrows[] = $row;
				echo json_encode($row);
				$tripid =  empty($row["trip_id"]) ? '-1' : $row["trip_id"];
				break;
				
			}
		}
		$result->free();
	}
	$mysqli->close();
} else {
	echo -1; // something went wrong, probably sql failed
	$tripid = -1;
}
