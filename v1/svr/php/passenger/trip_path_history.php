<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'tripPathHistory';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$trippathhistoryRows = array();
	 
	$sql = " call trip_path_history(" .$tripid . "," .$triptype.  "," .$appuserid. ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$trippathhistoryRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$trippathhistoryRows = array_filter($trippathhistoryRows);
		if (!empty($trippathhistoryRows)) {
			echo json_encode($trippathhistoryRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}