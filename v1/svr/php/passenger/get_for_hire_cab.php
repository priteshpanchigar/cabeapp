<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getForHireCab';
include("../dbconn_sar_apk.php");
include("../variables.php");
include_once("../mobile_common_data_short.php");
if ($mysqli) {
	$getmyforhirecabRows = array();
	 $sql = " call get_for_hire_cab(" . $lat . "," . $lng . "," . $tolat . "," . $tolng . "," . $servicecode . 
			"," . $clientdatetime . ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$getmyforhirecabRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$getmyforhirecabRows = array_filter($getmyforhirecabRows);
		if (!empty($getmyforhirecabRows)) {
			echo json_encode($getmyforhirecabRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}