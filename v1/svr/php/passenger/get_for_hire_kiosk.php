<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getForHireKiosk';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$getmyforhirekioskRows = array();
	 
	$sql = " call get_for_hire_kiosk(" . $imei .  "," . $clientdatetime . ")"; 
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$getmyforhirekioskRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$getmyforhirekioskRows = array_filter($getmyforhirekioskRows);
		if (!empty($getmyforhirekioskRows)) {
			echo json_encode($getmyforhirekioskRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}