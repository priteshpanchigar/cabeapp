<?php header('Access-Control-Allow-Origin: *');
$php_name = "GetDriverPositionShort";
include("../mobile_common_data_short.php");
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../output_log.php");
if ($mysqli) {
	
	$sendeverything = empty($_REQUEST['sendeverything']) || !isset($_REQUEST['sendeverything']) ? 1 : $_REQUEST['sendeverything'];
	$sql = " call get_driver_position_short(" . $tripid. ","  . $sendeverything .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	$result = $mysqli->query($sql);
    if (is_object($result)) {
        if ($result) {
            $rowcount = mysqli_num_rows($result);
            if ($rowcount == 0) {
                echo '';
            }
            while ($row = $result->fetch_assoc()) {
				output_log($php_name, $appuserid, json_encode($row));
                echo json_encode($row);                
                break;
            }            
        } else {
            echo "-1"; // something went wrong, probably sql failed
        }
    }
    $mysqli->close();
} else {
    echo "-2"; // "Connection to db failed";
}