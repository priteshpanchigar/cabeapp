<?php
	include("variables.php");
	include("mobile_common_data_short.php");
	error_reporting(~E_NOTICE);
	error_reporting(E_ALL);
	set_time_limit (0);
	$address = "66.201.95.11";
	$port = 5555;
	$max_clients = 250;
	//ini_set("default_socket_timeout", 1000);
	
	
	if(!($sock = socket_create(AF_INET, SOCK_STREAM, 0))){
		$errorcode = socket_last_error();
		$errormsg = socket_strerror($errorcode);
		die("Couldn't create socket: [$errorcode] $errormsg \n");
	}

	echo "Socket created \n";
	// Bind the source address
	
	if( !socket_bind($sock, $address , $port) ){
		$errorcode = socket_last_error();
		$errormsg = socket_strerror($errorcode);
		die("Could not bind socket : [$errorcode] $errormsg \n");
	}

	echo "Socket bind OK \n";
	
	if(!socket_listen ($sock , 10)){
		$errorcode = socket_last_error();
		$errormsg = socket_strerror($errorcode);
		die("Could not listen on socket : [$errorcode] $errormsg \n");
	}

	echo "Socket listen OK \n";
	echo "Waiting for incoming connections... \n";
	//array of client sockets
	$client_socks = array();
	//array of sockets to read
	$read = array();
	//start loop to listen for incoming connections and process existing connections
	while (true) {
		//prepare array of readable client sockets
		$read = array();
		//first socket is the master socket
		$read[0] = $sock;
		//now add the existing client sockets
		for ($i = 0; $i < $max_clients; $i++)    {
			if(!isset($client_socks[$i]))
				break;
			if($client_socks[$i] != null)        {
				$read[$i+1] = $client_socks[$i];
			}

		}

		//now call select - blocking call
		
		if(socket_select($read , $write , $except , null) === false)    {
			$errorcode = socket_last_error();
			$errormsg = socket_strerror($errorcode);
			die("Could not listen on socket : [$errorcode] $errormsg \n");
		}

		//if ready contains the master socket, then a new connection has come in
		
		if (in_array($sock, $read))     {
			for ($i = 0; $i < $max_clients; $i++)        {
				
				if ($client_socks[$i] == null)             {
					$client_socks[$i] = socket_accept($sock);
					//display information about the client who is connected
					//socket_set_option($sock,SOL_SOCKET, SO_RCVTIMEO, array("sec"=>1, "usec"=>0));
					//socket_set_option($sock, SOL_SOCKET, SO_SNDTIMEO, array('sec' => 1, 'usec' => 0));
					if(socket_getpeername($client_socks[$i], $address, $port))                {
						echo "Client $address : $port is now connected to us. \n";
					}

					//Send Welcome message to client
					$message = "Welcome to php socket server version 1.0 \n";
					$message .= "Enter a message and press enter, and i shall reply back \n";
					socket_write($client_socks[$i] , $message);
					break;
				}

			}

		}

		//check each client if they send any data
		for ($i = 0; $i < $max_clients; $i++)    {
			if(!isset($client_socks[$i]))
				break;
			if (in_array($client_socks[$i] , $read))        {
				socket_set_timeout($client_socks[$i], 0, 35000);
				$input = socket_read($client_socks[$i] , 1024);
				
				if ($input == null)             {
					//zero length string meaning disconnected, remove and close the socket
					unset($client_socks[$i]);
					socket_close($client_socks[$i]);
				}

				$n = trim($input);
include("dbconn_sar_apk.php");
$sql = "call device_reading(" . $input . ")";
			
					if ($verbose != 'N') {
						echo $sql . '<br>';
					}


if ($mysqli) {

$deviceArray = explode(',', $input);
$type =  $deviceArray[0];
$sql = '';
if($type == '$GNX_MLOC' ) {
	$imei =  $deviceArray[1];
	$packet_length = $deviceArray[2];
	$packet_type = $deviceArray[3];
	$current_time = $deviceArray[4];
	$current_date = $deviceArray[5];
	$logged_time = $deviceArray[6];
	$logged_date = $deviceArray[7];
	$gps_data_valid = $deviceArray[8];
	$lat = $deviceArray[9];
	$latitude_direction = $deviceArray[10];
	$lng = $deviceArray[11];
	$longitude_direction = $deviceArray[12];
	$speed = $deviceArray[13];
	$heading_angle = $deviceArray[14];
	$total_satellites = $deviceArray[15];
	$gsm_signal_strength = $deviceArray[16];
	$panic_status = $deviceArray[17];
	$vehicle_movement_status = $deviceArray[18];
	$gps_quality = $deviceArray[19];
	$power_status = $deviceArray[20];
	$odometer_reading_pulse = $deviceArray[21];
	$odometer_reading_gps = $deviceArray[22]; 
	$metering_status = $deviceArray[23];
	$trip_id = $deviceArray[24];
	$running_distance = $deviceArray[25];
	$GNX_ver = $deviceArray[26];
	$crc = $deviceArray[27];
	
	$sql = "call device_reading('" .$type ."'," .$imei ."," .$packet_length .",  " .$packet_type . 	",'"  .$current_time .
		   "','"  .$current_date . "','" .$logged_time . "','" .$logged_date . "'," .$gps_data_valid .", " .$lat .",'" .$latitude_direction .
		   "',".$lng . ",'" .$longitude_direction ."'," .$speed . "," .$heading_angle .", ". $total_satellites .	",".$gsm_signal_strength.
		   ",".$panic_status.",'".$vehicle_movement_status . "','".$gps_quality."',". $power_status."," .$odometer_reading_pulse.
		   ",". $odometer_reading_gps . ",'".$metering_status."', ".$trip_id.	", ".$running_distance.",'".$GNX_ver."','".$crc . "')";
		
		//echo $sql;	
}
					$result = $mysqli->query($sql);
					if (is_object($result)) {						
						if ($result) {
							$rowcount = mysqli_num_rows($result);
							if ($rowcount == 0) {
								echo '';
							}
							while ($row = $result->fetch_assoc()) {
								echo json_encode($row);
								break;
							}

						} else {
							echo "-1";
							// something went wrong, probably sql failed
						}

					}

					$mysqli->close();
				}

				echo $input;
				// $output = "OK ... $input";
				//echo "Sending output to client \n";
				//send response to client
				//socket_write($client_socks[$i] , $output);
			}

		}

	}

	?>