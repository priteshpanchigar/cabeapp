<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getDriverProfileData';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli) {
    $getsingledriverprofileRows = array();
    
    $driverprofileid = empty($_REQUEST['driverprofileid']) || !isset($_REQUEST['driverprofileid']) ? 'NULL' : $_REQUEST['driverprofileid'];
    
    $sql = " call get_single_driver_profile_data(" . $driverprofileid . ")";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
    if ($result = $mysqli->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $getsingledriverprofileRows[] = $row;
        }
        $result->free(); // free result set
    }    
    $getsingledriverprofileRows = array_filter($getsingledriverprofileRows);
    if (!empty($getsingledriverprofileRows)) {
        echo json_encode($getsingledriverprofileRows);
    } else {
        echo "-1";
    }    
    $mysqli->close(); // close connection
} else {
    echo "-1";
}