<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getDriverProfileCode';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli) {
    $getothercompanycodeRows = array();
        
    $sql = " call get_other_company_code()";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
    if ($result = $mysqli->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $getothercompanycodeRows[] = $row;
        }
        $result->free(); // free result set
    }    
    $getothercompanycodeRows = array_filter($getothercompanycodeRows);
    if (!empty($getothercompanycodeRows)) {
        echo json_encode($getothercompanycodeRows);
    } else {
        echo "-1";
    }
    $mysqli->close(); // close connection    
} else {
    echo "-1";
}