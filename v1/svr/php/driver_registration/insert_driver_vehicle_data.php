<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'insertdriverVehicleData';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli) {
    $uniqueid = empty($_REQUEST['uniqueid']) || !isset($_REQUEST['uniqueid']) ? 'NULL' : "'" . $_REQUEST['uniqueid'] . "'";
    
    $vehiclenostate = empty($_REQUEST['vehiclenostate']) || !isset($_REQUEST['vehiclenostate']) ? 'NULL' : "'" . $_REQUEST['vehiclenostate'] . "'";
    
    $vehiclenocity = empty($_REQUEST['vehiclenocity']) || !isset($_REQUEST['vehiclenocity']) ? 'NULL' : "'" . $_REQUEST['vehiclenocity'] . "'";
    
    $vehiclenocode = empty($_REQUEST['vehiclenocode']) || !isset($_REQUEST['vehiclenocode']) ? 'NULL' : "'" . $_REQUEST['vehiclenocode'] . "'";
    
    $vehicleno = empty($_REQUEST['vehicleno']) || !isset($_REQUEST['vehicleno']) ? 'NULL' : $_REQUEST['vehicleno'];
    
    
    $vehiclecompany = empty($_REQUEST['vehiclecompany']) || !isset($_REQUEST['vehiclecompany']) ? 'NULL' : "'" . $_REQUEST['vehiclecompany'] . "'";
    
    $vehiclecategory = empty($_REQUEST['vehiclecategory']) || !isset($_REQUEST['vehiclecategory']) ? 'NULL' : "'" . $_REQUEST['vehiclecategory'] . "'";
    
    $vehiclemodel = empty($_REQUEST['vehiclemodel']) || !isset($_REQUEST['vehiclemodel']) ? 'NULL' : "'" . $_REQUEST['vehiclemodel'] . "'";
    
    $vehicletype = empty($_REQUEST['vehicletype']) || !isset($_REQUEST['vehicletype']) ? 'NULL' : "'" . $_REQUEST['vehicletype'] . "'";
    
    $vehiclecolor = empty($_REQUEST['vehiclecolor']) || !isset($_REQUEST['vehiclecolor']) ? 'NULL' : "'" . $_REQUEST['vehiclecolor'] . "'";
    
    $servicecode      = empty($_REQUEST['servicecode']) || !isset($_REQUEST['servicecode']) ? 'NULL' : "'" . $_REQUEST['servicecode'] . "'";
    $vehicleownership = empty($_REQUEST['vehicleownership']) || !isset($_REQUEST['vehicleownership']) ? 'NULL' : "'" . $_REQUEST['vehicleownership'] . "'";
    
    $sql = "call insert_driver_vehicle_data(" . $uniqueid . "," . $vehiclenostate . "," . $vehiclenocity . ",  " . $vehiclenocode . "," . $vehicleno . "," . $vehiclecompany . "," . $vehiclecategory . "," . $vehiclemodel . "," . $vehicletype . ", " . $vehiclecolor . ", " . $servicecode . "," . $vehicleownership . ")";
    
    
    if ($verbose != 'N') {
        echo '<br>' . $sql . '<br>';
    }
    $result = $mysqli->query($sql);
    if (is_object($result)) {
        while ($row = $result->fetch_assoc()) {
            echo json_encode($row);
            break;
        }
        
    }
    $mysqli->close();
} else {
    echo "-1";
}