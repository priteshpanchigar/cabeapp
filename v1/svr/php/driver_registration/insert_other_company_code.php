<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name   = "insertOtherCompanyCode";
$resultrows = array();
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli){
    
    $inothercabrows = empty($_REQUEST['othercabrows']) || !isset($_REQUEST['othercabrows']) ? 'NULL' : $_REQUEST['othercabrows'];
    
    if (empty($inothercabrows)) {
        echo -1;
        return -1;
    }
    
    $othercabrows = json_decode($inothercabrows);
    
    
    
    $valuessql = "";
    $insertsql = 'insert into driver_other_member(driver_profile_id, other_company_code,  
				  driver_other_company_id) values ';
    $delim     = '';
    
    foreach ($othercabrows as $obj) {
        $valuessql = $valuessql . $delim . "('" . $obj->driver_profile_id . "','" . $obj->other_company_code . "','" . $obj->driver_other_company_id . "')";
        $delim     = ',';
        
        $runningsql = $insertsql . $valuessql . " ON DUPLICATE KEY UPDATE  " . "driver_other_company_id = VALUES(driver_other_company_id)";
        
        if ($verbose != 'N') {
            echo "<br>: " . $insertsql . $valuessql . "<br>";
            echo $runningsql;
            if ($verbose != 'N') {
                echo "<br>: " . $insertsql . $valuessql . "<br>";
                echo $runningsql;
                
                if ($runresult = $mysqli->query($runningsql)) {
                    echo '';
                } else {
                    printf("Errormessage: %s\n", $mysqli->error);
                    // echo -1; // something went wrong, probably sql failed
                }
            }
        }
    }
	$mysqli->close();
} else {
    echo -2;
}

