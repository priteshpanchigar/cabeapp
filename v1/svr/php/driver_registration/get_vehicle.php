<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getVehicle';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli) {
    $getdriverprofileRows = array();
    
    $uniqueid    = empty($_REQUEST['uniqueid']) || !isset($_REQUEST['uniqueid']) ? 'NULL' : "'" . $_REQUEST['uniqueid'] . "'";
    $searchvalue = empty($_REQUEST['searchvalue']) || !isset($_REQUEST['searchvalue']) ? 'NULL' : "'" . $_REQUEST['searchvalue'] . "'";
    
    $sql = " call get_vehicle(" . $uniqueid . ", " . $searchvalue . ")";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
    if ($result = $mysqli->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $getdriverprofileRows[] = $row;
        }
        $result->free(); // free result set
    }
    
    
    $getdriverprofileRows = array_filter($getdriverprofileRows);
    if (!empty($getdriverprofileRows)) {
        echo json_encode($getdriverprofileRows);
    } else {
        echo "-1";
    }
	$mysqli->close(); // close connection    
} else {
    echo "-1";
}