<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getOtherCompanyMember';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli) {
    $getothercompanymemberRows = array();
    
    $driverprofileid = empty($_REQUEST['driverprofileid']) || !isset($_REQUEST['driverprofileid']) ? 'NULL' : $_REQUEST['driverprofileid'];
    
    $sql = " call get_other_company_member(" . $driverprofileid . ")";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
    if ($result = $mysqli->query($sql)) {
        while ($row = $result->fetch_assoc()) {
            $getothercompanymemberRows[] = $row;
        }
        $result->free(); // free result set
    }
    
    
    $getothercompanymemberRows = array_filter($getothercompanymemberRows);
    if (!empty($getothercompanymemberRows)) {
        echo json_encode($getothercompanymemberRows);
    } else {
        echo "-1";
    }    
    $mysqli->close(); // close connection
} else {
    echo "-1";
}