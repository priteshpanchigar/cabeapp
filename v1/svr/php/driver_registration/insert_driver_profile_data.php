<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'insertdriverProfileData';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_sar.php");
if ($mysqli ){
	
	$uniquekey = empty($_REQUEST['uniquekey']) || !isset($_REQUEST['uniquekey']) ? 'NULL' :
		"'" . $_REQUEST['uniquekey'] . "'" ;
	$tpincode = empty($_REQUEST['tpincode']) || !isset($_REQUEST['tpincode']) ? 'NULL' : $_REQUEST['tpincode'] ;
	$ppincode = empty($_REQUEST['ppincode']) || !isset($_REQUEST['ppincode']) ? 'NULL' : $_REQUEST['ppincode'];
	$driverincitysince = empty($_REQUEST['driverincitysince']) || !isset($_REQUEST['driverincitysince']) ? 'NULL' :
		"'" . $_REQUEST['driverincitysince'] . "'" ;
	
	$driverfirstname = empty($_REQUEST['driverfirstname']) || !isset($_REQUEST['driverfirstname']) ? 'NULL' :
		"'" . $_REQUEST['driverfirstname'] . "'" ;		
	$driverlastname = empty($_REQUEST['driverlastname']) || !isset($_REQUEST['driverlastname']) ? 'NULL' :
		"'" . $_REQUEST['driverlastname'] . "'" ;
	$driverdob = empty($_REQUEST['driverdob']) || !isset($_REQUEST['driverdob']) ? 'NULL' :
		"'" . $_REQUEST['driverdob'] . "'" ;
	$driveremail = empty($_REQUEST['driveremail']) || !isset($_REQUEST['driveremail']) ? 'NULL' :
		"'" . $_REQUEST['driveremail'] . "'" ;
	$driverphoneno = empty($_REQUEST['driverphoneno']) || !isset($_REQUEST['driverphoneno']) ? 'NULL' :
		"'" . $_REQUEST['driverphoneno'] . "'" ;
	$drivergender = empty($_REQUEST['drivergender']) || !isset($_REQUEST['drivergender']) ? 'NULL' :
		"'" . $_REQUEST['drivergender'] . "'" ;	
	$driverpancard = empty($_REQUEST['driverpancard']) || !isset($_REQUEST['driverpancard']) ? 'NULL' :
		"'" . $_REQUEST['driverpancard'] . "'" ;
	$driveraadhar = empty($_REQUEST['driveraadhar']) || !isset($_REQUEST['driveraadhar']) ? 'NULL' :
		"'" . $_REQUEST['driveraadhar'] . "'" ;	
	$driverlicenseno = empty($_REQUEST['driverlicenseno']) || !isset($_REQUEST['driverlicenseno']) ? 'NULL' :
		"'" . $_REQUEST['driverlicenseno'] . "'" ;
	$driverpermiflag = empty($_REQUEST['driverpermiflag']) || !isset($_REQUEST['driverpermiflag']) ? 'NULL' :
		"'" . $_REQUEST['driverpermiflag'] . "'" ;
	$driverpermitno = empty($_REQUEST['driverpermitno']) || !isset($_REQUEST['driverpermitno']) ? 'NULL' :
		"'" . $_REQUEST['driverpermitno'] . "'" ;
	$driverbadgeflag = empty($_REQUEST['driverbadgeflag']) || !isset($_REQUEST['driverbadgeflag']) ? 'NULL' :
		"'" . $_REQUEST['driverbadgeflag'] . "'" ;
	$driverbadgeno = empty($_REQUEST['driverbadgeno']) || !isset($_REQUEST['driverbadgeno']) ? 'NULL' :
		"'" . $_REQUEST['driverbadgeno'] . "'" ;
	$cabneededflag = empty($_REQUEST['cabneededflag']) || !isset($_REQUEST['cabneededflag']) ? 'NULL' :
		"'" . $_REQUEST['cabneededflag'] . "'" ;
	$drivervehicleno = empty($_REQUEST['drivervehicleno']) || !isset($_REQUEST['drivervehicleno']) ? 'NULL' :
		"'" . $_REQUEST['drivervehicleno'] . "'" ;
	$remark = empty($_REQUEST['remark']) || !isset($_REQUEST['remark']) ? 'NULL' :
		"'" . $_REQUEST['remark'] . "'" ;
	$internalcomment = empty($_REQUEST['internalcomment']) || !isset($_REQUEST['internalcomment']) ? 'NULL' :
		"'" . $_REQUEST['internalcomment'] . "'" ;
	$googleaddress = empty($_REQUEST['googleaddress']) || !isset($_REQUEST['googleaddress']) ? 'NULL' :
		"'" . $_REQUEST['googleaddress'] . "'" ;
	$taddress1 = empty($_REQUEST['taddress1']) || !isset($_REQUEST['taddress1']) ? 'NULL' :
		"'" . $_REQUEST['taddress1'] . "'" ;
	$taddress2 = empty($_REQUEST['taddress2']) || !isset($_REQUEST['taddress2']) ? 'NULL' :
		"'" . $_REQUEST['taddress2'] . "'" ;
	$tcitytown = empty($_REQUEST['tcitytown']) || !isset($_REQUEST['tcitytown']) ? 'NULL' :
		"'" . $_REQUEST['tcitytown'] . "'" ;
	$tlandmark = empty($_REQUEST['tlandmark']) || !isset($_REQUEST['tlandmark']) ? 'NULL' :
		"'" . $_REQUEST['tlandmark'] . "'" ;
	$tstate = empty($_REQUEST['tstate']) || !isset($_REQUEST['tstate']) ? 'NULL' :
		"'" . $_REQUEST['tstate'] . "'" ;
	$paddress1 = empty($_REQUEST['paddress1']) || !isset($_REQUEST['paddress1']) ? 'NULL' :
		"'" . $_REQUEST['paddress1'] . "'" ;
	$paddress2 = empty($_REQUEST['paddress2']) || !isset($_REQUEST['paddress2']) ? 'NULL' :
		"'" . $_REQUEST['paddress2'] . "'" ;
	$pcitytown = empty($_REQUEST['pcitytown']) || !isset($_REQUEST['pcitytown']) ? 'NULL' :
		"'" . $_REQUEST['pcitytown'] . "'" ;
	$plandmark = empty($_REQUEST['plandmark']) || !isset($_REQUEST['plandmark']) ? 'NULL' :
		"'" . $_REQUEST['plandmark'] . "'" ;
	$pstate = empty($_REQUEST['pstate']) || !isset($_REQUEST['pstate']) ? 'NULL' :
		"'" . $_REQUEST['pstate'] . "'" ;		
	
	$locality = isset($_REQUEST['locality']) ? "\"" . $_REQUEST['locality'] . "\"" : 'NULL';
	$sublocality = isset($_REQUEST['sublocality']) ? "\"" . $_REQUEST['sublocality'] . "\"" : 'NULL';
	$postalcode = isset($_REQUEST['postalcode']) ? "\"" . $_REQUEST['postalcode'] . "\"" : 'NULL';	
	$route = isset($_REQUEST['route']) ? "\"" . $_REQUEST['route'] . "\"" : 'NULL';	
	
	$neighborhood = isset($_REQUEST['neighborhood']) ? "\"" . $_REQUEST['neighborhood'] . "\"" : 'NULL';	
	
	$administrative_area_level_2 = isset($_REQUEST['administrative_area_level_2']) ? "\"" . $_REQUEST['administrative_area_level_2'] . "\"" : 'NULL';
	
	$administrative_area_level_1 = isset($_REQUEST['administrative_area_level_1']) ? "\"" . $_REQUEST['administrative_area_level_1'] . "\"" : 'NULL';	
	
	$latitude = isset($_REQUEST['latitude']) ? "\"" . $_REQUEST['latitude'] . "\"" : 'NULL';	
	
	$longitude = isset($_REQUEST['longitude']) ? "\"" . $_REQUEST['longitude'] . "\"" : 'NULL';	
	
	$profilesubmitted = empty($_REQUEST['profilesubmitted']) || !isset($_REQUEST['profilesubmitted']) ? 0 :  $_REQUEST['profilesubmitted'] ;

	$uniqueid = empty($_REQUEST['uniqueid']) || !isset($_REQUEST['uniqueid']) ? 'NULL' :
			"'" . $_REQUEST['uniqueid'] . "'" ;

	$paymentstatus = empty($_REQUEST['paymentstatus']) || !isset($_REQUEST['paymentstatus']) ? 
			'NULL' : $_REQUEST['paymentstatus'];

	$paymentamount = empty($_REQUEST['paymentamount']) || !isset($_REQUEST['paymentamount']) ? 
			'NULL' : $_REQUEST['paymentamount'];

	$receiptno = empty($_REQUEST['receiptno']) || !isset($_REQUEST['receiptno']) ? 'NULL' :
			"'" . $_REQUEST['receiptno'] . "'" ;

	$receiptdate = empty($_REQUEST['receiptdate']) || !isset($_REQUEST['receiptdate']) ? 'NULL' :
			"'" . $_REQUEST['receiptdate'] . "'" ;

	$referralwalkin = empty($_REQUEST['referralwalkin']) || !isset($_REQUEST['referralwalkin']) ? 'NULL' :
		"'" . $_REQUEST['referralwalkin'] . "'" ;
		
	$referralsupervisor = empty($_REQUEST['referralsupervisor']) || !isset($_REQUEST['referralsupervisor']) ? 'NULL' :
		"'" . $_REQUEST['referralsupervisor'] . "'" ;

	$referraldriver = empty($_REQUEST['referraldriver']) || !isset($_REQUEST['referraldriver']) ? 'NULL' :
		"'" . $_REQUEST['referraldriver'] . "'" ;

	$receiptno2 = empty($_REQUEST['receiptno2']) || !isset($_REQUEST['receiptno2']) ? 'NULL' :
		"'" . $_REQUEST['receiptno2'] . "'" ;

	$receiptno3 = empty($_REQUEST['receiptno3']) || !isset($_REQUEST['receiptno3']) ? 'NULL' :
		"'" . $_REQUEST['receiptno3'] . "'" ;

	$driverbackoutreason = empty($_REQUEST['driverbackoutreason']) || !isset($_REQUEST['driverbackoutreason']) ? 'NULL' :
		"'" . $_REQUEST['driverbackoutreason'] . "'" ;

	$trlicenseexpdate = empty($_REQUEST['trlicenseexpdate']) || !isset($_REQUEST['trlicenseexpdate']) ? 'NULL' :
		"'" . $_REQUEST['trlicenseexpdate'] . "'" ;	

	$receiptdate2 = empty($_REQUEST['receiptdate2']) || !isset($_REQUEST['receiptdate2']) ? 'NULL' :
		"'" . $_REQUEST['receiptdate2'] . "'" ;	

	$receiptdate3 = empty($_REQUEST['receiptdate3']) || !isset($_REQUEST['receiptdate3']) ? 'NULL' :
		"'" . $_REQUEST['receiptdate3'] . "'" ;	

	
	
	$privateverification = empty($_REQUEST['privateverification']) || !isset($_REQUEST['privateverification']) ? 0 : $_REQUEST['privateverification'];
	
	$policeverification = empty($_REQUEST['policeverification']) || !isset($_REQUEST['policeverification']) ? 0 : $_REQUEST['policeverification'];
	
	$lastsyncdatetime = empty($_REQUEST['lastsyncdatetime']) || !isset($_REQUEST['lastsyncdatetime']) ? 'NULL' :
		"'" . $_REQUEST['lastsyncdatetime'] . "'" ;	
  
	$sql = "call insert_driver_profile_data(" .$clientdatetime ."," .$uniquekey ."," .$driverfirstname .",  " .$driverlastname .
	","  .$driverdob .","  .$driveremail . "," .$driverphoneno . "," .$drivergender . "," .$driverpancard .", " .$driveraadhar .", " .$driverlicenseno .
	", ".$driverpermiflag . "," .$driverpermitno ."," .$driverbadgeflag . "," .$driverbadgeno .", ". $driverincitysince .
	",".$cabneededflag.", ".$drivervehicleno.", ".$remark . ", ".$internalcomment.", ". $googleaddress.", " .$taddress1.", ". $taddress2 . ", ".$tcitytown.", ".$tlandmark.
	", ".$tstate.", ".$tpincode.", ".$paddress1.", " .$paddress2.", " .$pcitytown. ", " .$plandmark. ", " .$pstate. ", " .$ppincode.
	", " .$locality .", " .$sublocality . ", ".$postalcode . "," .$route .
	"," .$neighborhood . "," .$administrative_area_level_2 . ",". $administrative_area_level_1 .
	",".$latitude.", ".$longitude . ", ".$profilesubmitted . "," .$uniqueid .", ". $paymentstatus .
	",".$paymentamount.", ".$receiptno . ", ".$receiptdate . ", ".$referralwalkin .", ".$referralsupervisor .
	", ".$referraldriver .", ".$receiptno2 .", ".$receiptno3 .", ".$driverbackoutreason . 
	", ".$trlicenseexpdate . ", ".$receiptdate2 . ", ".$receiptdate3 .
	", ".$appuserid . ", ".$privateverification  . ", ".$policeverification . ", ".$lastsyncdatetime . ")";
	
	
	if ($verbose != 'N') {
		echo '<br>' . $sql . '<br>';
	}
	$result = $mysqli->query($sql);
	if(is_object($result)) {	
		while ($row = $result->fetch_assoc()) {
			echo json_encode($row);
			break;
		}
		
	}	
	$mysqli->close();
} else {
	echo "-1";
}