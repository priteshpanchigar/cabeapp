<?php	
error_reporting(E_ALL);
if(empty($php_name)) {
	$php_name = "GenericPhp";
}
$verbose = isset($_REQUEST['verbose']) ? $_REQUEST['verbose'] : 'N';
$actual_link = "http://$_SERVER[HTTP_HOST]";
$fname = isset($_REQUEST['e']) ? $_REQUEST['e']  : "genericemail";
if(empty($fname)) {
	$fname = isset($_REQUEST['i']) ? $_REQUEST['i']  : "genericemail";
}
$actual_link = "http://$_SERVER[HTTP_HOST]" . $_SERVER['REQUEST_URI'];
$fp = fopen('log/' . $php_name . "_" . $fname . '.log', 'a');
$delim = "?";
$params = "";
foreach ($_POST as $param_name => $param_val) {
	$params = $params . $delim . $param_name . '=' . $param_val;
	$delim = "&";
}
$actual_link = $actual_link . $params . "&verbose=Y\n";
fwrite($fp, $actual_link);
fclose($fp);

//Key Data
$appuserid = isset($_REQUEST['i']) ?
$_REQUEST['i'] : 'NULL';

//$countrycode = empty($_REQUEST['c']) || !isset($_REQUEST['c']) ? 'NULL' : $_REQUEST['c'];
$countrycode = isset($_REQUEST['c']) ?
"\"" . (strpos($_REQUEST['c'], "+") !== false ? "" : "") . trim($_REQUEST['c']) . "\"" : 'NULL';
$phoneno = isset($_REQUEST['ph']) ?
"\"" . $_REQUEST['ph'] . "\"" : 'NULL';

//Location Data	
$lat = isset($_REQUEST['l']) ? "\"" . $_REQUEST['l'] . "\"" : 'NULL';

$lng = isset($_REQUEST['o']) ? "\"" . $_REQUEST['o'] . "\"" : 'NULL';
$accuracy = isset($_REQUEST['a']) ? 	"\"" . $_REQUEST['a'] . "\"" : 'NULL';		
$locationdatetime = isset($_REQUEST['lt']) ? 
"\"" . $_REQUEST['lt'] . "\"" : 'NULL';
$provider = isset($_REQUEST['p']) ?
"\"" . $_REQUEST['p'] . "\"" : 'NULL';
$speed = isset($_REQUEST['s']) ? "\"" . $_REQUEST['s'] . "\"" : 'NULL';
$bearing = isset($_REQUEST['b']) ? "\"" . $_REQUEST['b'] . "\"" : 'NULL';
$altitude  = isset($_REQUEST['al']) ?
"\"" . $_REQUEST['al'] . "\"" : 'NULL';

//client access date time
$clientdatetime = isset($_REQUEST['ct']) ? "\"" . $_REQUEST['ct'] . "\"" : 'NULL';	

//identification data
$imei = isset($_REQUEST['im']) ? "\"" . $_REQUEST['im'] . "\"" : 'NULL';

$email = empty($_REQUEST['e']) || !isset($_REQUEST['e']) ? 'NULL' : "'" . $_REQUEST['e'] . "'" ;

//phone data
$date = isset($_REQUEST['d']) ? 	"\"" . $_REQUEST['d'] . "\"" : 'NULL';
$time = isset($_REQUEST['t']) ? 	"\"" . $_REQUEST['t'] . "\"" : 'NULL';

$carrier = isset($_REQUEST['cr']) ?  "\"" . $_REQUEST['cr'] . "\"" : 'NULL';
$product = isset($_REQUEST['p']) ? "\"" . $_REQUEST['p'] . "\"" : 'NULL';
$manufacturer = isset($_REQUEST['m']) ?
"\"" . $_REQUEST['m'] . "\"" : 'NULL';			

//app data
$app = isset($_REQUEST['ap']) ? "\"" . $_REQUEST['ap'] . "\"" : 'NULL';
$version = isset($_REQUEST['v']) ?
	"\"" . $_REQUEST['v'] . "\"" : 'NULL';
$versioncode = isset($_REQUEST['vc']) ?
	"\"" . $_REQUEST['vc'] . "\"" : 'NULL';
$module = isset($_REQUEST['md']) ?
	"\"" . $_REQUEST['md'] . "\"" : 'NULL';


//ip data
$ip = isset($_SERVER['REMOTE_ADDR']) ?
"\"" . $_SERVER['REMOTE_ADDR'] . "\"" : 'NULL';
$useragent = isset($_SERVER['HTTP_USER_AGENT']) ?
"\"" . $_SERVER['HTTP_USER_AGENT'] . "\"" : 'NULL';

//miscellanious data	
$address = isset($_REQUEST['ad']) ? 
"\"" . $_REQUEST['ad'] . "\"" : 'NULL';

// Grouping Data

$key_data = $countrycode . "," . $phoneno;
$location_data = $lat . "," . $lng . "," . $accuracy . "," .
$locationdatetime . "," . $provider . "," .
$speed . "," . $bearing . "," . $altitude;
$identification_data = $email . "," . $imei;
$phone_data =  $carrier . "," . $product . "," . 
$manufacturer;
$app_data = $app . "," . $version . "," . $versioncode . "," . $module;
$ip_data = $ip . "," . $useragent;	

$mobile_common_data = $key_data . "," . $clientdatetime . "," . 
$email . "," . 	$location_data . "," . $imei . "," .
$phone_data . "," . $app_data . "," . $ip_data;

if ($verbose != "N") {
	echo "<br>App user id: " . $appuserid . "<br>";
}
?>