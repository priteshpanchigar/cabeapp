<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'CarsDeviceLocation';
include("dbconn_sar_apk.php");
include("mobile_common_data_short.php");
if ($mysqli) {
	$locationRows = array();
	 	
    $sql = " call get_cars_device_location()";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$locationRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$locationRows = array_filter($locationRows);
		if (!empty($locationRows)) {
			echo json_encode($locationRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}