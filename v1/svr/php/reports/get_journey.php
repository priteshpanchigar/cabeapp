<?php header('Access-Control-Allow-Origin: *');
$php_name = "GetJourney";
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$tripJourneyRows = array();
	
	$appuserid = isset($_REQUEST['appuserid']) ?
		$_REQUEST['appuserid'] : 'NULL';
    $sql = " call get_journey(" .$tripid . "," .$triptype.  "," .$appuserid. ")";
	
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$tripJourneyRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$tripJourneyRows = array_filter($tripJourneyRows);
		if (!empty($tripJourneyRows)) {
			echo json_encode($tripJourneyRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}