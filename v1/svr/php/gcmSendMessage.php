<?php


function gcmSendMessage($resultrows, $jmessage, $title, $subtitle, $vibrate, $sound, $other, $notification_type, 
						$notification_category) 
	{
		
	$message = array
(
	'message' 	=> $jmessage,
	'title'		=> $title,
	'subtitle'	=> $subtitle,
	'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
	'vibrate'	=> $vibrate,
	'sound'		=> $sound,
	'largeIcon'	=> 'large_icon',
	'smallIcon'	=> 'small_icon',
	'other'	=> $other,
	'notification_type'	=> $notification_type,
	'notification_category'	=> $notification_category
);
	
		
	/*	
	$message = array(
		"notification" => $jmessage
	);
	*/
	$fields = array(
		'registration_ids' => $resultrows,
		'data' => $message,
	);
	$headers = array(
		'Authorization: key=AIzaSyCjcGRMuRdhGKFAsr9A0SCG63RaTmFd_Zs',
		'Content-Type: application/json'
	);
	// Open connection
	$ch = curl_init();
	// Set the url, number of POST vars, POST data
	curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Disabling SSL Certificate support temporarly
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	// Execute post
	$result = curl_exec($ch);
	if ($result === FALSE)
		{
		die('Curl failed: ' . curl_error($ch));
		}
	// Close connection
	curl_close($ch);
	$verbose = isset($_REQUEST['verbose']) ? $_REQUEST['verbose'] : 'N';
	if ($verbose != 'N')
		{
		echo '<br />Result: ' . $result . '<br />';
		echo 'Result Rows: ' . json_encode($resultrows);
		}
	}
?>