<?php
class cMailParams
{
    public $Host, $Username, $Password, $FromEmail, $FromLabel, $ToEmail, $ToLabel, $ReplyTo, $Subject, $Message, $Cc, $Bcc;
    function __construct($server)
    {
        if ($server == "cabebooking") {
            $this->Host      = "mail.cabebooking.com"; // sets the SMTP server
            $this->Username  = "error@cabebooking.com"; // SMTP account username
            $this->Password  = "Tech@1234"; // SMTP account password
            $this->FromEmail = "error@cabebooking.com";
            $this->FromLabel = "cab-e";
            $this->ReplyTo   = "error@cabebooking.com";
            $this->Subject   = "From cab-e";
            $this->Message   = "Hello from cab-e";
        }
        
    }
    public function GetHost()
    {
        return $this->Host;
    }
    public function GetUsername()
    {
        return $this->Username;
    }
    public function GetPassword()
    {
        return $this->Password;
    }
    public function GetFromEmail()
    {
        return $this->FromEmail;
    }
    public function GetFromLabel()
    {
        return $this->FromLabel;
    }
    public function GetToEmail()
    {
        return $this->ToEmail;
    }
    public function GetToLabel()
    {
        return $this->ToLabel;
    }
    public function GetReplyTo()
    {
        return $this->ReplyTo;
    }
    public function GetSubject()
    {
        return $this->Subject;
    }
    public function GetMessage()
    {
        return $this->Message;
    }
    public function GetCc()
    {
        return $this->Cc;
    }
    public function GetBcc()
    {
        return $this->Bcc;
    }   
    
    
    
    public function SetFrom($fe, $fl)
    {
        $this->FromEmail = $fe;
        $this->FromLabel = $fl;
    }
    public function setTo($te, $tl)
    {
        $this->ToEmail = $te;
        $this->ToLabel = $tl;
    }
    public function setUserName($un)
    {
        $this->Username = $un;
    }
    public function setReplyTo($re)
    {
        $this->ReplyTo = $re;
    }
    public function setSubject($sb)
    {
        $this->Subject = $sb;
    }
    public function setMessage($sm)
    {
        $this->Message = $sm;
    }
    public function SetCc($Cc)
    {
        $this->Cc = $Cc;
    }
    public function SetBcc($Bcc)
    {
        $this->Bcc = $Bcc;
    }    
}
?>