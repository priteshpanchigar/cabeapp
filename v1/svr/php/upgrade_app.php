<?php 
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'upgradeApp';
include("mobile_common_data_sar.php");
include("variables.php");
include("gcmSendMessage.php");
$resultrows  = array();
$basemessage = "";
if ($versioncode == -1) {
	echo 'Please enter the current versioncode for app as parameter';
	return;
}include("dbconn_sar_apk.php");
if ($mysqli) {
	$sql = "call upgrade_app (" . $app . "," . $versioncode .  "," . $appuserid .")";
	if ($verbose != 'N') {
		echo $sql . '<br>';
	}
	$result = $mysqli->query($sql);
    if (is_object($result)) {
        if ($result) {
            $rowcount = mysqli_num_rows($result);
            if ($rowcount == 0) {
                echo '';
            }
            while ($row = $result->fetch_assoc()) {
                array_push($resultrows, $row['gcm_registration_id']);
					
				echo json_encode($row);
			
				$basemessage = ", \"message\":\"";
				if ($verbose != 'N') {	
					echo "basemessage : ". $basemessage . "<br>";
				}
				
				$messagetopost = 'Please Upgrade App';
		
				$jmessage = $basemessage . $messagetopost . "\"}";
				if ($verbose != 'N') {
					echo "jmessage : $jmessage" . "<br>";
				}
				$title =  "Cab-e";
				
				gcmSendMessage($resultrows, $messagetopost, $title, '', 1, 1, $row);              
                break;
            }            
        } else {
            echo "-1"; // something went wrong, probably sql failed
        }
    }
    $mysqli->close();
} else {
    echo "-2"; // "Connection to db failed";
}