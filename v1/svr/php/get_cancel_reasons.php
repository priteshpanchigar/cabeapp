<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getCancelReasons';
include("dbconn_sar_apk.php");
include("mobile_common_data_short.php");
if ($mysqli) {
	$cancelRows = array();
	 
	$canceluser = empty($_REQUEST['canceluser']) || !isset($_REQUEST['canceluser']) ? 'NULL' :
		"'" . $_REQUEST['canceluser'] . "'" ;
	
    $sql = " call get_cancel_reasons(" . $canceluser . ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$cancelRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$cancelRows = array_filter($cancelRows);
		if (!empty($cancelRows)) {
			echo json_encode($cancelRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}