<?php
//echo  $_SERVER['DOCUMENT_ROOT'];
include("../cMailParams.php");

require($_SERVER['DOCUMENT_ROOT'] . "/assets/php/PHPMailer/PHPMailerAutoload.php");
//require ("/assets/php/PHPMailer/PHPMailerAutoload.php");
//This PHP script takes email id's for the person to whom, the mail is to be sent, mail id
// of the person, who will be sending this mail, cc email id, subject of the mail and the
// email message as input parameters. and checks whether mail is sent successfully .

//function sendMail($MailParams) {
function sendMail($MailParams)
{
    
    $mail = new PHPMailer();
    $mail->IsSMTP(); // telling the class to use SMTP
    $mail->SMTPAuth = true; // enable SMTP authentication
    $mail->Host     = $MailParams->GetHost(); // sets the SMTP server
    $mail->Port     = 25; // set the SMTP port for the GMAIL server
    $mail->Username = $MailParams->GetUsername(); // SMTP account username
    $mail->Password = $MailParams->GetPassword(); // SMTP account password
    $mail->SetFrom($MailParams->GetFromEmail(), $MailParams->GetFromLabel());
    $mail->AddReplyTo($MailParams->GetReplyTo());
    $mail->Subject = $MailParams->GetSubject();
    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
    $mail->MsgHTML($MailParams->GetMessage());
    $mail->AddAddress($MailParams->GetToEmail(), $MailParams->GetToLabel());

    $cc = $MailParams->GetCc();
    if (!empty($cc)) {
        $arr = explode(",", $cc);
        foreach ($arr as $ccemail) {
            $mail->addCC($ccemail);
        }
    }
    $bcc = $MailParams->GetBcc();
    if (!empty($bcc)) {
        $arr = explode(",", $bcc);
        foreach ($arr as $bccemail) {
            $mail->addBCC($bccemail);
        }
    }
    
    if (!$mail->Send()) {
        echo "-1 failed";
        return -1;
    } else {
        echo "1";
        return 0;
    }
}