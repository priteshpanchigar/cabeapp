<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'weeklyHistory';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$weeklyhistoryRows = array();
	$sql = " call weekly_history(" . $appuserid . "," . $fromdatetime .  "," . $todatetime .")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$weeklyhistoryRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$weeklyhistoryRows = array_filter($weeklyhistoryRows);
		if (!empty($weeklyhistoryRows)) {
			echo json_encode($weeklyhistoryRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}