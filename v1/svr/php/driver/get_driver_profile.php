<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getDriverProfile';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
    
    $sql = " call get_driver_profile(" . $appuserid . ")";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
    $result = $mysqli->query($sql);
    if (is_object($result)) {
        if ($result) {
            $rowcount = mysqli_num_rows($result);
            if ($rowcount == 0) {
                echo '';
            }
            while ($row = $result->fetch_assoc()) {
                echo json_encode($row);                
                break;
            }            
        } else {
            echo "-1"; // something went wrong, probably sql failed
        }
    }
    $mysqli->close(); // close connection
}