<?php
error_reporting(E_ALL);
header('Access-Control-Allow-Origin: *');
$php_name = 'TripActionDriver';
include ("../dbconn_sar_apk.php");
include ("../mobile_common_data_short.php");
include ("../variables.php");
include ("../gcmSendMessage.php");
$resultrows = array();
$basemessage = "";
$notification_category = "D";
if ($mysqli) {
	$resultrows = array();
	$notification_type = 'T' . $intrip_action;
	$intrip_action = empty($_REQUEST['tripaction']) || !isset($_REQUEST['tripaction']) ? 'NULL' : $_REQUEST['tripaction'];

	$sql = " call trip_action_driver(" . $appuserid . ",'" . $intrip_action . "'," . $triptype . ", " . $clientdatetime . "," . 									$tripid . "," . $location_data . "); ";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
			$hasResult = true;
			array_push($resultrows, $row['gcm_registration_id']);
				   $trip_action_time = $row['trip_action_time'];
			$notification_type = 'TA_' . $intrip_action;
			
			
			$basemessage = "\"trip_action_time\":" . "'" . $trip_action_time . "'" .
						   ", \"notification_category\":" ."'". $notification_category . "'" .
						   ", \"notification_type\":" ."'". $notification_type . "'" .
						   ", \"message\":\"";
			if ($verbose = 'Y') {
				echo "<br />" . $basemessage . "<br />";
				echo $intrip_action . "<br />";
			}
			if ($intrip_action == 'B') {
				$messagetopost = "Trip has Started";
			}
			else if ($intrip_action == 'P') {
				$messagetopost = "Trip has been Paused";
			}
			else if ($intrip_action == 'R') {
				$messagetopost = "Trip has been Resumed";
			}
			else if ($intrip_action == 'E') {
				$messagetopost = "Thank You for riding with us";
			}
			else if ($intrip_action == 'A') {
				$messagetopost = "Trip Has Been Cancelled";
			}
			$jmessage = $basemessage . $messagetopost . "\"}";
			if ($verbose = 'Y') {
				echo "Message : $jmessage" . "<br />";
			}
			$title = "Cab-e";
			gcmSendMessage($resultrows, $messagetopost, $title, '', 1, 1, $row, $notification_type,$notification_category);
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$resultrows = array_filter($resultrows);
		if (!empty($resultrows)) {
			echo json_encode($resultrows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}