<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'insertBookedBusy';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
    
    $sql = "CALL insert_booked_driver_busy(" . $appuserid . "," . $clientdatetime . "," . $tripid . ")";
    
    if ($verbose != 'N') {
        echo "sql: " . $sql . "<br>";
    }
    if ($result = $mysqli->query($sql)) {
        if ($verbose != 'N') {
            echo "<br>sql result: ";
            var_dump($result);
            echo "<br>";
        }
    }
    if ($result && is_object($result)) {
        $rowcount = mysqli_num_rows($result);
        if ($verbose != 'N') {
            echo "rowcount: " . $rowcount . "<br>";
        }
        if ($rowcount > 0) {
            while ($row = $result->fetch_assoc()) {                
                $resultrows[] = $row;
                echo json_encode($row);
                $tripid = empty($row["trip_id"]) ? '-1' : $row["trip_id"];
                break;
                
            }
        }
        $result->free();
    }
    $mysqli->close();
} else {
    echo -1; // something went wrong, probably sql failed
    $tripid = -1;
}