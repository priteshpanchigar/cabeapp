<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'getCabTripStatus';
include ("../mobile_common_data_short.php");
include ("../dbconn_sar_apk.php");
include ("../variables.php");
include ("../output_log.php");

if ($mysqli) {
	$cabStatusRows = array();
	$sql = " call get_cab_tripstatus(" . $tripid . "," . $triptype . ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$cabStatusRows[] = $row;
				output_log($php_name, $appuserid, json_encode($row));
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$cabStatusRows = array_filter($cabStatusRows);
		if (!empty($cabStatusRows)) {
			echo json_encode($cabStatusRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}
