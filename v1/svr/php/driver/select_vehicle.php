<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'SelectVehicle';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$vehicleRows = array();
	$sql = " call select_vehicle(" . $vehicleno . "," . $appuserid . ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$vehicleRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$vehicleRows = array_filter($vehicleRows);
		if (!empty($vehicleRows)) {
			echo json_encode($vehicleRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}