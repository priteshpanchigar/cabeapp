<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'driverHistory';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	$driverhistoryRows = array();
	$sql = " call driver_history(" . $appuserid . "," . $clientdatetime . ")";
	if ($verbose != 'N') {
		echo $sql . '<br />';
	}
	$result = $mysqli->query($sql);
	$hasResult = false;
	if (is_object($result)) {
		if ($result) {
			while ($row = $result->fetch_assoc()) {
				$hasResult = true;
				$driverhistoryRows[] = $row;
				
			}
			$result->free(); // free result set
		}
	}
	
	if ($hasResult) {
		$driverhistoryRows = array_filter($driverhistoryRows);
		if (!empty($driverhistoryRows)) {
			echo json_encode($driverhistoryRows);
		}
	}
	else {
		echo 0;
	}
	$mysqli->close(); // close connection
}
else {
	echo "-1";
}