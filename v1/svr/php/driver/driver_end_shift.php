<?php header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'EndShift';
include("../mobile_common_data_short.php");
include("../dbconn_sar_apk.php");
include("../variables.php");
if ($mysqli) {
	
	$sql = "call driver_end_shift("  . $vehicleid ."," .  $clientdatetime . 
		"," . $shiftid . "," . $appuserid . ")";
	
	if ($verbose != 'N') {
		echo $sql;
	}	
	$result = $mysqli->query($sql);
    if (is_object($result)) {
        if ($result) {
            $rowcount = mysqli_num_rows($result);
            if ($rowcount == 0) {
                echo '';
            }
            while ($row = $result->fetch_assoc()) {
                echo json_encode($row);                
                break;
            }            
        } else {
            echo "-1"; // something went wrong, probably sql failed
        }
    }
	$mysqli->close();
} else {
	echo "-2"; // "Connection to db failed";
}
