<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name   = "CreateTripDriver";
$resultrows = array();
$tripid = 0;
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
    
    date_default_timezone_set('Asia/Calcutta');
    $currentdate = date('m/d/Y h:i:s a', time());
    $timeinms    = strtotime($currentdate) * 1000;
    $inpath      = "NULL";
    
    $sql = "call create_trip_driver(" . $triptype . "," . $plannedstartdatetime . "," . $fromaddress . 
		"," . $fromshortaddress . "," . $toaddress . "," . $toshortaddress . 
		"," . $fromsublocality . "," . $tosublocality . "," . $trip_directions_polyline . 
		"," . $fromlat . "," . $fromlng . "," . $tolat . "," . $tolng . "," . $timeinms . 
		"," . $tripaction . "," . $triptime . "," . $tripdistance . "," . $appuserid . 
		"," . $clientdatetime . "," . $email . ")";
    
    if ($verbose != 'N') {
        echo "<br>Sub locality: " . $tosublocality . "<br>";
        echo $sql . "<br>";
    }
    $tripFound = 0;
    if ($result = $mysqli->query($sql)) {
        if ($row = $result->fetch_assoc()) {
            $resultrows[] = $row;
            $tripid       = $row['trip_id'];
            $tripFound    = $row['tripFound'];
            if ($verbose != 'N') {
                echo "<br>tripRow: ";
                var_dump($row);
                echo "<br>tripFound: " . $tripFound . "<br>";
            }
            echo json_encode($row);
            
        }
        $result->free();
    } else {
        echo -1; // something went wrong, probably sql failed
        $tripid = -1;
    }
    if ($verbose != 'N') {
        echo "<br>tripFound: " . $tripFound . "<br>";
    }
    $mysqli->close();
}