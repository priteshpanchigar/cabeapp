<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = "missingLatLng";
$resultrows = array();
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
if ($mysqli) {
	
	date_default_timezone_set('Asia/Calcutta');
	$currentdate = date('m/d/Y h:i:s a', time());
	$timeinms = strtotime($currentdate) * 1000;
	if(isset($_REQUEST['path']) && !empty($_REQUEST['path'])){
		$inpath = $_REQUEST['path'];
	}
	
	$tripid = isset($_REQUEST['tripid']) ? $_REQUEST['tripid'] : -1;
	
	// $path = json_decode($inpath)->path;
	$path = json_decode($inpath, true);
	$ctr = 0;
	$length = count($path);
	$valuessql = "";
	$insertsql = 'insert into location(appuser_id,  lat, lng, accuracy, 
				  loctime, provider, speed, bearing,altitude, clientaccessdatetime) values ';
	$delim = '';
	
		foreach($path as $obj) {
			if ($verbose != 'N') {
				echo  "<br>obj: ";
				var_dump($obj);
				echo  "<br>json obj: ";
				echo $obj['speed'];
				
			}
			$ctr = $ctr + 1; // call
			$lat = $obj['lat'];
			$lng = $obj['lng'];
			$accuracy = $obj['accuracy'];
			$locationdatetime = $obj['locationdatetime'];
			$provider = $obj['provider'];
			$speed = $obj['speed'];
			$bearing = $obj['bearing'];
			$altitude = $obj['altitude'];
			$cdt = $obj['current_datetime'];
			$valuessql =  $valuessql . $delim . '(' . $appuserid .  
			"," . $lat . "," . $lng . "," . $accuracy . ",'" .$locationdatetime . 
			"','" . $provider . "'," .$speed . "," . $bearing . "," . $altitude .  
			",'" . $cdt."')" ;
			$delim = ',';
		}
		if ($verbose != 'N') {
			echo "<br>Sub locality: " .$insertsql . $valuessql . "<br>";
		}		
		$runningsql = $insertsql . $valuessql;
		if ($runresult = $mysqli->query($runningsql)) {
			echo trim($tripid);
		} else {
			printf("Errormessage: %s\n", $mysqli->error);
			// echo -1; // something went wrong, probably sql failed
		}
		$mysqli->close();
} else {
	echo -2; ;
}