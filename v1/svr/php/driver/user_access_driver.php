<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'UserAccessDriver';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_sar.php");
if ($mysqli) {
    
    $sql = "call user_access_driver(" . $gcmregistration_id . 
		"," . $mobile_common_data . "," . $android_release_version . 
		"," . $android_sdk_version . "," . $verified . ")";
    if ($verbose != 'N') {
        echo '<br>sql; ' . $sql . '<br>';
    }
    
    if ($result = $mysqli->query($sql)) {
        if ($verbose != 'N') {
            echo "<br>sql result: ";
            var_dump($result);
            echo "<br>";
        }
    }
    if ($result && is_object($result)) {
        $rowcount = mysqli_num_rows($result);
        if ($verbose != 'N') {
            echo "rowcount: " . $rowcount . "<br>";
        }
        if ($rowcount > 0) {
            while ($row = $result->fetch_assoc()) {
                $resultrows[] = $row;
                echo json_encode($row);
                break;
            }
        }
        
    } else {
        echo "-1"; // something went wrong, probably sql failed
    }
    $mysqli->close();
} else {
    echo "-2"; // "Connection to db failed";
}
?>