<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'checkedIfBooked';
include("../dbconn_sar_apk.php");
include("../mobile_common_data_short.php");
include("../output_log.php");
include("../variables.php");

if ($mysqli) {
    
    $sql = "call check_if_booked(" . $appuserid . "," . $clientdatetime . "," . $location_data .  ")";
    if ($verbose != 'N') {
        echo $sql;
    }
    $result = $mysqli->query($sql);
    if (is_object($result)) {
        if ($result) {
            $rowcount = mysqli_num_rows($result);
            if ($rowcount == 0) {
                echo '';
            }
            while ($row = $result->fetch_assoc()) {
                output_log($php_name, $appuserid, json_encode($row));
                echo json_encode($row);                
                break;
            }            
        } else {
            echo "-1"; // something went wrong, probably sql failed
        }
    }
    $mysqli->close();
} else {
    echo "-2"; // "Connection to db failed";
}