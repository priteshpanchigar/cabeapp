<?php
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = 'jumpIn';
include("../dbconn_sar_apk.php");
include("../variables.php");
include("../mobile_common_data_short.php");
include("../gcmSendMessage.php");
$resultrows  = array();
$basemessage = "";
$notification_category = "J";
if ($mysqli) {
  
	$sql = "call jump_in(" . $appuserid . "," .$clientdatetime . 
		"," . $location_data . "," .$passengerappuserid . 
		"," . $tripid . "," . $jumpin . "," . $jumpout .")";
    if ($verbose != 'N') {
        echo $sql . '<br>';
    }
   $result = $mysqli->query($sql);
    if (is_object($result)) {
        if ($result) {
            $rowcount = mysqli_num_rows($result);
            if ($rowcount == 0) {
                echo '';
            }
            while ($row = $result->fetch_assoc()) {
				array_push($resultrows, $row['gcm_registration_id']);
				$fullname    = $row['fullname'];
				
				echo json_encode($row);
				$notification_type = '';
				$basemessage = "\"fullname\":" ."'". $fullname . "'" .
							   ", \"notification_category\":" ."'". $notification_category . "'" .
							   ", \"notification_type\":" ."'". $notification_type . "'" .
							   ", \"message\":\"";
				if ($verbose != 'N') {	
					echo $basemessage;
				}

				if ($jumpin == 1) {	
					$messagetopost = 'You have Jumped IN the car ';
				} else {
					$messagetopost = 'Your trip has ended';
				}
				
				if ($verbose = 'Y') {
					echo $messagetopost . "<br>";
				}
				$title = $jumpin == 1 ? "Jump in" : "Jump out";
				
				gcmSendMessage($resultrows, $messagetopost, $title, '', 1, 1, "", $notification_type, $notification_category);			
                
				break;
            }            
        } else {
            echo "-1"; // something went wrong, probably sql failed
        }
    }

    $mysqli->close();
				
} else {
    echo "-2"; // "Connection to db failed";
}