<?php
include('jtrack_variables.php');
header('Access-Control-Allow-Origin: *');
error_reporting(E_ALL);
$php_name = "gpsDevice";


function getAvailableCabs($lat, $lng, $radius)
{
	$url = 'http://vcare.co/wservices/services/jtrack.php';
	$api = '2b2451e3674bdf0ef6f93fb820d0e456';	
    $action_type = 'GET-CBF';
    $fields = array(
        'api_key' => urlencode($api),
        'action_type' => urlencode($action_type),
        'lat' => urlencode($lat),
        'lng' => urlencode($lng)
    );
   return runcurl($fields);
}

function addCompany($companyname, $companycode, $ccontactperson, $cmobile, $cemail)
{
	$url = 'http://vcare.co/wservices/services/jtrack.php';
	$api = '2b2451e3674bdf0ef6f93fb820d0e456';	
    $action_type = 'ADD-CMP';
    $fields = array(
        'api_key' => urlencode($api),
        'action_type' => urlencode($action_type),
        'cname' => urlencode($companyname),
		'ccode' => urlencode($companycode),
		'cperson_name' => urlencode($ccontactperson),
		'cperson_mobile' => urlencode($cmobile),
		'cperson_email' => urlencode($cemail)
        
    );
   return runcurl($fields);
}
function addfranchise($companyid, $branchname, $branchcode)
{
	$url = 'http://vcare.co/wservices/services/jtrack.php';
	$api = '2b2451e3674bdf0ef6f93fb820d0e456';	
    $action_type = 'ADD-BRN';
    $fields = array(
        'api_key' => urlencode($api),
        'action_type' => urlencode($action_type),
        'cid' => urlencode($companyid),
		'brname' => urlencode($branchname),
		'brcode' => urlencode($branchcode)
        
    );
   return runcurl($fields);
}

function adddriver($companyid, $branchid, $drivername, $drivermobile)
{
	$url = 'http://vcare.co/wservices/services/jtrack.php';
	$api = '2b2451e3674bdf0ef6f93fb820d0e456';	
    $action_type = 'ADD-DVR';
    $fields = array(
        'api_key' => urlencode($api),
        'action_type' => urlencode($action_type),
        'cid' => urlencode($companyid),
		'brid' => urlencode($branchid),
		'drname' => urlencode($drivername),
		'drmobno' => urlencode($drivermobile)
        
    );
   return runcurl($fields);
}

function addvehicle($companyid, $branchid, $drivervehicleno, $vehicleno, $gpsno, $modelname, 
					$vphone, $driverid)
{
	$url = 'http://vcare.co/wservices/services/jtrack.php';
	$api = '2b2451e3674bdf0ef6f93fb820d0e456';	
    $action_type = 'ADD-CAB';
    $fields = array(
        'api_key' => urlencode($api),
        'action_type' => urlencode($action_type),
        'cid' => urlencode($companyid),
		'brid' => urlencode($branchid),
		'vno' => urlencode($drivervehicleno),
		'vregno' => urlencode($vehicleno),
		'vgpsno' => urlencode($gpsno),
		'vmodel_name' => urlencode($modelname),
		'vmobno' => urlencode($vphone),
		'drid' => urlencode($driverid)
        
    );
		
		echo '<pre>addvehicle fields: ';
        var_dump($fields);
        echo '</pre>';
		
   return runcurl($fields);
}

function runcurl($fields)
{
	$url = 'http://vcare.co/wservices/services/jtrack.php';
    $fields_string = '';
    //url-ify the data for the POST
    foreach ($fields as $key => $value) {
        $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');
    $ch = curl_init();
    
    //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    $data = curl_exec($ch); // execute curl request curl_close($ch); 
    $json = json_decode($data, true);
    return $json;
}
?>


