DROP PROCEDURE IF EXISTS get_cancel_reasons;
DELIMITER $$
CREATE  PROCEDURE get_cancel_reasons(IN incanceluser VARCHAR (5))
BEGIN

IF incanceluser = 'K' THEN 
SELECT cancel_reason_id, cancel_reason, cancel_reason_code, cancel_reason_user FROM cancel_reason
WHERE cancel_reason_user = 'D'
UNION  
SELECT cancel_reason_id, cancel_reason, cancel_reason_code, cancel_reason_user FROM cancel_reason
WHERE cancel_reason_user = 'K';
ELSE
SELECT cancel_reason_id, cancel_reason, cancel_reason_code, cancel_reason_user FROM cancel_reason
WHERE cancel_reason_user = incanceluser; 
END IF;

END$$
DELIMITER;
CALL get_cancel_reasons('K');