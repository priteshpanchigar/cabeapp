DROP PROCEDURE IF EXISTS add_shared_passenger;
DELIMITER $$
CREATE  PROCEDURE add_shared_passenger(IN `inappuserid` INT, IN `intripid` INT, IN `injoinlat` DOUBLE,
	IN `injoinlng` DOUBLE, IN `injumpinaddress` VARCHAR(300), IN inclientdatetime datetime)
BEGIN

INSERT INTO passenger_shared_trip (passenger_appuser_id, trip_id, has_joined, join_lat, join_lng,
								   jumpin_address, has_joined_datetime)
VALUES(inappuserid, intripid, 1, injoinlat, injoinlng, injumpinaddress, inclientdatetime);

	SELECT bd.trip_id, bd.driver_appuser_id, vw.gcm_registration_id, ps.passenger_appuser_id, 
(SELECT firstname FROM vw_passenger_info WHERE appuser_id = inappuserid)passenger_name,
vw.image_path, vw.image_name, l.lat, l.lng,
	vw.email, vw.country_code, vw.phoneno driver_phoneno, 	vw.firstname, vw.lastname, 
	vehicleno, vw.vehicle_company,
	vehicle_category, vehicle_model, vehicle_type, vehicle_color,   service_code
	FROM booked_driver_shared bd	
	LEFT JOIN passenger_shared_trip ps
		ON bd.trip_id = ps.trip_id
	INNER  JOIN vwcommercialvehicle vw
		ON vw.appuser_id = bd.driver_appuser_id
	INNER JOIN last_location l
		ON l.appuser_id = bd.driver_appuser_id
	WHERE ps.trip_id = intripid 
	AND ps.passenger_appuser_id = inappuserid;


END$$
DELIMITER;


call add_shared_passenger(1,27,19.10461,72.850418,'Smruti, Paranjape Nagar, Vile Parle, Mumbai, Maharashtra 400057 ',"2016-11-11 11:26:53");