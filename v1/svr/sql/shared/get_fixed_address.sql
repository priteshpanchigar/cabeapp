DROP PROCEDURE IF EXISTS get_fixed_address;
DELIMITER $$
CREATE  PROCEDURE get_fixed_address()
BEGIN

SELECT fixed_address_id, area, landmark, pick_drop_point, formatted_address, locality,
sublocality, postal_code, route, neighborhood, administrative_area_level_2, 
administrative_area_level_1, latitude, longitude FROM fixed_address;

END$$
DELIMITER;
CALL get_fixed_address();