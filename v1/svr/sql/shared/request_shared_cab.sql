DROP PROCEDURE IF EXISTS request_shared_cab;
DELIMITER $$
CREATE  PROCEDURE request_shared_cab(IN `inappuserid` INT, IN `infromlat` DOUBLE, IN `infromlng` DOUBLE,
	IN `intolat` DOUBLE, IN `intolng` DOUBLE, IN inclientdatetime datetime)
BEGIN


SET @inDistance = 0.5;


SELECT trip_id, triptype, trip_action, planned_start_datetime,
trip_start_time, trip_end_time, appuser_id, fromaddress, 
toaddress,  fromsublocality, tosublocality, fromlat, fromlng, tolat, tolng, trips_unique_ms,
planned_start_time, something_went_wrong, triptime, tripdistance, trip_directions_polyline  FROM trips 
	WHERE  trip_action = 'C' 
		AND triptype = 'P' 
		AND ABS(intolat - tolat) < @inDistance / 100 
		AND ABS(intolng - tolng) < @inDistance / 100 
		AND ABS(infromlat - fromlat) < @inDistance / 100 
		AND ABS(infromlng - fromlng) < @inDistance / 100 
		LIMIT 1;

END$$
DELIMITER;

CALL request_shared_cab(1,19.174289, 72.860405, 19.055715, 72.85109, '2016-09-27 17:39:41');
