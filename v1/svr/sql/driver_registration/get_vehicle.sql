
DROP PROCEDURE IF EXISTS get_vehicle;
DELIMITER $$
CREATE PROCEDURE `get_vehicle`(IN inuniqueid varchar(15), IN insearchval VARCHAR(200))
BEGIN
SET @sel = 'SELECT driver_vehicle_id, company_id, unique_id, vehicleno_state
vehicleno_city, vehicleno_code, vehicleno, vehicle_company, vehicle_category
vehicle_model, vehicle_type, vehicle_color, service_code, rate_per_km
seating, is_booked, vehicle_distinctive_marks, date_registered, minimum_fare ';


#init vars
SET @whr = NULL;

IF inuniqueid IS NOT NULL THEN
	SET @whr = CONCAT('unique_id = \'', inuniqueid,'\'');
ELSE
	IF insearchval IS NOT NULL  THEN
		SET @whr = CONCAT('unique_id  LIKE "%', insearchval, '%"',
											'OR vehicleno_state LIKE "%', insearchval, '%"',
											'OR vehicleno_city LIKE "%', insearchval, '%"',
											'OR vehicleno_code LIKE "%', insearchval, '%"',
											'OR vehicle_company LIKE "%', insearchval, '%"',
											'OR vehicle_category LIKE "%', insearchval, '%"',
											'OR vehicle_model LIKE "%', insearchval, '%"',
											'OR vehicle_type LIKE "%', insearchval, '%"',
											'OR vehicle_color LIKE "%', insearchval, '%"',
											'OR service_code LIKE "%', insearchval, '%"',
											'OR vehicleno LIKE "%', insearchval, '%"');
	END IF;
END IF;




SET @orderby = '\n ORDER BY driver_vehicle_id DESC';
SET @dynquery = CONCAT(@sel, 'FROM driver_vehicle \n',
									IFNULL(CONCAT(' WHERE ', @whr),''),
									IFNULL(@orderby,''));

#SELECT @dynquery;
PREPARE stmt FROM @dynquery; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

END$$
DELIMITER;

call get_vehicle('tewew', NULL)