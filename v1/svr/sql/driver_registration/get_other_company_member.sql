
DROP PROCEDURE IF EXISTS get_other_company_member;
DELIMITER $$
CREATE  PROCEDURE `get_other_company_member`(IN indriverprofileid INT(11))
BEGIN
	SELECT other_company_code, driver_other_company_id FROM  driver_other_member
	WHERE driver_profile_id = indriverprofileid ;

END$$
DELIMITER;
