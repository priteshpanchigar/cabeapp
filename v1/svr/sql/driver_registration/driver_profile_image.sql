DROP PROCEDURE IF EXISTS driver_image;
DELIMITER $$
CREATE  PROCEDURE `driver_image`( IN indriverprofileid INT, 
IN inimagename varchar(255),  IN inimagetype varchar(20), IN inclientdatetime datetime,
IN inimagepath varchar(255))

quit_proc: BEGIN
IF inimagename IS NULL THEN
LEAVE quit_proc;
END IF;

INSERT INTO driver_image( driver_profile_id, image_name, image_type, image_createion_datetime, 
	image_path)
VALUES (indriverprofileid, inimagename, inimagetype, inclientdatetime,
	inimagepath)
ON DUPLICATE KEY UPDATE
		image_name = COALESCE( inimagename, image_name),
		image_createion_datetime = COALESCE( inclientdatetime, image_createion_datetime),
		image_path = COALESCE(inimagepath, image_path);
		
SELECT LAST_INSERT_ID() driver_image_id;

END$$
DELIMITER;

