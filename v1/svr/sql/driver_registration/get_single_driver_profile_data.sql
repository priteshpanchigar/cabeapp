DROP PROCEDURE IF EXISTS get_single_driver_profile_data;
DELIMITER $$
CREATE  PROCEDURE `get_single_driver_profile_data`(IN indriverprofileid INT)

BEGIN

SELECT driver_profile_id, unique_key, clientdatetime, unique_id, payment_status, payment_amount, receipt_no, receipt_date,
 driver_firstname, driver_lastname, driver_dob, driver_email, driver_phoneno, driver_gender, 
driver_pancard, driver_aadhar, driver_license_no, driver_permit_flag, driver_permit_no, driver_badge_flag, driver_badge_no, 
driver_incity_since, cab_needed_flag, remark, internal_comment, google_address, t_address_1, t_address_2, t_city_town,
t_landmark, t_state, t_pincode, p_address_1, p_address_2, p_city_town, p_landmark, p_state, p_pincode,
other_company_code, driver_other_company_id, driver_vehicle_no, other_company_name FROM 
vw_driver_profile_data
WHERE driver_profile_id = indriverprofileid ;




END$$
DELIMITER;
call get_single_driver_profile_data(1);