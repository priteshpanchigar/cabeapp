
DROP PROCEDURE IF EXISTS get_driver;
DELIMITER $$
CREATE PROCEDURE `get_driver`(IN inphoneno varchar(15), IN insearchval VARCHAR(200), IN inuniqueid VARCHAR(200))
BEGIN
SET @sel = 'SELECT dp.driver_profile_id, dp.unique_key, 
dp.clientdatetime, dp.unique_id, payment_status, payment_amount, receipt_no, receipt_date,  
driver_firstname, driver_lastname, driver_dob, driver_email, driver_phoneno,driver_gender,
driver_pancard, driver_aadhar , driver_license_no, driver_permit_flag, driver_permit_no, 
driver_badge_flag,  driver_badge_no, driver_incity_since,cab_needed_flag,
remark, internal_comment, t_address_1, t_address_2, t_city_town, t_landmark,
t_state, t_pincode, p_address_1, p_address_2, p_city_town, p_landmark, 
p_state, p_pincode, driver_vehicle_no ,
(SELECT image_name FROM driver_image im WHERE im.driver_profile_id = dp.driver_profile_id and image_type = "Driver" )driver_image_name,
(SELECT image_name FROM driver_image im WHERE im.driver_profile_id = dp.driver_profile_id and image_type = "PanCard" )pan_image_name,
(SELECT image_name FROM driver_image im WHERE im.driver_profile_id = dp.driver_profile_id and image_type = "AadharCard" )aadhar_image_name,
(SELECT image_name FROM driver_image im WHERE im.driver_profile_id = dp.driver_profile_id and image_type = "License" ) license_image_name ';


#init vars
SET @whr = NULL;

IF inphoneno IS NOT NULL THEN
	SET @whr = CONCAT('dp.driver_phoneno = ', inphoneno);
END IF;
IF insearchval IS NOT NULL  THEN
		SET @whr = CONCAT('driver_firstname  LIKE "%', insearchval, '%"',
											'OR driver_lastname LIKE "%', insearchval, '%"',
											'OR driver_phoneno LIKE "%', insearchval, '%"',
											'OR unique_id LIKE "%', insearchval, '%"',
											'OR driver_license_no LIKE "%', insearchval, '%"');
END IF;
IF inuniqueid IS NOT NULL THEN
	SET @whr = CONCAT('dp.unique_id = \'', inuniqueid,'\'');
END IF;





SET @groupby = '\n GROUP BY dp.driver_profile_id';
SET @orderby = '\n ORDER BY dp.driver_profile_id';
SET @dynquery = CONCAT(@sel, 'FROM driver_profile dp
															 LEFT JOIN driver_address da ON dp.driver_profile_id = da.driver_profile_id
														   LEFT JOIN driver_other_member  om
															 ON dp.driver_profile_id = om.driver_profile_id\n',
									IFNULL(CONCAT(' WHERE ', @whr),''),
									IFNULL(@groupby,''), 
									IFNULL(@orderby,''));

#SELECT @dynquery;
PREPARE stmt FROM @dynquery; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

END$$
DELIMITER;

call get_driver(NULL, '71',NULL);