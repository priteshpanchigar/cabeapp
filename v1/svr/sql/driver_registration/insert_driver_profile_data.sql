DROP PROCEDURE IF EXISTS insert_driver_profile_data;
DELIMITER $$
CREATE  PROCEDURE `insert_driver_profile_data`(IN inclientdatetime datetime, IN inuniquekey varchar(30),
IN indriverfirstname varchar(50),IN indriverlastname varchar(50), 	IN indriverdob date,
IN indriveremail varchar(50), 	 IN indriverphoneno varchar(50),	IN indrivergender varchar(10),
IN indriverpancard varchar(15),  IN indriveraadhar varchar(20),    	IN indriverlicenseno varchar(50), 
IN indriverpermiflag varchar(2), IN indriverpermitno varchar(50), 	IN indriverbadgeflag varchar(2), 
IN indriverbadgeno varchar(50),  IN indriverincitysince varchar(5), IN incabneededflag varchar(2),
IN indrivervehicleno varchar(20),IN inremark varchar(500), 		 	IN ininternalcomment varchar(500),
IN ingoogleaddress varchar(250), IN intaddress1 varchar(250), 	 	IN intaddress2 varchar(250), 		
IN intcitytown varchar(20), 	 IN intlandmark varchar(50), 	 	IN intstate varchar(20), 			
IN intpincode INT (11), 		 IN inpaddress1 varchar(250), 	 	IN inpaddress2 varchar(250), 		
IN inpcitytown varchar(20), 	 IN inplandmark varchar(50), 	 	IN inpstate varchar(20), 			
IN inppincode INT (11),			 IN inlocality varchar(255), 	 	IN insublocality varchar(255),		
IN inpostal_code varchar(255),   IN inroute varchar(255), 		 	IN inneighborhood varchar(255),	
IN inadministrative_area_level_2 varchar(255), 						IN inadministrative_area_level_1 varchar(255),
IN inlatitude double,			 IN inlongitude double, 			IN inprofilesubmitted INT (5),
IN inuniqueid  varchar(250),	 IN inpaymentstatus INT (1), 		IN inpaymentamount INT (11),
IN inreceiptno varchar(250),	 IN inreceiptdate date,
IN inreferralwalkin varchar	(255), IN inreferralsupervisor varchar	(255), IN inreferraldriver varchar	(255), 
IN inreceiptno2 varchar	(255), IN inreceiptno3 varchar	(255), IN  indriverbackoutreason varchar	(500),
IN intrlicenseexpdate date, IN  inreceiptdate2 date, IN  inreceiptdate3 date,
IN inappuserid INT, IN inprivateverification  INT, IN inpoliceverification INT, IN inlastsyncdatetime datetime)
BEGIN

DECLARE vDriverprofileId INT;
DECLARE vaddress_id int;

SELECT address_id INTO vaddress_id from address
WHERE formatted_address = ingoogleaddress LIMIT 1;

IF vaddress_id IS NULL AND ingoogleaddress IS NOT NULL  THEN
		INSERT INTO address(formatted_address, locality, sublocality,postal_code,route,
			neighborhood,administrative_area_level_2,administrative_area_level_1,
			clientdatetime,latitude,longitude)
		VALUES(ingoogleaddress,inlocality, insublocality, inpostal_code, inroute,
			inneighborhood,inadministrative_area_level_2, inadministrative_area_level_1,
		inclientdatetime, inlatitude, inlongitude);
 SELECT LAST_INSERT_ID() INTO vaddress_id from address;
ELSE
	UPDATE address
		SET locality = inlocality, sublocality = insublocality, postal_code = inpostal_code,
				route = inroute,  neighborhood = inneighborhood, administrative_area_level_2 = inadministrative_area_level_2,
				administrative_area_level_1 = inadministrative_area_level_1
					WHERE formatted_address = ingoogleaddress;
END IF;

INSERT INTO driver_profile( unique_key ,clientdatetime, driver_firstname, driver_lastname, 
driver_dob, driver_email, driver_phoneno,
driver_gender, 	driver_pancard, driver_aadhar, driver_license_no, driver_permit_flag, driver_permit_no,
driver_badge_flag, driver_badge_no, driver_incity_since, cab_needed_flag, driver_vehicle_no,
remark, internal_comment, profile_submitted, unique_id, payment_status, payment_amount, receipt_no, receipt_date,
referral_walk_in, referral_supervisor, referral_driver, receipt_no_2, receipt_no_3, driver_backout_reason,
tr_license_exp_date, receipt_date_2, receipt_date_3, appuser_id, private_verification, police_verification,
last_sync_datetime)
VALUES( inuniquekey, inclientdatetime, indriverfirstname, indriverlastname, 
indriverdob, indriveremail, indriverphoneno, indrivergender,
indriverpancard, indriveraadhar, indriverlicenseno, indriverpermiflag, indriverpermitno, 
indriverbadgeflag, indriverbadgeno, indriverincitysince, incabneededflag, indrivervehicleno, inremark,
ininternalcomment, inprofilesubmitted, inuniqueid, inpaymentstatus, inpaymentamount, inreceiptno, inreceiptdate,
inreferralwalkin, inreferralsupervisor, inreferraldriver, inreceiptno2, inreceiptno3, indriverbackoutreason,
intrlicenseexpdate, inreceiptdate2, inreceiptdate3, inappuserid, inprivateverification, inpoliceverification,
inlastsyncdatetime)
	ON DUPLICATE KEY UPDATE
		clientdatetime = COALESCE( inclientdatetime, clientdatetime),
		driver_firstname = COALESCE( indriverfirstname, driver_firstname),
		driver_lastname = COALESCE( indriverlastname, driver_lastname),
		driver_dob = COALESCE( indriverdob, driver_dob),
		driver_email = COALESCE( indriveremail, driver_email),
		driver_gender = COALESCE( indrivergender, driver_gender),
		driver_pancard = COALESCE( indriverpancard, driver_pancard),
		driver_aadhar = COALESCE( indriveraadhar, driver_aadhar),
		driver_license_no = COALESCE( indriverlicenseno, driver_license_no),
		driver_permit_flag = COALESCE( indriverpermiflag, driver_permit_flag ),
		driver_permit_no = COALESCE( indriverpermitno, driver_permit_no ),
		driver_badge_flag = COALESCE( indriverbadgeflag, driver_badge_flag),
		driver_badge_no = COALESCE( indriverbadgeno, driver_badge_no),
		driver_incity_since = COALESCE( indriverincitysince, driver_incity_since),
		cab_needed_flag = COALESCE( incabneededflag, cab_needed_flag),
		driver_vehicle_no = COALESCE( indrivervehicleno, driver_vehicle_no),
		remark = COALESCE( inremark, remark),
		internal_comment = COALESCE(ininternalcomment, internal_comment),
		profile_submitted = COALESCE(inprofilesubmitted, profile_submitted),
		payment_status = COALESCE(inpaymentstatus, payment_status),
		payment_amount = COALESCE(inpaymentamount, payment_amount),
		receipt_no = COALESCE(inreceiptno, receipt_no),
		receipt_date = COALESCE(inreceiptdate, receipt_date),
		referral_walk_in = COALESCE(inreferralwalkin, referral_walk_in),
		referral_supervisor = COALESCE(inreferralsupervisor, referral_supervisor), 
		referral_driver = COALESCE(inreferraldriver, referral_driver), 
		receipt_no_2 = COALESCE(inreceiptno2, receipt_no_2),
		receipt_no_3 = COALESCE(inreceiptno3, receipt_no_3),
		driver_backout_reason = COALESCE(indriverbackoutreason, driver_backout_reason),
		tr_license_exp_date = COALESCE(intrlicenseexpdate, tr_license_exp_date), 
		receipt_date_2 = COALESCE(inreceiptdate2, receipt_date_2), 
		receipt_date_3 = COALESCE(inreceiptdate3, receipt_date_3),
		appuser_id = COALESCE(inappuserid, appuser_id), 
		private_verification = COALESCE(inprivateverification, private_verification), 
		police_verification = COALESCE(inpoliceverification, police_verification),
		last_sync_datetime = COALESCE(inlastsyncdatetime, last_sync_datetime);

	SELECT driver_profile_id FROM driver_profile 
		WHERE driver_phoneno = indriverphoneno INTO vDriverprofileId;
		
INSERT INTO driver_address(address_id, driver_profile_id,  t_address_1, t_address_2, t_city_town, t_landmark, t_state, t_pincode, p_address_1,
p_address_2, p_city_town,  p_landmark, p_state, p_pincode)
VALUES (vaddress_id, vDriverprofileId, intaddress1, intaddress2, intcitytown, intlandmark, intstate, intpincode, inpaddress1, 
inpaddress2, inpcitytown, inplandmark, inpstate, inppincode)
	ON DUPLICATE KEY UPDATE
	address_id = COALESCE( vaddress_id, address_id),
	t_address_1 = COALESCE( intaddress1, t_address_1),
	t_address_2 = COALESCE( intaddress2, t_address_2),
	t_city_town = COALESCE( intcitytown, t_city_town),
	t_landmark = COALESCE( intlandmark, t_landmark),
	t_state = COALESCE( intstate, t_state ),
	t_pincode = COALESCE( intpincode, t_pincode),
	p_address_1 = COALESCE( inpaddress1, p_address_1),
	p_address_2 = COALESCE(inpaddress2, p_address_2),
	p_city_town = COALESCE( inpcitytown, p_city_town), 
	p_landmark = COALESCE( inplandmark, p_landmark),
	p_state = COALESCE( inpstate, p_state),
	p_pincode = COALESCE( inppincode, p_pincode);
		


SELECT driver_profile_id, driver_phoneno FROM driver_profile
WHERE driver_phoneno = indriverphoneno;

END$$
DELIMITER;

call insert_driver_profile_data("2016-08-08 18:37:15",'1470657753440','sanjay', 
'malusare','2016-8-2',NULL,'9636369082','Male',NULL, NULL, 'vhvgjh',
'Y','higg','Y','gicv', NULL,'Y', NULL, 'givc', 'gjcv', 
'5, Hanuman Hedit Marg No 2, Om Shri Siddhivinayak Society, 
Paranjape Nagar, Vile Parle, Mumbai, Maharashtra 400047, India ', 'hjv', 
'ghvg', 'ggbv', 'vm', 'ttfv', 123456, 'hjv', 'ghvg', 'ggbv', 'vm', 'ttfv',
123456, NULL, NULL, NULL,NULL,NULL,NULL,NULL,NULL, NULL, 0,
'SM_08_2016_9636369082', NULL,3500, 'hhcvknb', '2016-8-8', 
'Referral', 'Other Driver', 'hdg', 'hhvv', 'guvv',
NULL,  '2016-8-8', '2016-8-8', '2016-8-8',16, 1, 1 );