DROP PROCEDURE IF EXISTS get_other_company_code;
DELIMITER $$
CREATE  PROCEDURE `get_other_company_code`()

BEGIN

SELECT driver_other_member_lookup_id, other_company_code, other_company_name 
	FROM  driver_other_company_lookup; 


END$$
DELIMITER;
call get_other_company_code();