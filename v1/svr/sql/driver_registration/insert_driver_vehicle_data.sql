DROP PROCEDURE IF EXISTS insert_driver_vehicle_data;
DELIMITER $$
CREATE  PROCEDURE `insert_driver_vehicle_data`(IN inuniqueid varchar(250), IN invehiclenostate varchar(5),
IN invehiclenocity varchar(5),	 IN invehiclenocode varchar(5), 	IN invehicleno int(10),
IN invehiclecompany varchar(50), IN invehiclecategory varchar(50),	IN invehiclemodel varchar(10),
IN invehicletype varchar(15),    IN invehiclecolor varchar(20),    	IN inservicecode varchar(50), IN invehicleownership varchar(50))
BEGIN

INSERT INTO driver_vehicle(unique_id, vehicleno_state, vehicleno_city, vehicleno_code, vehicleno, vehicle_company,
 vehicle_category, vehicle_model, vehicle_type, vehicle_color, service_code,vehicle_ownership)
VALUES(inuniqueid, invehiclenostate, invehiclenocity, invehiclenocode, invehicleno, invehiclecompany, 
invehiclecategory, invehiclemodel, 	invehicletype, invehiclecolor, inservicecode, invehicleownership)
ON DUPLICATE KEY UPDATE
	vehicleno_state =  COALESCE(invehiclenostate,vehicleno_state),
	vehicleno_city = COALESCE(invehiclenocity,vehicleno_city),
	vehicleno_code = COALESCE(invehiclenocode,vehicleno_code),
	vehicleno = COALESCE(invehicleno,vehicleno),
	vehicle_company = COALESCE(invehiclecompany,vehicle_company),
	vehicle_category = COALESCE(invehiclecategory,vehicle_category),
	vehicle_model = COALESCE(invehiclemodel,vehicle_model),
	vehicle_type = COALESCE(invehicletype,vehicle_type),
	vehicle_color = COALESCE(invehiclecolor,vehicle_color), 
	service_code = COALESCE(inservicecode, service_code),
vehicle_ownership = COALESCE(invehicleownership, vehicle_ownership);
	
SELECT LAST_INSERT_ID() as driver_vehicle_id;

END$$
DELIMITER;