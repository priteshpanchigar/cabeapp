DROP PROCEDURE IF EXISTS get_driver_profile_data;
DELIMITER $$
CREATE  PROCEDURE `get_driver_profile_data`()

BEGIN

SELECT driver_profile_id, unique_key, clientdatetime, unique_id, payment_status, payment_amount, receipt_no, receipt_date,
driver_firstname, driver_lastname, driver_dob, driver_email, driver_phoneno, driver_gender, 
driver_pancard, driver_aadhar, driver_license_no, driver_permit_flag, driver_permit_no, driver_badge_flag, driver_badge_no, 
driver_incity_since, cab_needed_flag, remark, internal_comment, google_address, t_address_1, t_address_2, t_city_town,
t_landmark, t_state, t_pincode, p_address_1, p_address_2, p_city_town, p_landmark, p_state, p_pincode,
other_company_code, driver_other_company_id, driver_vehicle_no, other_company_name,
driver_image_name, driver_image_path, driver_image_datettime, pancard_image_name,
pancard_image_path, pancard_image_datetime, license_image_name, license_image_path,
license_image_datetime, aadhar_image_name, aadhar_image_path, aadhar_image_datetime,
referral_walk_in, referral_supervisor, referral_driver, receipt_no_2, receipt_no_3, driver_backout_reason,
tr_license_exp_date, receipt_date_2, receipt_date_3, private_verification, police_verification, last_sync_datetime
FROM vw_driver_profile_data
WHERE unique_key IS NOT NULL;




END$$
DELIMITER;
call get_driver_profile_data();