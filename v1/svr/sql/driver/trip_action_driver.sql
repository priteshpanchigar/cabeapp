DROP PROCEDURE IF EXISTS trip_action_driver;
DELIMITER $$

CREATE PROCEDURE `trip_action_driver`(IN inappuserid INT,
	IN intripaction varchar(2), IN intriptype varchar(10),
 	IN inclientdatetime datetime,  
	IN intripid INT, IN `inlatno` double, IN `inlngno` double, 
	IN `inacc` int, IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double)
quit_proc: BEGIN
	DECLARE v_location_id INT DEFAULT 0;
	DECLARE v_gcm_reg_id VARCHAR(255) DEFAULT NULL;
	DECLARE v_trip_action VARCHAR(5) DEFAULT NULL;
	DECLARE v_trip_end_notification_sent INT DEFAULT 0;
	DECLARE v_hasdriver_started INT DEFAULT 0;
	SET @DriverAppUserId = inappuserid, 
		@vClientDateTime = inclientdatetime,
		@inDistance = 3, 
		@vTripId = intripid;
			
	SET v_location_id = insert_location(@DriverAppUserId, inclientdatetime, 
						inlatno, inlngno, inacc, inlocationdatetime,
						inprovider, inspeed, inbearing, inaltitude);
	
	SELECT trip_end_notification_sent, trip_action, hasdriver_started FROM trips 
		WHERE trip_id = @vTripId
		INTO v_trip_end_notification_sent, v_trip_action, v_hasdriver_started;

		

	IF v_trip_action = 'E' OR  v_trip_action IS NULL 
		OR intripaction = v_trip_action
	THEN
		#SELECT 1 FROM DUAL WHERE 1 = 0;
		LEAVE quit_proc;
	END IF;		
	
	UPDATE trips
		SET trip_action = intripaction,
			trip_action_time = @vClientDateTime
		WHERE trip_id = @vTripId;

IF  intriptype =  'P' THEN
	SELECT gcm_registration_id, trip_action, trip_action_time FROM trips t
INNER JOIN passenger_shared_trip ps
	ON t.trip_id =  ps.trip_id
INNER JOIN appuser ap
	ON ap.appuser_id = ps.passenger_appuser_id
	WHERE t.trip_id = @vTripId;

ELSE IF  intriptype = 'A' THEN
	SELECT gcm_registration_id, trip_action, trip_action_time FROM trips t
INNER JOIN booked_driver ps
	ON t.trip_id =  ps.trip_id
INNER JOIN appuser ap
	ON ap.appuser_id = ps.passenger_appuser_id
	WHERE t.trip_id = @vTripId;

	END IF;						
END IF;


/*		
	UPDATE trips
		SET planned_start_time = COALESCE(DATE_FORMAT(inplannedstartdatetime,'%H:%i'), planned_start_time),
				planned_start_datetime = COALESCE(inplannedstartdatetime, planned_start_datetime)
		WHERE trip_id = @vTripId AND trip_action = 'B';
*/	
IF intripaction = 'B' THEN
	UPDATE trips		
		SET hasdriver_started = 1,
				trip_start_time = @vClientDateTime
		WHERE trip_id = @vTripId;
END IF;

IF (intripaction = 'B' OR intripaction = 'C') THEN
	UPDATE trips 
		SET start_location_id = COALESCE(v_location_id, start_location_id) 
		WHERE trip_id = @vTripId;		
END IF;



IF  intripaction = 'A'  THEN
	UPDATE appuser_flag
		SET is_for_hire = 1
	WHERE appuser_id = @DriverAppUserId;
	
		UPDATE trips
		SET trip_action = "A",
			trip_action_time = inclientdatetime,
			trip_abort_time = inclientdatetime
		WHERE trip_id = intripid;	
	
END IF;




IF intripaction = 'E'  THEN
	UPDATE appuser_flag
		SET is_for_hire = 1
	WHERE appuser_id = @DriverAppUserId;

	UPDATE trips 
		SET last_location_id = COALESCE(v_location_id, last_location_id) , 
				trip_end_notification_sent = 1,
				trip_end_time  = inclientdatetime
	WHERE trip_id = @vTripId;	
END IF;

IF intripaction = 'E' AND intriptype = 'A' THEN 

UPDATE booked_driver
	SET trip_time = (SELECT TIMEDIFF(trip_end_time,trip_start_time ) FROM trips t
						WHERE trip_id = @vTripId) WHERE trip_id = @vTripId;

UPDATE booked_driver
			SET actual_trip_distance = fn_get_trip_distance(intripid, 'A')
					WHERE trip_id = intripid;

						
ELSE IF  intripaction = 'E' AND intriptype = 'K' THEN

		UPDATE booked_driver_kiosk
			SET trip_time = (SELECT TIMEDIFF(trip_end_time,trip_start_time ) FROM trips t
								WHERE trip_id = @vTripId) WHERE trip_id = @vTripId;

ELSE IF  intripaction = 'E' AND intriptype = 'S' THEN
		UPDATE booked_driver_busy
			SET trip_time = (SELECT TIMEDIFF(trip_end_time,trip_start_time ) FROM trips t
								WHERE trip_id = @vTripId) WHERE trip_id = @vTripId;
								
ELSE IF  intripaction = 'E' AND intriptype = 'W' THEN
		UPDATE booked_driver_web
			SET trip_time = (SELECT TIMEDIFF(trip_end_time,trip_start_time ) FROM trips t
								WHERE trip_id = @vTripId) WHERE trip_id = @vTripId;
								
ELSE IF  intripaction = 'E' AND intriptype = 'P' THEN
		UPDATE booked_driver_shared
			SET trip_time = (SELECT TIMEDIFF(trip_end_time,trip_start_time ) FROM trips t
								WHERE trip_id = @vTripId) WHERE trip_id = @vTripId;

		UPDATE booked_driver_shared
			SET trip_distance = fn_get_trip_distance(intripid, 'A')
					WHERE trip_id = intripid;
									
				
				END IF;				
			END IF;					
		END IF;						
	END IF;						
END IF;




END$$
DELIMITER;

call trip_action_driver(3,'P','P', "2016-11-11 13:07:42",31,"19.1046025","72.8506156","10.0","2016-11-11 13:07:39","fused","0.0","0.0","0.0");