DROP PROCEDURE IF EXISTS `jump_in`;
DELIMITER $$
CREATE  PROCEDURE `jump_in`(IN `inappuserid` INT, 
	IN `inclientdatetime` datetime, IN `inlatno` double, IN `inlngno` double, 
	IN `inacc` INT, IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double,
	IN `inpassengerappuserid` INT, IN `intripid` INT, IN `jumpin` INT, IN `jumpout` INT)
BEGIN

DECLARE vAppUserId INT;
DECLARE v_location_id INT DEFAULT 0;

SET vAppUserId = inappuserid;
 

IF (vAppUserId IS NOT NULL) THEN
		#SELECT vAppUserId;
		SET v_location_id = insert_location(vAppUserId, inclientdatetime, 
		inlatno, inlngno, inacc, inlocationdatetime,
			inprovider, inspeed, inbearing, inaltitude);
END IF;

IF jumpin = 1 THEN 
		UPDATE passenger_shared_trip
			SET has_jumped_in = 1, 
				jumpin_datetime = inclientdatetime,
				jumpin_loc_id = v_location_id,
				jumpin_notification_sent = 1 
			WHERE trip_id = intripid 
			AND passenger_appuser_id = inpassengerappuserid;

SELECT CONCAT(firstname,' ',lastname) fullname, vw.gcm_registration_id, ps.has_jumped_in, ps.jumpin_datetime, ps.jumpin_address
FROM passenger_shared_trip ps
	INNER JOIN vw_passenger_info vw
	ON ps.passenger_appuser_id = vw.appuser_id
WHERE trip_id = intripid 
			AND passenger_appuser_id = inpassengerappuserid;

		
END IF;


IF jumpout = 1 THEN 

		UPDATE passenger_shared_trip
			SET has_jumped_out = 1, 
				jumpout_datetime = inclientdatetime,
				jumpout_loc_id = v_location_id, 
				jumpout_notification_sent = 1 
			WHERE trip_id = intripid 
			AND passenger_appuser_id = inpassengerappuserid;
			
		UPDATE passenger_shared_trip
			SET trip_time = (SELECT TIMEDIFF(jumpout_datetime,jumpin_datetime))  
								WHERE trip_id = intripid 
								AND passenger_appuser_id = inpassengerappuserid;


SELECT CONCAT(firstname,' ',lastname) fullname, vw.gcm_registration_id, ps.has_jumped_out, ps.jumpout_datetime
FROM passenger_shared_trip ps
	INNER JOIN vw_passenger_info vw
	ON ps.passenger_appuser_id = vw.appuser_id
WHERE trip_id = intripid 
			AND passenger_appuser_id = inpassengerappuserid;
	
	UPDATE passenger_shared_trip
			SET trip_distance = fn_get_shared_trip_distance(intripid, 'P', inpassengerappuserid)
					WHERE trip_id = intripid 
								AND passenger_appuser_id = inpassengerappuserid;
	
END IF;


END$$
DELIMITER ;

call jump_in(6,"2016-11-10 13:05:42","19.1045539","72.850671","10.0","2016-11-10 13:05:38","fused","0.0","0.0","0.0",1,8,1,0)
