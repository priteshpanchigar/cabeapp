DROP PROCEDURE IF EXISTS `get_trip_path`;
DELIMITER $$
CREATE  PROCEDURE `get_trip_path`(IN intripid INT)
BEGIN

SELECT fromaddress, toaddress, fromlat, fromlng, tolat, tolng, trip_directions_polyline FROM trips
	WHERE trip_id = intripid;

END$$
DELIMITER ;

call get_trip_path(28)
