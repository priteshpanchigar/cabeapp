DROP PROCEDURE IF EXISTS `weekly_history`;
DELIMITER $$
CREATE  PROCEDURE `weekly_history`(IN inappuserid INT, IN infromdatetime datetime, IN intodatetime datetime)
BEGIN

SELECT  t.trip_id, triptype, fromaddress, toaddress, fromlat, fromlng, tolat, tolng, tripcreationtime, planned_start_datetime, 
trip_start_time, trip_end_time, something_went_wrong, trip_action, bd.trip_comment, bd.trip_cost, bd.trip_distance, 
bd.trip_rating, bd.trip_time
FROM trips t INNER JOIN
booked_driver_kiosk bd 
ON t.trip_id = bd.trip_id
 WHERE appuser_id = inappuserid  AND
 date(tripcreationtime) BETWEEN date(infromdatetime) AND date(intodatetime)
AND trip_action = 'E'
AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0  AND bd.trip_timed_out = 0
UNION
SELECT  t.trip_id, triptype, fromaddress, toaddress, fromlat, fromlng, tolat, tolng, tripcreationtime, planned_start_datetime, 
trip_start_time, trip_end_time, something_went_wrong, trip_action, bd.trip_comment, bd.trip_cost, bd.trip_distance, 
bd.trip_rating, bd.trip_time
FROM trips t INNER JOIN
booked_driver bd 
ON t.trip_id = bd.trip_id
 WHERE appuser_id = inappuserid  AND
 date(tripcreationtime) BETWEEN date(infromdatetime) AND date(intodatetime)
AND trip_action = 'E'
AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0  AND bd.trip_timed_out = 0
UNION
SELECT  t.trip_id, triptype, fromaddress, toaddress, fromlat, fromlng, tolat, tolng, tripcreationtime, planned_start_datetime, 
trip_start_time, trip_end_time, something_went_wrong, trip_action, bd.trip_comment, bd.trip_cost, bd.trip_distance, 
bd.trip_rating, bd.trip_time
FROM trips t INNER JOIN
booked_driver_busy bd 
ON t.trip_id = bd.trip_id
 WHERE appuser_id = inappuserid  AND
 date(tripcreationtime) BETWEEN date(infromdatetime) AND date(intodatetime)
AND trip_action = 'E'
AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0  AND bd.trip_timed_out = 0
UNION
SELECT  t.trip_id, triptype, fromaddress, toaddress, fromlat, fromlng, tolat, tolng, tripcreationtime, planned_start_datetime, 
trip_start_time, trip_end_time, something_went_wrong, trip_action, '' as trip_comment, bd.trip_cost, bd.trip_distance, 
'' as trip_rating, bd.trip_time
FROM trips t INNER JOIN
booked_driver_web bd 
ON t.trip_id = bd.trip_id
 WHERE appuser_id = inappuserid  AND
 date(tripcreationtime) BETWEEN date(infromdatetime) AND date(intodatetime)
AND trip_action = 'E'
AND bd.cancelled_driver = 0  AND bd.trip_timed_out = 0
ORDER BY trip_id DESC;


END$$
DELIMITER ;
CALL weekly_history(1,"2016-08-01 00:00:00", "2016-08-24 23:59:59");
