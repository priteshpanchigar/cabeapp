DROP PROCEDURE IF EXISTS update_position_driver;
DELIMITER //
CREATE  PROCEDURE `update_position_driver`(IN `inappuserid` INT, 
	IN `inclientdatetime` datetime, IN `inlatno` double, IN `inlngno` double, 
	IN `inacc` int, IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double)
quit_proc: BEGIN
	DECLARE v_location_id INT DEFAULT 0;
	DECLARE v_trip_id INT DEFAULT 0;
	
	SET @appUserId = inappuserid;
	SELECT MAX(trip_id) FROM trips
	WHERE appuser_id = @appUserId
	INTO v_trip_id;
	
	SET v_location_id = insert_location(@appUserId, inclientdatetime, 
						inlatno, inlngno, inacc, inlocationdatetime,
						inprovider, inspeed, inbearing, inaltitude);
	
	IF v_trip_id is NULL THEN
		LEAVE quit_proc;
	END IF;
	UPDATE trips
		SET last_location_id = COALESCE( v_location_id, last_location_id)
	WHERE trip_id = v_trip_id 
		AND trip_action IN ('B', 'R', 'P');

	SELECT 1 AS result;
	
END //
DELIMITER ;
CALL update_position_driver(1,"2016-07-13 12:17:30","19.104584300","72.850651800","14.142000198364258","2016-07-13 12:17:25","fused","0.0","0.0","0.0");