DROP PROCEDURE IF EXISTS `cancel_trip_driver`;
DELIMITER $$
CREATE  PROCEDURE `cancel_trip_driver`(IN indriverappuserid INT(5), IN intripid INT(5),
IN inclientdatetime datetime, IN intriptype VARCHAR (3), IN incancelreasoncode VARCHAR (10))
BEGIN
IF intriptype = 'A' THEN
	UPDATE booked_driver
		SET cancelled_driver = 1, 
			cancelled_driver_datetime = inclientdatetime,
			cancel_reason_code = incancelreasoncode
		WHERE trip_id = intripid
		 AND driver_appuser_id = indriverappuserid;
		 
 
		 
	
		 
	SELECT  triptype, cancelled_driver, cancelled_driver_datetime
		FROM booked_driver bd
		INNER JOIN trips ct 
			ON ct.trip_id = bd.trip_id
WHERE bd.trip_id = intripid;

ELSE IF intriptype = 'P' THEN #this is shared trip
	UPDATE booked_driver_shared
		SET cancelled_driver = 1, 
			cancelled_driver_datetime = inclientdatetime,
			cancel_reason_code = incancelreasoncode
		WHERE trip_id = intripid
		 AND driver_appuser_id = indriverappuserid;
		 
	
	
		 
	SELECT  triptype, cancelled_driver, cancelled_driver_datetime
		FROM booked_driver_shared bd
		INNER JOIN trips ct 
			ON ct.trip_id = bd.trip_id
WHERE bd.trip_id = intripid;


		
ELSE IF intriptype = 'K' THEN #this is kiosk trip
UPDATE booked_driver_kiosk
		SET cancelled_driver = 1, 
			cancelled_driver_datetime = inclientdatetime,
			cancel_reason_code = incancelreasoncode	
		WHERE trip_id = intripid
		 AND driver_appuser_id = indriverappuserid;
		 

		 
		 
	SELECT triptype, cancelled_driver, 	cancelled_driver_datetime
		FROM booked_driver_kiosk bd
		INNER JOIN trips ct 
			ON ct.trip_id = bd.trip_id
WHERE bd.trip_id = intripid;
			
ELSE IF intriptype = 'S' THEN #this is self trip
UPDATE booked_driver_busy
		SET cancelled_driver = 1, 
			cancelled_driver_datetime = inclientdatetime,
			cancel_reason_code = incancelreasoncode	
		WHERE trip_id = intripid
		 AND driver_appuser_id = indriverappuserid;
		 
				
	
		 
	SELECT triptype, cancelled_driver, cancelled_driver_datetime	
		FROM booked_driver_busy bd
		INNER JOIN trips ct 
			ON ct.trip_id = bd.trip_id
WHERE bd.trip_id = intripid;

ELSE IF intriptype = 'W' THEN #this is web trip
UPDATE booked_driver_web
		SET cancelled_driver = 1, 
			cancelled_driver_datetime = inclientdatetime,
			cancel_reason_code = incancelreasoncode	
		WHERE trip_id = intripid
		 AND driver_appuser_id = indriverappuserid;
		 
				

		 
	SELECT triptype, cancelled_driver, cancelled_driver_datetime	
		FROM booked_driver_web bd
		INNER JOIN trips ct 
			ON ct.trip_id = bd.trip_id
WHERE bd.trip_id = intripid;

END IF;			
END IF;			
END IF;
END IF;
END IF;				
			

END$$
DELIMITER;