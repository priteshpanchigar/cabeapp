DROP PROCEDURE IF EXISTS user_access_driver;
DELIMITER $$
CREATE  PROCEDURE `user_access_driver`( IN `inregid` varchar(255), 
	IN `incountrycode` varchar(05), IN `inphoneno` varchar(20),
	IN `inclientdatetime` datetime, IN `inemailid`  varchar(100),
	IN `inlatno` double, IN `inlngno` double,
	IN `inacc` int, IN `inlocationdatetime` datetime, 
	IN `inprovider` varchar(10), IN `inspeed` float, 
	IN `inbearing` float,IN `inaltitude` double,
	IN `inimeino` varchar(30), IN `incarrier` varchar(30),
	IN `inproduct` varchar(30), IN `inmanufacturer` varchar(30),
	IN `inapp` varchar(05), IN `inversion` varchar(25), 
	IN `inversioncode` INT, IN `inmodule` varchar(20),
	IN `inip` varchar(50), IN `inuseragent` varchar(300),
	IN `inandroid_release_version` varchar(30),IN `inandroid_sdk_version` int,
	IN `inverified` int)
BEGIN
	DECLARE vDriverAppuserId INT;
	
#	DECLARE vVehicleCategory varchar(10);
	DECLARE v_APPUSAGE_ID INT;
	DECLARE v_clientdttm datetime;
	DECLARE v_serverdttm datetime;
	DECLARE v_location_id INT DEFAULT 0;
	DECLARE countryphone VARCHAR(20) DEFAULT '';
	DECLARE vRowFound INT DEFAULT 0;
	SET @isInsert = 0;
	SET @inDistance = 5;
	

	SELECT appuser_id FROM appuser
	WHERE email = inemailid
	AND imei = inimeino
	INTO vDriverAppuserId;

#	SELECT vehicle_category FROM vwcommercialvehicle v 
#		WHERE v.driver_appuser_id = vDriverAppuserId INTO vVehicleCategory;


	IF (vDriverAppuserId IS NULL) THEN
		INSERT INTO appuser(gcm_registration_id, email, phoneno, imei, 
			lat, lng, accuracy, provider,
			carrier, product, manufacturer, android_release_version, android_sdk_version, 
			country_code, clientlastaccessdatetime)
		VALUES(inregid, inemailid, inphoneno, inimeino, 
		   inlatno, inlngno, inacc, inprovider, 
		   incarrier, inproduct, inmanufacturer, inandroid_release_version, inandroid_sdk_version, 
		   incountrycode, inclientdatetime);
		   SELECT LAST_INSERT_ID() INTO vDriverAppuserId;
		   SET @isInsert = 1;

		INSERT INTO appusage(appuser_id, clientfirstaccessdatetime, 
			clientlastaccessdatetime, serverfirstaccessdatetime, 
			serverlastaccessdatetime, version, versioncode, app)
		VALUES(vDriverAppuserId, inclientdatetime, inclientdatetime, 
			NOW(), NOW(), inversion, inversioncode, inapp)
			ON DUPLICATE KEY UPDATE    
		version = COALESCE(inversion, version);
	END IF;
	

	
	IF (vDriverAppuserId IS NOT NULL) THEN

			
			INSERT INTO driver(appuser_id, email, country_code, phoneno, gcm_registration_id, clientdatetime)
			VALUES(vDriverAppuserId, inemailid, incountrycode, inphoneno, inregid, inclientdatetime)
				ON DUPLICATE KEY UPDATE    
						email = COALESCE(inemailid,email),
						gcm_registration_id = COALESCE(inregid, gcm_registration_id), 
						clientdatetime = COALESCE(inclientdatetime, clientdatetime),
						country_code = (COALESCE(incountrycode,country_code)),
						phoneno = (COALESCE( inphoneno, phoneno));
		
#		SELECT vDriverAppuserId;
		SET v_location_id = insert_location(vDriverAppuserId, inclientdatetime, 
						inlatno, inlngno, inacc, inlocationdatetime,
						inprovider, inspeed, inbearing, inaltitude);
						
#		SELECT 'Location id : ', v_location_id;
		IF v_location_id IS NULL THEN
			SET v_location_id = -1;
		END IF;
		
		IF (v_location_id > 0) THEN	
			IF @isInsert = 1 THEN
#				SELECT v_location_id;
				UPDATE appuser
				SET first_location_id = v_location_id,
					last_location_id = v_location_id, 
					clientlastaccessdatetime = inclientdatetime
				WHERE appuser_id = vDriverAppuserId; 
			ELSE
#				SELECT vDriverAppuserId, v_location_id;
				UPDATE appuser
					SET last_location_id = v_location_id,
					clientlastaccessdatetime = inclientdatetime
				WHERE appuser_id = vDriverAppuserId; 
			END IF;
		END IF;
#	SELECT vDriverAppuserId;	
		UPDATE appuser
			SET first_location_id = COALESCE(first_location_id, last_location_id),
				gcm_registration_id = COALESCE(inregid, gcm_registration_id),
				ip = (COALESCE(inip, ip)),
				lat = (COALESCE(inlatno, lat)),
				lng = (COALESCE(inlngno, lng)),
				accuracy = (COALESCE(inacc, accuracy)),
				provider = (COALESCE(inprovider, provider)),
				useragent = (COALESCE(inuseragent, useragent)),
				android_release_version = (COALESCE(inandroid_release_version, android_release_version)),
				android_sdk_version = (COALESCE(inandroid_sdk_version, android_sdk_version)),
				clientlastaccessdatetime =(COALESCE(inclientdatetime,clientlastaccessdatetime)),
				country_code = (COALESCE(incountrycode,country_code)),
				phoneno = (COALESCE( inphoneno, phoneno)),
				verified  = (COALESCE(inverified, verified))
			WHERE appuser_id = vDriverAppuserId;
		INSERT INTO appusage(appuser_id, clientfirstaccessdatetime, 
			clientlastaccessdatetime, serverfirstaccessdatetime, 
			serverlastaccessdatetime, version, versioncode, app)
		VALUES(vDriverAppuserId, inclientdatetime, inclientdatetime, 
			NOW(), NOW(), inversion, inversioncode, inapp)
			ON DUPLICATE KEY UPDATE    
			clientlastaccessdatetime = COALESCE(inclientdatetime, clientlastaccessdatetime), 
			serverlastaccessdatetime = COALESCE(NOW(), serverlastaccessdatetime),
			version = COALESCE(inversion, version), 
			versioncode = COALESCE(inversioncode, versioncode);	

	END IF;
	
	INSERT INTO appuser_flag(appuser_id)
			VALUES (vDriverAppuserId)
				ON DUPLICATE KEY UPDATE    
			appuser_id = vDriverAppuserId;
			
	SELECT CASE WHEN vDriverAppuserId IS NULL THEN -1 ELSE vDriverAppuserId END appuser_id, 
		a.is_admin,   receive_notifications, show_cab_notifications, verified, manually_verified, d.country_code, d.phoneno,
		show_me, a.is_commercial, allowstrangernotifications 
		#vVehicleCategory as vehicle_category
FROM vw_appuser_flag a
	INNER JOIN driver d ON a.appuser_id = d.appuser_id
	WHERE a.appuser_id = vDriverAppuserId;
	
END$$
	
DELIMITER ;
call user_access_driver('dWWkXc_Rwog:APA91bH-94Mk5mkG9erV9smnG7tHJse5aiM1410zX2x1yimdJ9tIqTrUGKAm7dYxMF1l31sx_KjZ6VJYdPu23USW758pYvewnhpQ2jflKhgK3nY8ADd_VkanvPVy5L83RfD2CyLTP5V8',"91","7506879676","2016-11-25 15:21:13",'joshiajay365@gmail.com',"19.104565800","72.850692000","20.0","2016-11-25 15:20:44","network","0.0","0.0","0.0","867371025758035","40420","Kraft-A6000","LENOVO","CED","0.04.48","49","SAR","175.100.181.240","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36",'5.0.2',21,1)