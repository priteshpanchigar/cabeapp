DROP PROCEDURE IF EXISTS `get_driver_profile`;
DELIMITER $$
CREATE  PROCEDURE `get_driver_profile`(IN inappuserid INT)
BEGIN

SELECT a.appuser_id, 
COALESCE(dp.driver_firstname, a.firstname) driver_firstname,
COALESCE(dp.driver_lastname, a.lastname) driver_lastname,
COALESCE(dp.driver_dob, a.dob) driver_dob, 
COALESCE(dp.driver_email, a.email) driver_email, 
COALESCE(dp.driver_phoneno, a.phoneno) driver_phoneno ,
COALESCE(dp.driver_gender, a.gender)  driver_gender,
image_name, image_type, image_createion_datetime, image_path FROM appuser a
	INNER JOIN driver_profile dp
	ON  a.phoneno = dp.driver_phoneno
	INNER JOIN driver_image di
	ON dp.driver_profile_id = di.driver_profile_id
	WHERE di.image_type = 'Driver'
	AND a.appuser_id = inappuserid;

END$$
DELIMITER ;

call get_driver_profile(23)
