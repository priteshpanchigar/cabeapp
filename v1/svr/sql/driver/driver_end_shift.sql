DROP PROCEDURE IF EXISTS driver_end_shift;
DELIMITER $$
CREATE  PROCEDURE `driver_end_shift`( IN `invehicleid` INT,
 IN `inclientdatetime` datetime, IN `inshiftid` INT, IN inappuserid INT)
BEGIN


UPDATE trips
	SET trip_action = 'E' ,
		something_went_wrong = 1
	WHERE  appuser_id = inappuserid
		AND trip_action IN ('C', 'B', 'P', 'R');



UPDATE driver_shift
SET end_shift_time = inclientdatetime,
	is_login = 0
	WHERE shift_id = inshiftid;
	
UPDATE driver_vehicle
SET is_booked = 0,
	login_requested = 0 
	WHERE driver_vehicle_id = invehicleid ;

UPDATE	appuser_flag
	SET is_for_hire = 0
		WHERE appuser_id = inappuserid;	



	SELECT ROW_COUNT() AS result;

END$$
DELIMITER ;

call driver_end_shift(1,"2016-03-17 12:51:54",584,1 )
