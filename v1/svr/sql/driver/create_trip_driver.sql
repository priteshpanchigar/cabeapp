DROP PROCEDURE IF EXISTS `create_trip_driver`;
DELIMITER $$
CREATE  PROCEDURE `create_trip_driver`(IN `intriptype` varchar(5), IN `inplannedstartdatetime` datetime,
	IN `infromaddress` varchar(300), IN `infromshortaddress` varchar(300), 
	IN `intoaddress` varchar(300), IN `intoshortaddress` varchar(300), 
	IN 	infromsublocality varchar(200), IN intosublocality varchar(200), 
	IN 	in_trip_directions_polyline varchar(5000), 
	IN `infromlat` double, IN `infromlng` double, 
	IN `intolat` double, IN `intolng` double, 
	IN `intimems` bigint(20), IN `intripaction` varchar(1), 
	IN `intriptime` int, IN `intripdistance` double, 
	IN	nappuserid INT,	IN `inclientdatetime` datetime, 
	IN `inemailid` varchar(100))
proc_label:BEGIN
DECLARE ndriverid INT DEFAULT 0;
DECLARE ntripid INT DEFAULT 0;
DECLARE lasttripid INT DEFAULT 0;
DECLARE lastfromlat DOUBLE DEFAULT 0;
DECLARE lastfromlng DOUBLE DEFAULT 0;
DECLARE lasttolat DOUBLE DEFAULT 0;
DECLARE lasttolng DOUBLE DEFAULT 0;
DECLARE lasttotaldistance DOUBLE DEFAULT 0;
DECLARE currenttotaldistance DOUBLE DEFAULT 0;
DECLARE previousTripId INT DEFAULT 0;
DECLARE vBookedDriverId INT DEFAULT -1;

SELECT MAX(trip_id) FROM trips
	WHERE appuser_id = nappuserid
	INTO previousTripId;	

IF nappuserid IS NULL THEN
	SELECT -1;
	LEAVE proc_label;
END IF;
UPDATE appuser SET clientlastaccessdatetime = inclientdatetime
	WHERE appuser_id = nappuserid;

SELECT trip_id FROM trips t
	WHERE trip_id =
		(SELECT MAX(trip_id) FROM trips
			WHERE appuser_id = nappuserid
			AND trip_action = 'C')
		AND appuser_id = nappuserid		
		AND ((getDistanceBetweenPoints(t.fromlat, t.fromlng, infromlat, infromlng) < .01
		AND getDistanceBetweenPoints(t.tolat, t.tolng, intolat, intolng) < .01)
		OR (infromaddress = fromaddress AND intoaddress = toaddress))
	INTO ntripid;

IF ntripid > 0 THEN
	UPDATE trips
	SET fromlat = infromlat,
		fromlng = infromlng,
		tolat = intolat,
		tolng = intolng,
		tripcreationtime = inclientdatetime
	WHERE trip_id = lasttripid;
	SELECT ntripid as trip_id, 1 as tripFound FROM DUAL ;
	LEAVE proc_label;
END IF; 

	SELECT driver_id FROM driver
	WHERE appuser_id = nappuserid
	INTO @driver_id;

	IF (@driver_id IS NULL) THEN
		INSERT INTO driver(appuser_id, email)
		VALUES((SELECT appuser_id FROM appuser WHERE appuser_id = nappuserid ), inemailid);
	END IF;	
	INSERT INTO trips(appuser_id, triptype, planned_start_datetime, planned_start_time, 
		fromaddress, fromshortaddress, toaddress, toshortaddress, fromsublocality, 
		tosublocality, trip_directions_polyline, fromlat, fromlng, 
		tolat, tolng, trips_unique_ms, tripcreationtime,
		trip_action, triptime, tripdistance)
	VALUES ((SELECT appuser_id FROM appuser WHERE  appuser_id = nappuserid), 
		intriptype, inplannedstartdatetime, DATE_FORMAT(inplannedstartdatetime,'%H:%i'), 
		infromaddress, infromshortaddress, intoaddress, intoshortaddress,
		infromsublocality, intosublocality, in_trip_directions_polyline,
		infromlat, infromlng,
		intolat, intolng, intimems, inclientdatetime,
		intripaction, intriptime, intripdistance);
	SET ntripid = LAST_INSERT_ID(); 
	SELECT ntripid as trip_id, 0 as  tripFound FROM DUAL ;
	
	IF intriptype = 'P' THEN
		INSERT  INTO booked_driver_shared(driver_appuser_id,  trip_id, booking_time)
		VALUES (nappuserid, ntripid, inclientdatetime);
	ELSE
		INSERT  INTO booked_driver_busy(driver_appuser_id,  trip_id, booking_time)
		VALUES (nappuserid, ntripid, inclientdatetime);
	END IF;
	/*
	SELECT  ct.trip_id, bd.driver_appuser_id,  vw.firstname, vw.lastname,
	vw.country_code, vw.phoneno as driver_phoneno, gender, email, bd.booked_busy_id, 
	bd.booking_time, 	ct.tripcreationtime,  ct.trip_action, ct.trip_action_time, 
	vehicleno, vehicle_company, vehicle_category, vehicle_model, vehicle_type, vehicle_color, service_code
	FROM trips ct
		INNER JOIN booked_driver_busy bd	
		ON ct.trip_id = bd.trip_id
		INNER  JOIN vwcommercialvehicle vw
		ON vw.appuser_id = bd.driver_appuser_id	
		INNER JOIN last_location l
		ON l.appuser_id = bd.driver_appuser_id		
WHERE ct.trip_id = ntripid 
ORDER BY bd.booked_busy_id DESC LIMIT 1; 
	
	
	
	SELECT MAX(booked_driver_id)  FROM booked_driver
		WHERE driver_appuser_id = nappuserid	
		 AND cancelled = 0 AND rejected = 0
		INTO vBookedDriverId;
	
	UPDATE booked_driver 
	SET trip_id = ntripid
		WHERE booked_driver_id = vBookedDriverId;	
*/
END$$
DELIMITER;
call create_trip_driver('P','20:07','General Arun Kumar Vaidya Marg, Yashodham, Malad East, Mumbai, Maharashtra 400063',NULL,'G Block BKC, Bandra Kurla Complex, Bandra East, Mumbai, Maharashtra',NULL,'Dindoshi','BKC',1,19.174289,72.860405,19.063481,72.860206,1474958317000,'C',NULL,NULL,4,"2016-09-27 12:07:51",'soumenmaity.cse@gmail.com')