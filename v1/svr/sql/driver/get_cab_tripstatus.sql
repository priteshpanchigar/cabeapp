DROP PROCEDURE IF EXISTS `get_cab_tripstatus`;
DELIMITER $$
CREATE PROCEDURE `get_cab_tripstatus`(IN intripid INT(11), IN intriptype VARCHAR (3)) 
BEGIN #this IS app trip 

IF intriptype = 'A' THEN
	SELECT bd.trip_id, trip_action, trip_action_time, cancelled_driver, cancelled_passenger, 
		bd.trip_timed_out
	FROM booked_driver bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id
	WHERE bd.trip_id = intripid;
ELSE IF intriptype = 'P' THEN #this IS shared trip
SELECT bd.trip_id, trip_action, trip_action_time, cancelled_driver, bd.trip_timed_out,
	ps.passenger_appuser_id, vw.phoneno, CONCAT(vw.firstname, ' ',lastname) fullname,
vw.gender, vw.dob, vw.userprofileimagefilename, has_joined, has_joined_datetime, cancel_join,
has_jumped_in,	has_jumped_out, jumpin_datetime, jumpout_datetime
	FROM booked_driver_shared bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id
		INNER JOIN passenger_shared_trip ps
		ON t.trip_id = ps.trip_id
		LEFT JOIN vw_passenger_info vw
		ON vw.appuser_id = ps.passenger_appuser_id
	WHERE bd.trip_id = intripid
		AND ps.cancel_join = 0;
ELSE IF intriptype = 'K' THEN #this IS kiosk trip
	SELECT bd.trip_id, trip_action, trip_action_time, cancelled_driver, cancelled_passenger,  bd.trip_timed_out
	FROM booked_driver_kiosk bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id 
	WHERE bd.trip_id = intripid; 
END IF; 
END IF;
END IF;
 
END$$ 
DELIMITER ;

CALL get_cab_tripstatus(35, 'P');