DROP PROCEDURE IF EXISTS `transfer_driver_info`;
DELIMITER $$
CREATE  PROCEDURE `transfer_driver_info`()
BEGIN

INSERT driver  (firstname, lastname, dob, phoneno, gender, image_name, image_path)
SELECT driver_firstname, driver_lastname, driver_dob, driver_phoneno,  driver_gender, di.image_name, di.image_path 
	FROM driver_profile dp
	INNER JOIN driver_image di 
		ON di.driver_profile_id = dp.driver_profile_id
	WHERE image_type = 'Driver' 
		AND dp.verification_completed = 1
ON DUPLICATE KEY UPDATE 
			firstname = driver_firstname,
			lastname = driver_lastname,
			gender = driver_gender,
			dob = driver_dob,
			image_name = di.image_name,
			image_path = di.image_path;


END$$
DELIMITER ;

CALL transfer_driver_info();
