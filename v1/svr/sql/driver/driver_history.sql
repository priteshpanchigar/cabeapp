DROP PROCEDURE IF EXISTS `driver_history`;
DELIMITER $$
CREATE  PROCEDURE `driver_history`(IN inappuserid INT, IN inclientdatetime datetime)
BEGIN
DECLARE vTotalShift time;
DECLARE vTotalforHire time;
DECLARE vTotalTrips INT;


SELECT COUNT(*) FROM trips
WHERE appuser_id = inappuserid
	GROUP BY appuser_id  INTO vTotalTrips;


SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(timediff(end_shift_time, start_shift_time))))  FROM driver_shift
WHERE appuser_id = inappuserid
AND DATE(start_shift_time) = DATE(inclientdatetime)
AND TIMEDIFF(end_shift_time, start_shift_time) IS NOT NULL
GROUP BY appuser_id INTO vTotalShift;


SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(timediff(end_is_for_hire, start_is_for_hire))))  FROM for_hire
WHERE appuser_id = inappuserid
AND DATE(start_is_for_hire) = DATE(inclientdatetime)
AND TIMEDIFF(end_is_for_hire,start_is_for_hire) IS NOT NULL
GROUP BY appuser_id INTO vTotalforHire;

SELECT driver_appuser_id,  vTotalforHire as total_for_hire, vTotalShift as total_shift_time, vTotalTrips as total_trips,
 booking_time, trip_id, trip_cost, trip_distance, 
trip_time FROM  booked_driver bd
INNER JOIN appuser ap
ON ap.appuser_id = bd.driver_appuser_id
WHERE driver_appuser_id = inappuserid
AND cancelled_driver = 0 
AND cancelled_passenger = 0 
AND trip_timed_out = 0
AND DATE(booking_time) = DATE(inclientdatetime)
UNION
SELECT driver_appuser_id,  vTotalforHire as total_for_hire, vTotalShift as total_shift_time, vTotalTrips as total_trips,
 booking_time, trip_id, trip_cost, trip_distance, 
trip_time FROM  booked_driver_kiosk bd
INNER JOIN appuser ap
ON ap.appuser_id = bd.driver_appuser_id
WHERE driver_appuser_id = inappuserid
AND cancelled_driver = 0 
AND cancelled_passenger = 0 
AND trip_timed_out = 0
AND DATE(booking_time) = DATE(inclientdatetime)
UNION
SELECT driver_appuser_id,  vTotalforHire as total_for_hire, vTotalShift as total_shift_time, vTotalTrips as total_trips,
 booking_time, trip_id, trip_cost, trip_distance, 
trip_time FROM  booked_driver_busy bd
INNER JOIN appuser ap
ON ap.appuser_id = bd.driver_appuser_id
WHERE driver_appuser_id = inappuserid
AND cancelled_driver = 0 
AND cancelled_passenger = 0 
AND trip_timed_out = 0
AND DATE(booking_time) = DATE(inclientdatetime)
UNION
SELECT driver_appuser_id,  vTotalforHire as total_for_hire, vTotalShift as total_shift_time, vTotalTrips as total_trips,
 booking_time, trip_id, trip_cost, trip_distance, 
trip_time FROM  booked_driver_web bd
INNER JOIN appuser ap
ON ap.appuser_id = bd.driver_appuser_id
WHERE driver_appuser_id = inappuserid
AND cancelled_driver = 0 
AND trip_timed_out = 0
AND DATE(booking_time) = DATE(inclientdatetime)
UNION
SELECT driver_appuser_id,  vTotalforHire as total_for_hire, vTotalShift as total_shift_time, vTotalTrips as total_trips,
 booking_time, trip_id,  trip_cost, trip_distance, 
trip_time FROM  booked_driver_shared bd
INNER JOIN appuser ap
ON ap.appuser_id = bd.driver_appuser_id
WHERE driver_appuser_id = inappuserid
AND cancelled_driver = 0 
AND trip_timed_out = 0
AND DATE(booking_time) = DATE(inclientdatetime)
ORDER BY trip_id DESC;


END$$
DELIMITER ;

call driver_history(8,"2016-11-23 15:52:27")