DROP PROCEDURE IF EXISTS `update_trip_cost`;
DELIMITER $$
CREATE  PROCEDURE `update_trip_cost`(IN intripid INT, IN intripcost INT, IN intripdistance DOUBLE,
 IN intriptype VARCHAR (3))
BEGIN
IF intriptype = 'A' THEN
UPDATE booked_driver
	SET trip_cost = COALESCE(intripcost, trip_cost),
		trip_distance = COALESCE(intripdistance, trip_distance)
		WHERE trip_id = intripid;
UPDATE trips
	SET trip_action = 'E'
		WHERE  trip_id = intripid;
		

SELECT trip_id, trip_cost FROM booked_driver
	WHERE trip_id = intripid;
	
ELSE IF intriptype = 'P' THEN
UPDATE booked_driver_shared
	SET trip_cost = COALESCE(intripcost, trip_cost),
		trip_distance = COALESCE(intripdistance, trip_distance)
		WHERE trip_id = intripid;
UPDATE trips
	SET trip_action = 'E'
		WHERE  trip_id = intripid;
		

SELECT trip_id, trip_cost FROM booked_driver_shared
	WHERE trip_id = intripid;	
ELSE IF intriptype = 'K' THEN #this IS kiosk trip

UPDATE booked_driver_kiosk
	SET trip_cost = COALESCE(intripcost, trip_cost),
		trip_distance = COALESCE(intripdistance, trip_distance)
		WHERE trip_id = intripid;
	UPDATE trips
	SET trip_action = 'E'
		WHERE  trip_id = intripid;

SELECT trip_id, trip_cost FROM booked_driver_kiosk
	WHERE trip_id = intripid;
	
ELSE IF intriptype = 'S' THEN #this IS busy trip

UPDATE booked_driver_busy
	SET trip_cost = COALESCE(intripcost, trip_cost),
		trip_distance = COALESCE(intripdistance, trip_distance)
		WHERE trip_id = intripid;
	UPDATE trips
	SET trip_action = 'E'
		WHERE  trip_id = intripid;

SELECT trip_id, trip_cost FROM booked_driver_busy
	WHERE trip_id = intripid;

ELSE IF intriptype = 'W' THEN #this IS web trip

UPDATE booked_driver_web
	SET trip_cost = COALESCE(intripcost, trip_cost),
		trip_distance = COALESCE(intripdistance, trip_distance)
		WHERE trip_id = intripid;
	UPDATE trips
	SET trip_action = 'E'
		WHERE  trip_id = intripid;

SELECT trip_id, trip_cost FROM booked_driver_web
	WHERE trip_id = intripid;	

	
END IF;
END IF;
END IF; 
END IF;
END$$
DELIMITER ;


call update_trip_cost(45,542,46,'A');