DROP PROCEDURE IF EXISTS `check_if_login_requested`;
DELIMITER $$
CREATE  PROCEDURE `check_if_login_requested`(IN `inappuserid` INT)
BEGIN

DECLARE vVehicleNo  INT;
DECLARE vAppUserId INT;
SET vAppUserId = inappuserid;

SELECT vehicleno FROM driver_vehicle dv
	INNER JOIN driver_shift ds
	ON ds.driver_vehicle_id = dv.driver_vehicle_id
		WHERE is_login =1
		AND ds.appuser_id = vAppUserId
	INTO vVehicleNo;

#SELECT vVehicleNo;
SELECT login_requested FROM driver_vehicle dv
	INNER JOIN driver_shift ds
	ON ds.driver_vehicle_id = dv.driver_vehicle_id 
	WHERE dv.vehicleno =  vVehicleNo
	AND NOT  appuser_id = inappuserid;

		
UPDATE driver_vehicle
	SET login_requested = 0 
		WHERE vehicleno = vVehicleNo;



END$$
CALL check_if_login_requested(2);