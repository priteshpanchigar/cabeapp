DROP PROCEDURE IF EXISTS driver_start_shift;
DELIMITER $$
CREATE  PROCEDURE `driver_start_shift`(IN `inappuserid` INT, IN `invehicleid` INT,
	IN `inclientdatetime` datetime)
BEGIN
DECLARE vIsLogin INT;
DECLARE vShiftID INT;


START TRANSACTION;	

SELECT is_login, shift_id FROM driver_shift
	WHERE appuser_id = inappuserid
	#AND driver_vehicle_id = invehicleid
	AND is_login = 1 INTO vIsLogin, vShiftID;

IF vIsLogin = 0 OR vIsLogin  IS NULL  THEN
UPDATE trips
	SET trip_action = 'E' 
	WHERE  appuser_id = inappuserid
		AND trip_action IN ('C', 'B', 'P', 'R');


UPDATE driver_vehicle
	SET is_booked = 1
	WHERE driver_vehicle_id = invehicleid ;


INSERT INTO driver_shift(appuser_id, driver_vehicle_id, start_shift_time, is_login)
	VALUES (inappuserid, invehicleid, inclientdatetime, 1);

SET vShiftID = LAST_INSERT_ID();
 
END IF;

SELECT vShiftID as shift_id, driver_vehicle_id,a.firstname, 
	a.lastname, a.phoneno,  gender , dob
FROM driver_shift s
 INNER JOIN vw_appuser_flag a  
	ON s.appuser_id = a.appuser_id
AND a.appuser_id = inappuserid
WHERE is_login =1;

COMMIT;
END$$
DELIMITER ;

call driver_start_shift(3,7,"2016-11-16 11:07:59");