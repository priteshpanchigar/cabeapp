DROP PROCEDURE IF EXISTS `insert_device_location`;
DELIMITER $$
CREATE  PROCEDURE `insert_device_location`(IN invehicleno VARCHAR(30),IN inclientdatetime datetime, IN inlat DOUBLE, 
	IN inlng DOUBLE, IN `inacc` int, IN `inloctime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double)
BEGIN
DECLARE v_location_id INT DEFAULT 0;

SET v_location_id = insert_location((SELECT driver_appuser_id FROM vwcommercialvehicle 
	WHERE invehicleno = UPPER(specialcharacter(vehicleno))), inclientdatetime, 
		inlat, inlng, inacc, inloctime, inprovider, inspeed, inbearing, inaltitude);

		
END$$
DELIMITER ;
call insert_device_location('MH02AR3676','2016-06-15 17:35:43',19.104615,72.850705,NULL,'2016-06-15 17:35:43','D',NULL,NULL,NULL);