DROP PROCEDURE IF EXISTS `get_shared_info_after_trip`;
DELIMITER $$
CREATE PROCEDURE `get_shared_info_after_trip`(IN intripid INT(11), IN intriptype VARCHAR (3)) 
BEGIN 

#this IS shared trip
IF intriptype = 'P' THEN 
SELECT bd.trip_id, trip_action, trip_action_time, cancelled_driver, bd.trip_timed_out,
	ps.passenger_appuser_id, vw.phoneno, CONCAT(vw.firstname, ' ',lastname) fullname,
vw.gender, vw.dob, vw.userprofileimagefilename, has_joined, has_joined_datetime, cancel_join,
has_jumped_in,	has_jumped_out, jumpin_datetime, jumpout_datetime
	FROM booked_driver_shared bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id
		INNER JOIN passenger_shared_trip ps
		ON t.trip_id = ps.trip_id
		LEFT JOIN vw_passenger_info vw
		ON vw.appuser_id = ps.passenger_appuser_id
	WHERE bd.trip_id = intripid
		AND ps.cancel_join = 0;

END IF;
 
END$$ 
DELIMITER ;

CALL get_shared_info_after_trip(35, 'P');