DROP PROCEDURE IF EXISTS `select_vehicle`;
DELIMITER $$
CREATE  PROCEDURE `select_vehicle`(IN invehicleno int, IN inappuserid int)
BEGIN

DECLARE vIsLogin  INT;
DECLARE vDriverAppuserID  INT;

SELECT is_login, ds.appuser_id from driver_shift ds 
	INNER JOIN driver_vehicle dv
	ON  ds.driver_vehicle_id = dv.driver_vehicle_id
		WHERE vehicleno = invehicleno
		AND is_login =1 INTO vIsLogin, vDriverAppuserID;



SELECT IFNULL((vIsLogin), 0) is_login , vDriverAppuserID as driver_appuser_id,
login_requested,
 c.company_id, company_name, company_address, vehicle_company,
 dv.driver_vehicle_id,  vehicleno,  concat(vehicleno_state,' ' ,vehicleno_city,' ' ,vehicleno_code,' ', vehicleno) full_vehicleno,
vehicle_category, vehicle_model, vehicle_type, vehicle_color, service_code, rate_per_km, seating, minimum_fare,
 vehicle_distinctive_marks, date_registered
	
FROM driver_vehicle dv
INNER JOIN company c
ON dv.company_id = c.company_id
INNER JOIN driver d
ON dv.company_id = d.company_id
AND dv.franchise_id = d.franchise_id
INNER JOIN appuser a 
ON  a.appuser_id = d.appuser_id 
AND a.appuser_id = inappuserid
WHERE vehicleno = invehicleno;


UPDATE driver_vehicle dv
	SET login_requested = 1 
		WHERE vehicleno = invehicleno
		AND vIsLogin = 1;



END$$
DELIMITER ;

call select_vehicle(2,11);

