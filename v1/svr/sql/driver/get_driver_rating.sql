DROP PROCEDURE IF EXISTS `get_driver_rating`;
DELIMITER $$
CREATE  PROCEDURE `get_driver_rating`(IN inappuserid INT)
BEGIN

SELECT trip_id, driver_appuser_id,  booking_time,
 trip_cost, trip_distance, trip_time, trip_rating, trip_comment FROM booked_driver
WHERE driver_appuser_id = inappuserid

UNION
SELECT trip_id, driver_appuser_id,  booking_time,
 trip_cost, trip_distance, trip_time, trip_rating, trip_comment FROM booked_driver_kiosk
WHERE driver_appuser_id = inappuserid
ORDER BY trip_id DESC;

END$$
DELIMITER ;

call get_driver_rating(5)
