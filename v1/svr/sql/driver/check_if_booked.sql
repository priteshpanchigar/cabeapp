DROP PROCEDURE IF EXISTS `check_if_booked`;
DELIMITER $$
CREATE  PROCEDURE `check_if_booked`(IN `inappuserid` INT, 
	IN `inclientdatetime` datetime, IN `inlatno` double, IN `inlngno` double, 
	IN `inacc` int, IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double)
BEGIN

DECLARE vBookingDriverID INT;
DECLARE vTripType VARCHAR(15);
DECLARE vAppUserId int;
DECLARE v_location_id INT DEFAULT 0;

SET vAppUserId = inappuserid;
 

IF (vAppUserId IS NOT NULL) THEN
		#SELECT vAppUserId;
		SET v_location_id = insert_location(vAppUserId, inclientdatetime, 
		inlatno, inlngno, inacc, inlocationdatetime,
			inprovider, inspeed, inbearing, inaltitude);
END IF;

		
SELECT triptype FROM trips
	WHERE appuser_id = vAppUserId
ORDER BY trip_id DESC LIMIT 1 
INTO vTripType;

IF vTripType = 'A'  THEN 	
# Avoid old rows for this driver and take after the last valid booked driver row
	SELECT booked_driver_id FROM booked_driver bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id
		WHERE bd.driver_appuser_id = vAppUserId 
		AND (cancelled_passenger = 1  OR cancelled_driver = 1  
			OR trip_timed_out = 1 OR trip_action IN ('E','A') OR something_went_wrong =1)
		ORDER BY booked_driver_id DESC LIMIT 1
		INTO vBookingDriverID; 
ELSE IF  vTripType = 'K' THEN
	SELECT booked_kiosk_id FROM booked_driver_kiosk bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id
		WHERE bd.driver_appuser_id = 1 
		AND (cancelled_passenger = 1  OR cancelled_driver = 1  
			OR trip_timed_out = 1 OR trip_action IN ('E','A') OR something_went_wrong =1)
		ORDER BY booked_kiosk_id DESC LIMIT 1
		INTO vBookingDriverID;
ELSE IF vTripType = 'W' THEN
	SELECT booked_web_id FROM booked_driver_web bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id
		WHERE bd.driver_appuser_id = 1 
		AND ( cancelled_driver = 1  
			OR trip_timed_out = 1 OR trip_action IN ('E','A') OR something_went_wrong =1)
		ORDER BY booked_web_id DESC LIMIT 1
		INTO vBookingDriverID;
ELSE IF vTripType = 'S' THEN		
	SELECT booked_busy_id FROM booked_driver_busy bd
		INNER JOIN trips t 
		ON t.trip_id = bd.trip_id
		WHERE bd.driver_appuser_id = 1 
		AND ( cancelled_driver = 1  
			OR trip_timed_out = 1 OR trip_action IN ('E','A') OR something_went_wrong =1)
		ORDER BY booked_busy_id DESC LIMIT 1
		INTO vBookingDriverID;	
END IF;		
END IF;
END IF;
END IF;

IF vBookingDriverID IS NULL THEN 
	SET vBookingDriverID = 0; 
END IF;

#SELECT vBookingDriverID, vAppUserId, vTripType;
#Found an app booked trip	
IF vTripType = 'A'  THEN 	
	SELECT 	bd.cancelled_driver, bd.cancelled_passenger,ct.trip_id,  'A' as usertype,
	  ap.email, ap.phoneno, firstname, lastname, 	bd.booked_driver_id, bd.booking_time,
	ct.trip_id, ct.triptype,  ct.fromaddress, 
	ct.toaddress, ct.fromlat, ct.fromlng, ct.tolat, ct.tolng,
	ct.tripcreationtime, ct.trip_action, 
	ct.trip_action_time, ct.triptime,
	ct.tripdistance	
 	FROM booked_driver bd		
		INNER JOIN trips ct
		ON ct.trip_id = bd.trip_id
		AND trip_action = 'C'
		INNER JOIN appuser ap 
		ON ap.appuser_id = bd.passenger_appuser_id
WHERE bd.driver_appuser_id = vAppUserId
	AND cancelled_passenger = 0  AND cancelled_driver = 0  AND trip_timed_out = 0
	AND  booked_driver_id > vBookingDriverID
ORDER BY bd.booked_driver_id DESC LIMIT 1;

ELSE IF  vTripType = 'K' THEN # No app booked trip, check for kiosk booked trip

	SELECT  bd.cancelled_driver, bd.cancelled_passenger, ct.trip_id, 'K' as usertype,
	bd.national_no as phoneno, bd.booked_kiosk_id, bd.booking_time,  
	k.address, k.building, k.lat as fromlat, k.lng as fromlng, k.postal_code,
	ct.tripcreationtime, ct.trip_action,  ct.trip_action_time
	FROM booked_driver_kiosk bd		
		INNER JOIN trips ct
		ON ct.trip_id = bd.trip_id
		AND trip_action = 'C'	
		LEFT JOIN kiosk k
		ON k.kiosk_id = bd.kiosk_id
WHERE bd.driver_appuser_id = vAppUserId
	AND cancelled_passenger = 0  AND cancelled_driver = 0  AND trip_timed_out = 0
ORDER BY bd.booked_kiosk_id DESC LIMIT 1;

ELSE IF vTripType = 'W' THEN  # No app booked trip, check for web booked trip

SELECT bd.cancelled_driver,  ct.trip_id, 'W' as usertype,
 bd.national_no as phoneno, bd.booked_web_id, bd.booking_time,  
 fromlat, fromlng, ct.tripcreationtime, ct.trip_action,  ct.trip_action_time
	FROM booked_driver_web bd		
		INNER JOIN trips ct
		ON ct.trip_id = bd.trip_id
		AND trip_action = 'C'	
	WHERE bd.driver_appuser_id = vAppUserId
 AND cancelled_driver = 0  AND trip_timed_out = 0
ORDER BY bd.booked_web_id DESC LIMIT 1;

ELSE IF vTripType = 'S' THEN  # No app booked trip, check for busy booked trip

SELECT bd.cancelled_driver,  ct.trip_id, 'S' as usertype,
 bd.booked_busy_id, bd.booking_time,  
 fromlat, fromlng, ct.tripcreationtime, ct.trip_action,  ct.trip_action_time
	FROM booked_driver_busy bd		
		INNER JOIN trips ct
		ON ct.trip_id = bd.trip_id
		AND trip_action = 'C'	
	WHERE bd.driver_appuser_id = vAppUserId
 AND cancelled_driver = 0  AND trip_timed_out = 0
ORDER BY bd.booked_busy_id DESC LIMIT 1;

END IF;
END IF;
END IF;
END IF;
END$$

CALL check_if_booked(1,"2016-08-23 14:59:29","19.104529800","72.850652800","20.0","2016-08-23 14:59:29","fused","0.0","0.0","0.0");