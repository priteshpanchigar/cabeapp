DROP PROCEDURE IF EXISTS upgrade_app;
DELIMITER $$
CREATE  PROCEDURE `upgrade_app`(IN `inapp` VARCHAR(30), IN `inversioncode` INT, IN inappuserid INT)
BEGIN
DECLARE vGcm VARCHAR (1000);

SELECT gcm_registration_id FROM appuser
	WHERE appuser_id = inappuserid INTO vGcm;

	SELECT link, vGcm AS gcm_registration_id, 1 AS upgrade_app FROM appversion
		WHERE app = inapp 
		AND inversioncode < versioncode;	
END$$
DELIMITER ;
# Sample query
CALL upgrade_app('CEP',49, 1);