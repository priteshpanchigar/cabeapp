DROP VIEW IF EXISTS vwcommercialvehicle;

CREATE VIEW vwcommercialvehicle AS
SELECT a.appuser_id,  dv.login_requested, d.gcm_registration_id , d.driver_id, d.country_code, d.phoneno, d.email, d.firstname, d.lastname, d.image_path, d.image_name, 
d.gender, 
shift_id, start_shift_time, end_shift_time,  is_login,
dv.device_imei, dv.reading_id,
dv.driver_vehicle_id, CONCAT(vehicleno_state,' ' ,vehicleno_city,' ' ,vehicleno_code,' ', vehicleno) vehicleno,
vehicle_category, vehicle_company ,vehicle_model, vehicle_type, vehicle_color, dv.service_code,sl.service_description,
metered,sl.rate_per_km, seating, is_booked, vehicle_distinctive_marks,
sl.minimum_fare,
f.company_id, f.franchise_id, c.company_name, c.company_address, c.company_code, c.c_contact_person, c.c_mobile, 
c.c_email, f.franchise_name, f.franchise_code FROM driver_vehicle dv
INNER JOIN driver d
ON dv.company_id = d.company_id
AND dv.franchise_id = d.franchise_id
INNER JOIN appuser a
ON d.appuser_id  = a.appuser_id
INNER JOIN company c
ON dv.company_id = c.company_id
INNER JOIN franchise f
ON f.company_id = c.company_id
INNER JOIN driver_shift cds
ON cds.appuser_id = d.appuser_id
AND cds.driver_vehicle_id = dv.driver_vehicle_id
INNER JOIN service_lookup sl
ON sl.service_code = dv.service_code
LEFT JOIN service_eligibility se
ON se.service_code  = dv.service_code
WHERE is_login = 1; 


SELECT * FROM  vwcommercialvehicle;