DROP VIEW IF EXISTS vw_driver_profile_data;
CREATE VIEW  vw_driver_profile_data AS
SELECT p.driver_profile_id, unique_key, profile_submitted, clientdatetime,unique_id, payment_status, payment_amount, receipt_no, receipt_date, driver_firstname, driver_lastname, driver_dob, driver_email, driver_phoneno, driver_gender, 
driver_pancard, driver_aadhar, driver_license_no, driver_permit_flag, driver_permit_no, driver_badge_flag, driver_badge_no, 
driver_incity_since, cab_needed_flag,driver_vehicle_no, remark, internal_comment, google_address, t_address_1, t_address_2, t_city_town,
t_landmark, t_state, t_pincode, p_address_1, p_address_2, p_city_town, p_landmark, p_state, p_pincode,
o.other_company_code, driver_other_company_id,  other_company_name,
referral_walk_in, referral_supervisor, referral_driver, receipt_no_2, receipt_no_3, driver_backout_reason,
tr_license_exp_date, receipt_date_2, receipt_date_3, private_verification, police_verification, last_sync_datetime,
(SELECT image_name FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='Driver' )driver_image_name,
(SELECT image_path FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='Driver' )driver_image_path,
(SELECT image_createion_datetime FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='Driver' )driver_image_datettime,
(SELECT image_name FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='License' )license_image_name,
(SELECT image_path FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='License' )license_image_path,
(SELECT image_createion_datetime FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='License' )license_image_datetime,
(SELECT image_name FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='AadharCard' )aadhar_image_name,
(SELECT image_path FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='AadharCard' )aadhar_image_path,
(SELECT image_createion_datetime FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='AadharCard' )aadhar_image_datetime,
(SELECT image_name FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='PanCard' )pancard_image_name,
(SELECT image_path FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='PanCard' )pancard_image_path,
(SELECT image_createion_datetime FROM driver_image oi WHERE oi.driver_profile_id = p.driver_profile_id AND image_type ='PanCard' )pancard_image_datetime
FROM driver_profile p 
LEFT JOIN driver_address a
ON p.driver_profile_id = a.driver_profile_id
LEFT JOIN driver_other_member o
ON o.driver_profile_id = p.driver_profile_id
LEFT JOIN driver_other_company_lookup l
ON l.other_company_code = o.other_company_code;



SELECT * FROM  vw_driver_profile_data;