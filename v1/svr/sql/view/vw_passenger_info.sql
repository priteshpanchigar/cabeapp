
DROP VIEW IF EXISTS vw_passenger_info;

CREATE VIEW vw_passenger_info AS

SELECT ap.appuser_id,
ap.gcm_registration_id,  
COALESCE(ap.email, g.email, f.facebook_email) email,  
COALESCE(ap.firstname, g.firstname, f.firstname) firstname, 
COALESCE(ap.lastname, g.lastname, f.lastname) lastname, 
COALESCE(ap.gender, g.gender, f.gender) gender,
COALESCE(ap.dob, g.dob, f.dob) dob,
COALESCE(ap.userprofileimagefilename, g.userprofileimagefilename, f.userprofileimagefilename) userprofileimagefilename,
ap.phoneno 	FROM appuser ap 
LEFT JOIN facebook_profile f
ON ap.appuser_id = f.appuser_id
LEFT JOIN googleplus_profile g
ON ap.appuser_id = g.appuser_id;


SELECT * FROM  vw_passenger_info;
