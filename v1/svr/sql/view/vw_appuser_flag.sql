-- ----------------------------
-- View structure for vw_appuser_flag
-- ----------------------------
DROP VIEW IF EXISTS vw_appuser_flag;

CREATE VIEW vw_appuser_flag AS
SELECT a.appuser_id, gcm_registration_id,   country_code, phoneno, a.email, fp.facebook_email, 
	COALESCE(a.firstname, fp.firstname, gp.firstname) firstname,
	COALESCE(a.lastname, fp.lastname, gp.lastname) lastname,
	COALESCE(a.gender, fp.gender, gp.gender) gender,
	COALESCE(a.dob, fp.dob, gp.dob) dob,
	COALESCE(a.userprofileimagefilename, fp.userprofileimagefilename, gp.userprofileimagefilename) userprofileimagefilename,
 imei, verified, manually_verified,usertype,
 home, work, home_lat, home_lng, work_lat, work_lng,  
 clientlastaccessdatetime, first_location_id, last_location_id, lat, lng, accuracy,
 provider, carrier, product, manufacturer, android_release_version, android_sdk_version, 
 ip, useragent, allowstrangernotifications, is_commercial, shared_cab, is_admin, 
 show_all_notifications, show_cab_notifications, show_me, receive_notifications, 
is_for_hire
#start_is_for_hire, end_is_for_hire 
FROM appuser a
LEFT JOIN facebook_profile fp
ON fp.appuser_id = a.appuser_id
LEFT JOIN googleplus_profile gp
ON a.appuser_id = gp.appuser_id 
INNER  JOIN appuser_flag af
ON a.appuser_id = af.appuser_id;
#LEFT JOIN for_hire fh
#ON fh.appuser_id = a.appuser_id;

SELECT * FROM  vw_appuser_flag;
