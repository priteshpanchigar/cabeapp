DROP PROCEDURE IF EXISTS set_for_hire;
DELIMITER $$
CREATE  PROCEDURE set_for_hire(IN inappuserid INT, 
	IN inforhiredatetime datetime, IN inisforhire INT)
BEGIN
DECLARE vForhireId INT;

INSERT INTO meter_status(appuser_id, meter_flag, meter_datetime)
VALUES(inappuserid, inisforhire, inforhiredatetime);

UPDATE appuser_flag
		SET is_for_hire = inisforhire
		WHERE appuser_id = inappuserid;
		
IF inisforhire = 1 THEN
#to be removed 
	INSERT for_hire(appuser_id, start_is_for_hire)
		VALUES (inappuserid, inforhiredatetime);
	
ELSE IF inisforhire = 0  OR inisforhire = 2 THEN
# Busy condition. If there was an active trip (shouldnt be), it has to end it

UPDATE trips
	SET #trip_action = 'E' ,
		something_went_wrong = 1
	WHERE  appuser_id = inappuserid
		AND trip_action IN ('C', 'B', 'P', 'R');


	SELECT MAX(for_hire_id) FROM for_hire 
		WHERE appuser_id = inappuserid INTO vForhireId;

	UPDATE for_hire
		SET end_is_for_hire = inforhiredatetime
			WHERE for_hire_id = vForhireId;
	
END IF;		
END IF;

END$$
DELIMITER;
call set_for_hire(4, '2016-08-03 14:28:30', 2)
