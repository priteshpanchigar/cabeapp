-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `formatted_address` varchar(500) NOT NULL,
  `locality` varchar(300) DEFAULT NULL,
  `sublocality` varchar(300) DEFAULT NULL,
  `postal_code` int(15) DEFAULT NULL,
  `route` varchar(300) DEFAULT NULL,
  `neighborhood` varchar(300) DEFAULT NULL,
  `administrative_area_level_2` varchar(300) DEFAULT NULL,
  `administrative_area_level_1` varchar(300) DEFAULT NULL,
  `clientdatetime` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `od_formatted_address` (`formatted_address`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appusage
-- ----------------------------
DROP TABLE IF EXISTS `appusage`;
CREATE TABLE `appusage` (
  `appusage_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) NOT NULL DEFAULT '0',
  `clientfirstaccessdatetime` datetime DEFAULT NULL,
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `serverfirstaccessdatetime` datetime DEFAULT NULL,
  `serverlastaccessdatetime` datetime DEFAULT NULL,
  `versioncode` int(6) DEFAULT NULL,
  `version` varchar(25) DEFAULT NULL,
  `app` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`appusage_id`),
  UNIQUE KEY `appuser_id` (`appuser_id`,`app`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appuser
-- ----------------------------
DROP TABLE IF EXISTS `appuser`;
CREATE TABLE `appuser` (
  `appuser_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL DEFAULT '',
  `phoneno` varchar(20) DEFAULT NULL,
  `usertype` varchar(5) DEFAULT NULL,
  `verified` int(11) NOT NULL DEFAULT '0',
  `manually_verified` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(80) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `imei` varchar(50) NOT NULL DEFAULT '',
  `userprofileimagefilename` varchar(500) DEFAULT NULL,
  `home` varchar(500) DEFAULT NULL,
  `work` varchar(500) DEFAULT NULL,
  `home_lat` double DEFAULT NULL,
  `home_lng` double DEFAULT NULL,
  `work_lat` double DEFAULT NULL,
  `work_lng` double DEFAULT NULL,
  `clientlastaccessdatetime` datetime DEFAULT NULL,
  `first_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  `provider` varchar(10) DEFAULT NULL,
  `carrier` varchar(50) DEFAULT NULL,
  `product` varchar(50) DEFAULT NULL,
  `manufacturer` varchar(50) DEFAULT NULL,
  `android_release_version` varchar(20) DEFAULT NULL,
  `android_sdk_version` int(5) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `useragent` varchar(1000) DEFAULT NULL,
  `gcm_registration_id` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`appuser_id`),
  UNIQUE KEY `app_email_imei` (`email`,`imei`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for appuser_flag
-- ----------------------------
DROP TABLE IF EXISTS `appuser_flag`;
CREATE TABLE `appuser_flag` (
  `appuser_flag_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `is_for_hire` int(11) NOT NULL DEFAULT '0',
  `allowstrangernotifications` int(1) DEFAULT '1',
  `is_commercial` int(1) NOT NULL DEFAULT '0' COMMENT 'Populated for registered commercial driver',
  `shared_cab` int(5) NOT NULL DEFAULT '0',
  `is_admin` int(1) NOT NULL DEFAULT '0',
  `show_all_notifications` int(1) NOT NULL DEFAULT '0',
  `show_cab_notifications` int(1) NOT NULL DEFAULT '1',
  `show_me` int(1) NOT NULL DEFAULT '0',
  `receive_notifications` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`appuser_flag_id`),
  UNIQUE KEY `flag_appuser_id` (`appuser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for appversion
-- ----------------------------
DROP TABLE IF EXISTS `appversion`;
CREATE TABLE `appversion` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `versioncode` int(10) NOT NULL,
  `app` varchar(20) NOT NULL DEFAULT '',
  `link` varchar(300) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for booked_driver
-- ----------------------------
DROP TABLE IF EXISTS `booked_driver`;
CREATE TABLE `booked_driver` (
  `booked_driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_appuser_id` int(11) DEFAULT NULL,
  `passenger_appuser_id` int(11) DEFAULT NULL,
  `booking_time` datetime DEFAULT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `cancelled_driver` int(1) DEFAULT '0',
  `cancel_reason_code` varchar(10) DEFAULT NULL,
  `cancelled_driver_datetime` datetime DEFAULT NULL,
  `cancelled_passenger` int(1) DEFAULT '0',
  `cancelled_passenger_datetime` datetime DEFAULT NULL,
  `trip_timed_out` int(11) NOT NULL DEFAULT '0',
  `trip_cost` int(10) DEFAULT NULL,
  `trip_distance` double DEFAULT NULL,
  `actual_trip_distance` double DEFAULT NULL,
  `trip_time` time DEFAULT NULL,
  `trip_rating` double DEFAULT NULL,
  `trip_comment` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`booked_driver_id`),
  KEY `bd_driver_appuser_id` (`driver_appuser_id`),
  KEY `bd_passenger_appuser_id` (`passenger_appuser_id`) USING BTREE,
  KEY `bd_trip_id` (`trip_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for booked_driver_busy
-- ----------------------------
DROP TABLE IF EXISTS `booked_driver_busy`;
CREATE TABLE `booked_driver_busy` (
  `booked_busy_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_appuser_id` int(11) DEFAULT NULL,
  `booking_time` datetime DEFAULT NULL,
  `national_no` varchar(15) DEFAULT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `cancelled_driver` int(1) DEFAULT '0',
  `cancelled_driver_datetime` datetime DEFAULT NULL,
  `cancel_reason_code` varchar(10) DEFAULT NULL,
  `cancelled_passenger` int(1) DEFAULT '0',
  `cancelled_passenger_datetime` datetime DEFAULT NULL,
  `trip_timed_out` int(11) NOT NULL DEFAULT '0',
  `trip_cost` int(10) DEFAULT NULL,
  `trip_distance` double DEFAULT NULL,
  `trip_time` time DEFAULT NULL,
  `trip_rating` double DEFAULT NULL,
  `trip_comment` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`booked_busy_id`),
  KEY `bd_busy_driver_appuser_id` (`driver_appuser_id`) USING BTREE,
  KEY `bd_busy_trip_id` (`trip_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for booked_driver_kiosk
-- ----------------------------
DROP TABLE IF EXISTS `booked_driver_kiosk`;
CREATE TABLE `booked_driver_kiosk` (
  `booked_kiosk_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_appuser_id` int(11) DEFAULT NULL,
  `kiosk_id` int(11) DEFAULT NULL,
  `booking_time` datetime DEFAULT NULL,
  `national_no` varchar(15) DEFAULT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `cancelled_driver` int(1) DEFAULT '0',
  `cancelled_driver_datetime` datetime DEFAULT NULL,
  `cancel_reason_code` varchar(10) DEFAULT NULL,
  `cancelled_passenger` int(1) DEFAULT '0',
  `cancelled_passenger_datetime` datetime DEFAULT NULL,
  `trip_timed_out` int(11) NOT NULL DEFAULT '0',
  `trip_cost` int(10) DEFAULT NULL,
  `trip_distance` double DEFAULT NULL,
  `trip_time` time DEFAULT NULL,
  `trip_rating` double DEFAULT NULL,
  `trip_comment` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`booked_kiosk_id`),
  KEY `bd_kiosk_id` (`kiosk_id`),
  KEY `bd_kiosk_driver_appuser_id` (`driver_appuser_id`) USING BTREE,
  KEY `bd_kiosk_trip_id` (`trip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for booked_driver_shared
-- ----------------------------
DROP TABLE IF EXISTS `booked_driver_shared`;
CREATE TABLE `booked_driver_shared` (
  `booked_driver_shared_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_appuser_id` int(11) DEFAULT NULL,
  `passenger_appuser_id` int(11) DEFAULT NULL,
  `booking_time` datetime DEFAULT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `cancelled_driver` int(1) DEFAULT '0',
  `cancel_reason_code` varchar(10) DEFAULT NULL,
  `cancelled_driver_datetime` datetime DEFAULT NULL,
  `cancelled_passenger` int(1) DEFAULT '0',
  `cancelled_passenger_datetime` datetime DEFAULT NULL,
  `trip_timed_out` int(11) NOT NULL DEFAULT '0',
  `trip_cost` int(10) DEFAULT NULL,
  `trip_distance` double DEFAULT NULL,
  `trip_time` time DEFAULT NULL,
  `trip_rating` double DEFAULT NULL,
  `trip_comment` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`booked_driver_shared_id`),
  KEY `bd_driver_appuser_id` (`driver_appuser_id`),
  KEY `bd_passenger_appuser_id` (`passenger_appuser_id`) USING BTREE,
  KEY `bd_trip_id` (`trip_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for booked_driver_web
-- ----------------------------
DROP TABLE IF EXISTS `booked_driver_web`;
CREATE TABLE `booked_driver_web` (
  `booked_web_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_appuser_id` int(11) DEFAULT NULL,
  `booking_time` datetime DEFAULT NULL,
  `national_no` varchar(15) DEFAULT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `cancelled_driver` int(1) DEFAULT '0',
  `cancelled_driver_datetime` datetime DEFAULT NULL,
  `cancel_reason_code` varchar(10) DEFAULT NULL,
  `trip_timed_out` int(11) NOT NULL DEFAULT '0',
  `trip_cost` int(10) DEFAULT NULL,
  `trip_distance` double DEFAULT NULL,
  `trip_time` time DEFAULT NULL,
  PRIMARY KEY (`booked_web_id`),
  KEY `bd_web_driver_appuser_id` (`driver_appuser_id`) USING BTREE,
  KEY `bd_web_trip_id` (`trip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for cancel_reason
-- ----------------------------
DROP TABLE IF EXISTS `cancel_reason`;
CREATE TABLE `cancel_reason` (
  `cancel_reason_id` int(11) NOT NULL AUTO_INCREMENT,
  `cancel_reason` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `cancel_reason_code` varchar(255) DEFAULT NULL,
  `cancel_reason_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`cancel_reason_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `jtrack_company_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `company_code` varchar(255) DEFAULT NULL,
  `c_contact_person` varchar(255) DEFAULT NULL,
  `c_mobile` varchar(15) DEFAULT NULL,
  `c_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for device_reading
-- ----------------------------
DROP TABLE IF EXISTS `device_reading`;
CREATE TABLE `device_reading` (
  `reading_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(9) DEFAULT NULL,
  `imei` bigint(15) DEFAULT NULL,
  `packet_length` int(11) DEFAULT NULL,
  `packet_type` int(11) DEFAULT NULL,
  `clientdatetime` datetime DEFAULT NULL,
  `loctime` datetime DEFAULT NULL,
  `gps_data_valid` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `latitude_direction` varchar(1) DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `longitude_direction` varchar(1) DEFAULT NULL,
  `speed` int(11) DEFAULT NULL,
  `heading_angle` double DEFAULT NULL,
  `total_satellites` int(11) DEFAULT NULL,
  `gsm_signal_strength` int(11) DEFAULT NULL,
  `panic_status` int(11) DEFAULT NULL,
  `vehicle_movement_status` varchar(1) DEFAULT NULL,
  `gps_quality` varchar(1) DEFAULT NULL,
  `power_status` int(11) DEFAULT NULL,
  `odometer_reading_pulse` double DEFAULT NULL,
  `odometer_reading_gps` double DEFAULT NULL,
  `metering_status` varchar(1) DEFAULT NULL,
  `trip_id` int(11) DEFAULT NULL,
  `running_distance` double DEFAULT NULL,
  `GNX_ver` varchar(8) DEFAULT NULL,
  `crc` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`reading_id`),
  KEY `dr_imei` (`imei`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for driver
-- ----------------------------
DROP TABLE IF EXISTS `driver`;
CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `jtrack_driver_id` int(11) DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `clientdatetime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `country_code` varchar(10) DEFAULT NULL,
  `phoneno` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(80) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `gcm_registration_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`driver_id`),
  UNIQUE KEY `appuser_id` (`appuser_id`),
  UNIQUE KEY `unique_phone` (`phoneno`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for driver_address
-- ----------------------------
DROP TABLE IF EXISTS `driver_address`;
CREATE TABLE `driver_address` (
  `driver_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_profile_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `google_address` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `t_address_1` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `t_address_2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `t_city_town` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `t_landmark` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `t_state` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `t_pincode` int(10) DEFAULT NULL,
  `p_address_1` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `p_address_2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `p_city_town` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `p_landmark` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `p_state` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `p_pincode` int(10) DEFAULT NULL,
  PRIMARY KEY (`driver_address_id`),
  UNIQUE KEY `dr_driver_profile_id` (`driver_profile_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for driver_image
-- ----------------------------
DROP TABLE IF EXISTS `driver_image`;
CREATE TABLE `driver_image` (
  `driver_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_profile_id` int(11) DEFAULT NULL,
  `image_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image_type` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image_createion_datetime` datetime DEFAULT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`driver_image_id`),
  UNIQUE KEY `image_unique` (`driver_profile_id`,`image_type`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for driver_other_company_lookup
-- ----------------------------
DROP TABLE IF EXISTS `driver_other_company_lookup`;
CREATE TABLE `driver_other_company_lookup` (
  `driver_other_member_lookup_id` int(11) NOT NULL AUTO_INCREMENT,
  `other_company_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `other_company_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`driver_other_member_lookup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for driver_other_member
-- ----------------------------
DROP TABLE IF EXISTS `driver_other_member`;
CREATE TABLE `driver_other_member` (
  `driver_other_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_profile_id` int(11) DEFAULT NULL,
  `other_company_code` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `driver_other_company_id` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`driver_other_id`),
  UNIQUE KEY `other_unique_key` (`driver_profile_id`,`other_company_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for driver_profile
-- ----------------------------
DROP TABLE IF EXISTS `driver_profile`;
CREATE TABLE `driver_profile` (
  `driver_profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `unique_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `unique_key` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `clientdatetime` datetime DEFAULT NULL,
  `profile_submitted` int(5) DEFAULT NULL,
  `driver_firstname` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `driver_lastname` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `driver_dob` date DEFAULT NULL,
  `driver_email` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `driver_phoneno` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `driver_gender` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `driver_pancard` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `driver_aadhar` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `driver_license_no` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `driver_permit_flag` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `driver_permit_no` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `driver_badge_flag` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `driver_badge_no` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `driver_incity_since` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `cab_needed_flag` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `driver_vehicle_no` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `remark` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `internal_comment` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `payment_status` int(1) DEFAULT NULL,
  `payment_amount` int(11) DEFAULT NULL,
  `verification_completed` int(1) NOT NULL DEFAULT '0',
  `private_verification` int(1) NOT NULL DEFAULT '0',
  `police_verification` int(1) NOT NULL DEFAULT '0',
  `referral_walk_in` varchar(255) DEFAULT NULL,
  `referral_supervisor` varchar(255) DEFAULT NULL,
  `referral_driver` varchar(255) DEFAULT NULL,
  `receipt_no` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `receipt_no_2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `receipt_no_3` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `driver_backout_reason` varchar(500) DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `receipt_date_2` date DEFAULT NULL,
  `receipt_date_3` date DEFAULT NULL,
  `tr_license_exp_date` date DEFAULT NULL,
  `last_sync_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`driver_profile_id`),
  UNIQUE KEY `dr_phoneno` (`driver_phoneno`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for driver_shift
-- ----------------------------
DROP TABLE IF EXISTS `driver_shift`;
CREATE TABLE `driver_shift` (
  `shift_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `driver_vehicle_id` int(11) DEFAULT NULL,
  `start_shift_time` datetime DEFAULT NULL,
  `end_shift_time` datetime DEFAULT NULL,
  `is_login` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shift_id`),
  KEY `cds_appuser_id` (`appuser_id`),
  KEY `ds_driver_vehicle_id` (`driver_vehicle_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for driver_vehicle
-- ----------------------------
DROP TABLE IF EXISTS `driver_vehicle`;
CREATE TABLE `driver_vehicle` (
  `driver_vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_code` varchar(255) DEFAULT NULL,
  `vehicleno` int(6) DEFAULT NULL,
  `device_imei` bigint(16) DEFAULT NULL,
  `reading_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `jtrack_vno` int(11) DEFAULT NULL,
  `jtrack_vehicle_id` int(11) DEFAULT NULL,
  `owner_appuser_id` int(11) DEFAULT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  `vehicleno_state` varchar(255) DEFAULT NULL,
  `vehicleno_city` varchar(255) DEFAULT NULL,
  `vehicleno_code` varchar(255) DEFAULT NULL,
  `gps_device_no` varchar(255) DEFAULT NULL,
  `vehicle_company` varchar(25) DEFAULT NULL,
  `vehicle_category` varchar(255) DEFAULT NULL,
  `vehicle_model` varchar(20) DEFAULT NULL,
  `vehicle_type` varchar(20) DEFAULT NULL,
  `vehicle_color` varchar(20) DEFAULT NULL,
  `rate_per_km` int(11) DEFAULT NULL,
  `seating` int(5) DEFAULT NULL,
  `is_booked` int(3) NOT NULL DEFAULT '0',
  `login_requested` int(11) NOT NULL DEFAULT '0',
  `vehicle_distinctive_marks` varchar(20) DEFAULT NULL,
  `date_registered` datetime DEFAULT NULL,
  `minimum_fare` int(11) DEFAULT NULL,
  `vehicle_ownership` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`driver_vehicle_id`),
  KEY `dv_company_id` (`company_id`),
  KEY `dv_imei` (`device_imei`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for facebook_profile
-- ----------------------------
DROP TABLE IF EXISTS `facebook_profile`;
CREATE TABLE `facebook_profile` (
  `facebook_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `facebook_email` varchar(50) DEFAULT NULL,
  `firstname` varchar(25) DEFAULT NULL,
  `lastname` varchar(25) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `userprofileimagefilename` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`facebook_id`),
  UNIQUE KEY `facebook_unique` (`facebook_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for filepaths
-- ----------------------------
DROP TABLE IF EXISTS `filepaths`;
CREATE TABLE `filepaths` (
  `filepaths_id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofileimagepath` varchar(1000) DEFAULT NULL,
  `pathtype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`filepaths_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for fixed_address
-- ----------------------------
DROP TABLE IF EXISTS `fixed_address`;
CREATE TABLE `fixed_address` (
  `fixed_address_id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(50) DEFAULT NULL,
  `landmark` varchar(50) DEFAULT NULL,
  `pick_drop_point` varchar(50) DEFAULT NULL,
  `formatted_address` varchar(500) NOT NULL,
  `locality` varchar(300) DEFAULT NULL,
  `sublocality` varchar(300) DEFAULT NULL,
  `postal_code` int(15) DEFAULT NULL,
  `route` varchar(300) DEFAULT NULL,
  `neighborhood` varchar(300) DEFAULT NULL,
  `administrative_area_level_2` varchar(300) DEFAULT NULL,
  `administrative_area_level_1` varchar(300) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`fixed_address_id`),
  UNIQUE KEY `pick_drop_unique` (`area`,`landmark`,`pick_drop_point`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for for_hire
-- ----------------------------
DROP TABLE IF EXISTS `for_hire`;
CREATE TABLE `for_hire` (
  `for_hire_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `start_is_for_hire` datetime DEFAULT NULL,
  `end_is_for_hire` datetime DEFAULT NULL,
  PRIMARY KEY (`for_hire_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for franchise
-- ----------------------------
DROP TABLE IF EXISTS `franchise`;
CREATE TABLE `franchise` (
  `franchise_id` int(11) NOT NULL AUTO_INCREMENT,
  `jtrack_franchise_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `franchise_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `franchise_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`franchise_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for googleplus_profile
-- ----------------------------
DROP TABLE IF EXISTS `googleplus_profile`;
CREATE TABLE `googleplus_profile` (
  `googleplus_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(25) DEFAULT NULL,
  `lastname` varchar(25) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `userprofileimagefilename` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`googleplus_id`),
  UNIQUE KEY `g_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for kiosk
-- ----------------------------
DROP TABLE IF EXISTS `kiosk`;
CREATE TABLE `kiosk` (
  `kiosk_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `building` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `postal_code` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `imei` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`kiosk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for last_location
-- ----------------------------
DROP TABLE IF EXISTS `last_location`;
CREATE TABLE `last_location` (
  `appuser_id` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` int(11) DEFAULT NULL,
  `loctime` datetime DEFAULT NULL,
  `clientaccessdatetime` datetime DEFAULT NULL,
  `speed` float DEFAULT NULL,
  `altitude` float DEFAULT NULL,
  `bearing` double DEFAULT NULL,
  `provider` varchar(100) DEFAULT NULL,
  `last_location_sent_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`appuser_id`),
  KEY `l_lat` (`lat`),
  KEY `l_lng` (`lng`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `location_id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `accuracy` int(11) DEFAULT NULL,
  `altitude` double DEFAULT NULL,
  `bearing` float DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `loctime` datetime DEFAULT NULL,
  `clientaccessdatetime` datetime DEFAULT NULL,
  `speed` float DEFAULT NULL,
  `cumulative_distance` double DEFAULT NULL,
  `provider` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_appuser_id` (`appuser_id`),
  KEY `loc_id` (`location_id`),
  KEY `ct_datetime` (`clientaccessdatetime`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for meter_status
-- ----------------------------
DROP TABLE IF EXISTS `meter_status`;
CREATE TABLE `meter_status` (
  `meter_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `appuser_id` int(11) DEFAULT NULL,
  `meter_flag` int(5) DEFAULT '0',
  `meter_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`meter_status_id`),
  UNIQUE KEY `m_flag` (`appuser_id`,`meter_flag`,`meter_datetime`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for passenger_shared_trip
-- ----------------------------
DROP TABLE IF EXISTS `passenger_shared_trip`;
CREATE TABLE `passenger_shared_trip` (
  `passenger_shared_trip_id` int(11) NOT NULL AUTO_INCREMENT,
  `passenger_appuser_id` int(11) NOT NULL DEFAULT '0',
  `trip_id` int(11) NOT NULL DEFAULT '0',
  `has_joined` int(11) DEFAULT NULL,
  `has_joined_datetime` datetime DEFAULT NULL,
  `cancel_join` int(11) NOT NULL DEFAULT '0',
  `cancel_join_datetime` datetime DEFAULT NULL,
  `cancel_reason_code` varchar(255) DEFAULT NULL,
  `has_jumped_in` int(1) NOT NULL DEFAULT '0',
  `jumpin_datetime` datetime DEFAULT NULL,
  `has_jumped_out` int(1) NOT NULL DEFAULT '0',
  `join_lat` double DEFAULT NULL,
  `join_lng` double DEFAULT NULL,
  `jumpout_datetime` datetime DEFAULT NULL,
  `jumpin_address` varchar(255) DEFAULT NULL,
  `nearby_notication` int(1) NOT NULL DEFAULT '0',
  `nearby_distance` double DEFAULT NULL,
  `jumpin_loc_id` int(11) DEFAULT NULL,
  `jumpout_loc_id` int(11) DEFAULT NULL,
  `jumpin_notification_sent` int(11) DEFAULT '0',
  `jumpout_notification_sent` int(11) DEFAULT '0',
  `trip_distance` double DEFAULT NULL,
  `trip_time` time DEFAULT NULL,
  `trip_rating` double DEFAULT NULL,
  `trip_comment` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`passenger_shared_trip_id`),
  UNIQUE KEY `passenger_unique_key` (`trip_id`,`passenger_appuser_id`) USING BTREE,
  KEY `sptt_appuser_id` (`passenger_appuser_id`),
  KEY `sptt_trip_id` (`trip_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for service_eligibility
-- ----------------------------
DROP TABLE IF EXISTS `service_eligibility`;
CREATE TABLE `service_eligibility` (
  `service_eligibility_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `service_code_eligible` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`service_eligibility_id`),
  KEY `se_service_code` (`service_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for service_lookup
-- ----------------------------
DROP TABLE IF EXISTS `service_lookup`;
CREATE TABLE `service_lookup` (
  `service_lookup_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_code` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `service_description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `metered` int(11) DEFAULT NULL,
  `minimum_fare` int(11) DEFAULT NULL,
  `rate_per_km` int(11) DEFAULT NULL,
  PRIMARY KEY (`service_lookup_id`),
  KEY `sl_service_code` (`service_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ;

-- ----------------------------
-- Table structure for trips
-- ----------------------------
DROP TABLE IF EXISTS `trips`;
CREATE TABLE `trips` (
  `trip_id` int(11) NOT NULL AUTO_INCREMENT,
  `triptype` varchar(3) DEFAULT NULL,
  `appuser_id` int(11) DEFAULT NULL,
  `trip_action` varchar(2) DEFAULT 'C',
  `trip_action_time` datetime DEFAULT NULL,
  `trip_abort_time` datetime DEFAULT NULL,
  `tripcreationtime` datetime DEFAULT NULL,
  `planned_start_datetime` datetime DEFAULT NULL,
  `trip_start_time` datetime DEFAULT NULL,
  `trip_end_time` datetime DEFAULT NULL,
  `start_location_id` int(11) DEFAULT NULL,
  `last_location_id` int(11) DEFAULT NULL,
  `fromaddress` varchar(250) DEFAULT NULL,
  `fromshortaddress` varchar(200) DEFAULT NULL,
  `toaddress` varchar(250) DEFAULT NULL,
  `toshortaddress` varchar(200) DEFAULT NULL,
  `fromsublocality` varchar(300) DEFAULT NULL,
  `tosublocality` varchar(300) DEFAULT NULL,
  `fromlat` double DEFAULT NULL,
  `fromlng` double DEFAULT NULL,
  `tolat` double DEFAULT NULL,
  `tolng` double DEFAULT NULL,
  `trips_unique_ms` bigint(20) DEFAULT NULL,
  `planned_start_time` varchar(10) DEFAULT NULL,
  `something_went_wrong` int(11) NOT NULL DEFAULT '0',
  `triptime` int(100) DEFAULT NULL,
  `tripdistance` double DEFAULT NULL,
  `hasdriver_moved` int(1) DEFAULT '0',
  `hasdriver_moved_time` datetime DEFAULT NULL,
  `hasdriver_started` int(1) DEFAULT '0',
  `trip_end_notification_sent` int(1) NOT NULL DEFAULT '0',
  `trip_directions_polyline` varchar(2000) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `open_trip` int(5) DEFAULT '0',
  `shared_cab` int(5) NOT NULL DEFAULT '0',
  `open_trip_notification_sent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`trip_id`),
  UNIQUE KEY `Unique_trip` (`appuser_id`,`tripcreationtime`),
  KEY `st_trip_id` (`trip_id`),
  KEY `st_appuser_id` (`appuser_id`),
  KEY `st_trip_action` (`trip_action`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for trips_path
-- ----------------------------
DROP TABLE IF EXISTS `trips_path`;
CREATE TABLE `trips_path` (
  `trip_path_id` int(11) NOT NULL AUTO_INCREMENT,
  `trip_id` int(11) DEFAULT NULL,
  `nodeno` int(11) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`trip_path_id`),
  KEY `stp_trip_id` (`trip_id`),
  KEY `stp_lat_lng` (`lat`,`lng`),
  KEY `stp_nodeno` (`nodeno`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithme_contacts
-- ----------------------------
DROP TABLE IF EXISTS `walkwithme_contacts`;
CREATE TABLE `walkwithme_contacts` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_contact_user_emailid` varchar(75) DEFAULT NULL,
  `w_contact_name` varchar(75) DEFAULT NULL,
  `w_contact_emailid` varchar(75) DEFAULT NULL,
  `w_creationdatetime` datetime NOT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `unique_walkwithme` (`w_contact_user_emailid`,`w_contact_emailid`,`w_creationdatetime`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithmelocupd
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmelocupd`;
CREATE TABLE `walkwithmelocupd` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_w_c_id` int(11) DEFAULT NULL,
  `user_emailid` varchar(100) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `datetm` datetime DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `accuracy` double DEFAULT NULL,
  `serverdatetime` datetime DEFAULT NULL,
  `intrip` bit(1) DEFAULT NULL,
  `mode` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithmepermissions
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmepermissions`;
CREATE TABLE `walkwithmepermissions` (
  `w_c_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_c_user_emailid` varchar(100) DEFAULT NULL,
  `randomstring` varchar(1000) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `stopflag` varchar(1) DEFAULT NULL,
  `intrip` bit(1) DEFAULT NULL,
  `user_link` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`w_c_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for walkwithmepermissionsstop
-- ----------------------------
DROP TABLE IF EXISTS `walkwithmepermissionsstop`;
CREATE TABLE `walkwithmepermissionsstop` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `w_s_user_emailid` varchar(100) DEFAULT NULL,
  `u_randomstring` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;