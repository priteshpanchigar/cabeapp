DROP FUNCTION IF EXISTS insert_location;
CREATE  FUNCTION `insert_location`(`inappuserid` int, 
	`inclientdatetime` datetime, `inlat` double, `inlng` double, 
	`inaccuracy` INT, `inlocationdatetime` datetime, `inprovider` varchar(100), 
	`inspeed` float, `inbearing` float, `inaltitude` double) RETURNS int(11)
quit_proc:BEGIN
	DECLARE v_location_id INT DEFAULT NULL;
	DECLARE vLastLocationTime datetime DEFAULT NULL;
	IF inappuserid IS NULL THEN
		RETURN v_location_id;
		LEAVE quit_proc;
	END IF;
	
	INSERT INTO location(appuser_id, clientaccessdatetime, lat, lng, accuracy, loctime, provider, 
	speed, bearing, altitude)
	VALUES (inappuserid, inclientdatetime, inlat, inlng, inaccuracy, inlocationdatetime, inprovider,
	inspeed, inbearing, inaltitude);

SELECT LAST_INSERT_ID() INTO v_location_id;

SELECT loctime FROM last_location
	WHERE appuser_id = inappuserid INTO vLastLocationTime;

IF vLastLocationTime <= inlocationdatetime THEN
	INSERT INTO last_location( appuser_id, clientaccessdatetime, lat, lng, accuracy, loctime, provider, 
	speed, bearing, altitude)
	VALUES (inappuserid, inclientdatetime, inlat, inlng, inaccuracy, inlocationdatetime, inprovider,
	inspeed, inbearing, inaltitude)
		ON DUPLICATE KEY UPDATE
	clientaccessdatetime = inclientdatetime,
	lat	= inlat,
	lng = inlng,
	accuracy = inaccuracy,
	loctime = inlocationdatetime,
	provider = inprovider,
	speed = inspeed,
	bearing = inbearing,
	altitude = inaltitude;
END IF;

	RETURN v_location_id;

END;


