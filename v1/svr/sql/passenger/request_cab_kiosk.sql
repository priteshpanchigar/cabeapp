DROP PROCEDURE IF EXISTS `request_cab_kiosk`;
DELIMITER $$
CREATE  PROCEDURE `request_cab_kiosk`(IN innationalno varchar(15), IN inimei VARCHAR(30),
						IN `inclientdatetime` datetime,  IN inservicecode varchar(10))
proc_label:BEGIN
DECLARE driverAppuserid INT;
DECLARE ntripid INT DEFAULT 0;
DECLARE hastripid INT;
DECLARE nFoundDriver INT DEFAULT 0;
DECLARE vLat DOUBLE DEFAULT NULL;
DECLARE vLng DOUBLE DEFAULT NULL;
DECLARE vkioskId INT DEFAULT NULL;
DECLARE vBuilding VARCHAR (50) DEFAULT NULL;
DECLARE vAddress VARCHAR (300) DEFAULT NULL;
DECLARE vPostalCode varchar (15) DEFAULT NULL;
SET @inDistance = 5;

SELECT kiosk_id, building, address, postal_code, lat, lng  
FROM kiosk
	WHERE imei = inimei 
INTO  
vkioskId, vBuilding, vAddress, vPostalCode, vLat, vLng;

SELECT a.appuser_id
	FROM appuser_flag a
		INNER JOIN last_location l ON l.appuser_id =  a.appuser_id
				WHERE is_for_hire = 1 
				AND 	(SELECT service_code FROM vwcommercialvehicle vw 
								WHERE  vw.appuser_id = a.appuser_id) = inservicecode		
				AND ABS(vLat - l.lat) < @inDistance / 100 
				AND ABS(vLng - l.lng) < @inDistance / 100
				AND TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime) < 3*60
			ORDER BY getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng)
			LIMIT 1 INTO driverAppuserid;
#SELECT driverAppuserid;

IF driverAppuserid IS NULL THEN 
	SELECT 0 driver_found;
	LEAVE proc_label;
END IF;
	INSERT INTO trips(appuser_id, triptype, fromaddress, fromlat, fromlng, 
		 tripcreationtime, trip_action, trip_action_time)
	VALUES (driverAppuserid, 'K', vAddress, vLat, vLng,
		 inclientdatetime, 'C', inclientdatetime);
	SET ntripid = LAST_INSERT_ID(); 
#SELECT driverAppuserid, inappuserid, ntripid,inclientdatetime;
	INSERT  INTO booked_driver_kiosk(driver_appuser_id, kiosk_id, trip_id, booking_time, national_no)
		VALUES (driverAppuserid, vkioskId, ntripid, inclientdatetime, innationalno);
	
	UPDATE appuser_flag
		SET is_for_hire = 0
		WHERE appuser_id = driverAppuserid;
		

	SELECT 1 driver_found,
	IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
	ct.trip_id, bd.driver_appuser_id,  bd.cancelled_driver,
	vw.image_path, vw.image_name,
	vw.email, vw.country_code, vw.phoneno driver_phoneno, 	vw.firstname, vw.lastname, 
	bd.kiosk_id, bd.booking_time, k.address, k.building, k.lat, k.lng, k.postal_code,
	ct.tripcreationtime,  ct.trip_action, ct.trip_action_time, 
	vehicleno, vehicle_company,
	vehicle_category, vehicle_model, vehicle_type, vehicle_color, service_code
	FROM trips ct
		INNER JOIN booked_driver_kiosk bd	
		ON ct.trip_id = bd.trip_id
		INNER  JOIN vwcommercialvehicle vw
		ON vw.appuser_id = bd.driver_appuser_id	
		INNER JOIN kiosk k
		ON k.kiosk_id = bd.kiosk_id
		INNER JOIN last_location l
		ON l.appuser_id = bd.driver_appuser_id		
WHERE ct.trip_id = ntripid  
AND bd.kiosk_id = vkioskId

ORDER BY bd.booked_kiosk_id DESC LIMIT 1; 

END$$
DELIMITER; 

call request_cab_kiosk("9820843471","352196071779888","2016-5-26 13:28:24",'CC');