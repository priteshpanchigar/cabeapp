DROP PROCEDURE IF EXISTS get_driver_position;
DELIMITER $$
CREATE  PROCEDURE `get_driver_position`(IN intriptype varchar(10), IN intripid INT, IN inclientdatetime datetime)
BEGIN

DECLARE vSomethingWentWrong INT DEFAULT -1;
DECLARE vDriverApperuseid INT DEFAULT -1;
DECLARE vTripAction VARCHAR(10) DEFAULT NULL;
DECLARE vTripCancelled INT DEFAULT 0;
DECLARE vLat DOUBLE DEFAULT NULL;
DECLARE vLng DOUBLE DEFAULT NULL;

IF intriptype = 'P' THEN

SELECT  driver_appuser_id, cancelled_driver  FROM booked_driver_shared
WHERE trip_id = intripid
		INTO  vDriverApperuseid, vTripCancelled;

ELSE

SELECT  driver_appuser_id, cancelled_driver  FROM booked_driver
WHERE trip_id = intripid
		INTO  vDriverApperuseid, vTripCancelled;

END IF;			

SELECT trip_action, fromlat , fromlng , something_went_wrong FROM trips
	WHERE trip_id = intripid 
		INTO vTripAction, vLat, vLng, vSomethingWentWrong;

SELECT IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
(getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng)) distance , 
getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng) /20*60*2 eta,
vTripAction as trip_action, vTripCancelled as cancelled_driver, 
 vSomethingWentWrong as something_went_wrong,
l.lat, l.lng, l.accuracy, l.altitude, l.bearing, l.loctime, l.clientaccessdatetime, l.speed  
FROM appuser ap
	INNER JOIN last_location l
	ON l.appuser_id = ap.appuser_id
	WHERE ap.appuser_id = vDriverApperuseid;


	
END$$
DELIMITER ;

CALL get_driver_position('A',546, '2016-08-11 14:24:25');