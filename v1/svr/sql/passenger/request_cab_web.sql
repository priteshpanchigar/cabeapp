DROP PROCEDURE IF EXISTS `request_cab_web`;
DELIMITER $$
CREATE PROCEDURE `request_cab_web`(IN innationalno varchar(15), IN `infromlat` double, IN `infromlng` double,
								   IN `inclientdatetime` datetime,  IN inservicecode varchar(10))

proc_label:BEGIN
DECLARE driverAppuserid INT;
DECLARE ntripid INT DEFAULT 0;
DECLARE hastripid INT;
DECLARE nFoundDriver INT DEFAULT 0;
DECLARE vWaitingForCab INT ;

SET @inDistance = 5;

SELECT COUNT(*) FROM booked_driver_web bw
	INNER JOIN trips t
	ON t.trip_id = bw.trip_id
	WHERE ((cancelled_driver = 0 or trip_timed_out = 0) AND  NOT t.trip_action IN ('E', 'A'))
	AND national_no =  innationalno
	INTO vWaitingForCab;
	
IF vWaitingForCab = 1 THEN
LEAVE proc_label;
END IF;



SELECT a.appuser_id
	FROM appuser_flag a
		INNER JOIN last_location l ON l.appuser_id =  a.appuser_id
				WHERE is_for_hire = 1 
				AND 	(SELECT service_code FROM vwcommercialvehicle vw 
								WHERE  vw.appuser_id = a.appuser_id) = inservicecode		
				AND ABS(infromlat - l.lat) < @inDistance / 100 
				AND ABS(infromlng - l.lng) < @inDistance / 100
				AND TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime) < 3*60
			ORDER BY getDistanceBetweenPoints(infromlat, infromlng, l.lat, l.lng)
			LIMIT 1 INTO driverAppuserid;

#SELECT driverAppuserid;
IF driverAppuserid IS NULL THEN 
	SELECT 0 driver_found;
	LEAVE proc_label;
END IF;
	UPDATE appuser_flag
		SET is_for_hire = 0
		WHERE appuser_id = driverAppuserid;
		
	INSERT INTO trips(appuser_id, triptype, fromlat, fromlng, 
		 tripcreationtime, trip_action, trip_action_time)
	VALUES (driverAppuserid, 'W',  infromlat, infromlng,
		 inclientdatetime, 'C', inclientdatetime);
	SET ntripid = LAST_INSERT_ID(); 
#SELECT driverAppuserid,  ntripid,inclientdatetime;
	INSERT  INTO booked_driver_web(driver_appuser_id,trip_id, booking_time,  national_no)
		VALUES (driverAppuserid,  ntripid,inclientdatetime, innationalno);
	
	
	SELECT 1 driver_found, 
IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
	ct.trip_id, bd.driver_appuser_id,  bd.cancelled_driver, 
		vw.email, vw.country_code, vw.phoneno driver_phoneno, 
	vw.firstname, vw.lastname, vw.gender, 
(SELECT userprofileimagepath FROM filepaths WHERE pathtype = 'UP'
		ORDER BY filepaths_id DESC limit 1)userprofileimagepath,
	bd.booking_time, ct.triptype,  ct.fromaddress, 
	ct.toaddress, ct.fromlat as lat, ct.fromlng as lng, ct.tolat, ct.tolng,
	ct.tripcreationtime,  ct.trip_action, ct.trip_action_time, 
	 vehicleno, vw.vehicle_company,
	vehicle_category, vehicle_model, vehicle_type, vehicle_color,   service_code
	FROM trips ct
		INNER JOIN booked_driver_web bd	
		ON ct.trip_id = bd.trip_id
		INNER  JOIN vwcommercialvehicle vw
		ON vw.appuser_id = bd.driver_appuser_id
	INNER JOIN last_location l
ON l.appuser_id = bd.driver_appuser_id
WHERE ct.trip_id = ntripid 
ORDER BY bd.booked_web_id DESC LIMIT 1; 

END$$
DELIMITER;

call request_cab_web(7506988274,"19.1045899","72.8506549","2016-8-27 12:22:59",'BY');