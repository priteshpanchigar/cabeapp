DROP PROCEDURE IF EXISTS `no_cab_found`;
DELIMITER $$
CREATE  PROCEDURE `no_cab_found`(IN intripid INT(5), IN inclientdatetime datetime)
BEGIN

	UPDATE trips
		SET trip_action = "A",
			trip_action_time = inclientdatetime
		WHERE trip_id = intripid;

SELECT trip_id, trip_action, trip_action_time FROM trips
	WHERE trip_id = intripid;
		

END$$
DELIMITER ;
CALL no_cab_found(48, '2016-07-07 15:48:01');
