DROP PROCEDURE IF EXISTS user_access;
DELIMITER $$
CREATE  PROCEDURE `user_access`(IN `inregid` varchar(255), 	IN `incountrycode` varchar(05), 
	IN `inphoneno` varchar(20), IN `inclientdatetime` datetime, IN `inemailid`  varchar(100),	
	IN `inlatno` double, 		IN `inlngno` double,			IN `inacc` int,	
	IN `inlocationdatetime` datetime,IN `inprovider` varchar(10),IN `inspeed` float, 
	IN `inbearing` float,			IN `inaltitude` double,		IN `inimeino` varchar(30),
	IN `incarrier` varchar(30),		IN `inproduct` varchar(30), IN `inmanufacturer` varchar(30),
	IN `inapp` varchar(05), 		IN `inversion` varchar(25),	IN `inversioncode` INT,
	IN `inmodule` varchar(20),		IN `inip` varchar(50),		IN `inuseragent` varchar(300),
	IN `inandroid_release_version` varchar(30),					IN `inandroid_sdk_version` int,
	IN `inverified` int, IN inusertype varchar(5))
BEGIN
	DECLARE vAppUserId int;
	DECLARE v_APPUSAGE_ID int;
	DECLARE v_clientdttm datetime;
	DECLARE v_serverdttm datetime;
	DECLARE v_location_id INT DEFAULT 0;
	DECLARE countryphone VARCHAR(20) DEFAULT '';
	SET @isInsert = 0;
	SET @inDistance = 5;

	SELECT appuser_id FROM appuser
	WHERE email = inemailid
	AND imei = inimeino
	INTO vAppUserId;

#	SELECT 'appuser_id : ', vAppUserId;

	IF (vAppUserId IS NULL) THEN
		INSERT INTO appuser(gcm_registration_id, email, phoneno, imei, 
			lat, lng, accuracy, provider,
			carrier, product, manufacturer, android_release_version, 
			android_sdk_version, country_code, clientlastaccessdatetime, usertype)
		VALUES(inregid, inemailid, inphoneno, inimeino, 
		   inlatno, inlngno, inacc, inprovider, 
		   incarrier, inproduct, inmanufacturer, inandroid_release_version, 
		   inandroid_sdk_version, incountrycode, inclientdatetime, inusertype);
		   SELECT LAST_INSERT_ID() INTO vAppUserId;
		   SET @isInsert = 1;
	
		INSERT INTO appusage(appuser_id, clientfirstaccessdatetime, 
			clientlastaccessdatetime, serverfirstaccessdatetime, 
			serverlastaccessdatetime, version, versioncode, app)
		VALUES(vAppUserId, inclientdatetime, inclientdatetime, 
			NOW(), NOW(), inversion, inversioncode, inapp)
			ON DUPLICATE KEY UPDATE    
			version = COALESCE(inversion, version);
	END IF;
	
	IF (vAppUserId IS NOT NULL) THEN
		#SELECT vAppUserId;
		
		SET v_location_id = insert_location(vAppUserId, inclientdatetime, 
						inlatno, inlngno, inacc, inlocationdatetime,
						inprovider, inspeed, inbearing, inaltitude);

#		SELECT 'Location id : ', v_location_id;
		IF v_location_id IS NULL THEN
			SET v_location_id = -1;
		END IF;
		
		IF (v_location_id > 0) THEN	
			IF @isInsert = 1 THEN
#				SELECT v_location_id;
				UPDATE appuser
				SET first_location_id = v_location_id,
					last_location_id = v_location_id, 
					clientlastaccessdatetime = inclientdatetime
				WHERE appuser_id = vAppUserId; 
			ELSE
#				SELECT vAppUserId, v_location_id;
				UPDATE appuser
					SET last_location_id = v_location_id,
					clientlastaccessdatetime = inclientdatetime
				WHERE appuser_id = vAppUserId; 
			END IF;
		END IF;	
		
UPDATE appuser
			SET first_location_id = COALESCE(first_location_id, last_location_id),
				gcm_registration_id = COALESCE(inregid, gcm_registration_id),
				ip = (COALESCE(inip, ip)),
				lat = (COALESCE(inlatno, lat)),
				lng = (COALESCE(inlngno, lng)),
				accuracy = (COALESCE(inacc, accuracy)),
				provider = (COALESCE(inprovider, provider)),
				useragent = (COALESCE(inuseragent, useragent)),
				android_release_version = (COALESCE(inandroid_release_version, android_release_version)),
				android_sdk_version = (COALESCE(inandroid_sdk_version, android_sdk_version)),
				clientlastaccessdatetime =(COALESCE(inclientdatetime, clientlastaccessdatetime )),
				country_code = (COALESCE(incountrycode,country_code)),
				phoneno = (COALESCE( inphoneno, phoneno)),
				verified  = (COALESCE(inverified, verified)),
				usertype = (COALESCE(inusertype, usertype))
			WHERE appuser_id = vAppUserId;


		INSERT INTO appusage(appuser_id, clientfirstaccessdatetime, 
			clientlastaccessdatetime, serverfirstaccessdatetime, 
			serverlastaccessdatetime, version, versioncode, app)
		VALUES(vAppUserId, inclientdatetime, inclientdatetime, 
			NOW(), NOW(), inversion, inversioncode, inapp)
			ON DUPLICATE KEY UPDATE    
			clientlastaccessdatetime = COALESCE(inclientdatetime, clientlastaccessdatetime), 
			serverlastaccessdatetime = COALESCE(NOW(), serverlastaccessdatetime),
			version = COALESCE(inversion, version), 
			versioncode = COALESCE(inversioncode, versioncode);

	END IF;
		INSERT INTO appuser_flag(appuser_id)
			VALUES (vAppUserId)
				ON DUPLICATE KEY UPDATE    
			appuser_id = vAppUserId;

SELECT CASE WHEN vAppUserId IS NULL THEN -1 ELSE vAppUserId END appuser_id, 
		a.is_commercial, a.is_admin, l.lat, l.lng, show_me, show_cab_notifications, usertype,	verified, country_code, phoneno,
		receive_notifications, allowstrangernotifications, 
			(SELECT MAX(trip_id) FROM trips  tt 
				WHERE tt.appuser_id = vAppUserId
					AND tt.trip_action != 'E' 
					AND tt.trip_action != 'A'
					AND tt.trip_action is NOT NULL) as driver_trip_id
		FROM vw_appuser_flag a
	LEFT JOIN last_location l 
	ON l.appuser_id = a.appuser_id
	WHERE a.appuser_id = vAppUserId;

END$$
	
DELIMITER;
call user_access(NULL,"91","9920809096","2016-05-07 15:09:55",'tommyvercetti1950@gmail.com',"19.104578800",
"72.850678800","20.0","2016-05-07 15:09:54","network","0.0","0.0","0.0","865645023256965","40420","armani","Xiaomi",
"CEP","0.01.62-alpha","4","SAR","175.100.181.240",
"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36",'4.4.4',19,1,'A');