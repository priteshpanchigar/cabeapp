DROP PROCEDURE IF EXISTS ws_getwalkwithmelocation;
DELIMITER$$
CREATE PROCEDURE `ws_getwalkwithmelocation`(IN `inuser_emilid` varchar(1000),
IN intripid INT, IN `inrandomstring` varchar(100))
quit_proc: BEGIN

DECLARE vCnt INT DEFAULT 0;

SELECT COUNT(*) cnt FROM walkwithmepermissions
	 WHERE randomstring = inrandomstring INTO vCnt;
IF vCnt < 1 THEN
	LEAVE quit_proc;
END IF;
	
	SELECT 'äbc' as  frnkey, loctime as date , lat as lat, 
					lng as lon, accuracy as accuracy, 
					IF (trip_action IN ('A', 'E'), 'Y', 'N') as stopflag
	FROM  trips t
	INNER JOIN last_location l
ON l.appuser_id = t.appuser_id
		WHERE trip_id = intripid;
END$$
DELIMITER;

CALL ws_getwalkwithmelocation('soumenmaity.cse@gmail.com',122,'M1U9ftBgDbNULLEsuCrBg3NW0PyvFk68Kx');