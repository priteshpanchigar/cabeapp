DROP PROCEDURE IF EXISTS ws_getwalkwithme_update_position;
CREATE  PROCEDURE `ws_getwalkwithme_update_position`(IN inuser_emailid varchar(1000),
IN infriend_emailid varchar(1000),IN inimei varchar(25),IN indatetime varchar(30),
IN inlat double,IN inlon double,IN inaccuracy double,IN inflag varchar(1),
IN inrandomno varchar(128),IN inuserlink varchar(1000),IN instopped VARCHAR(1),
IN increationdatetime datetime)
BEGIN
	DECLARE cnt INT DEFAULT 0;
	DECLARE tmpcnt INT DEFAULT 0;
	DECLARE tmpid INT DEFAULT 0;
	
	IF (inflag = 'A') THEN
			INSERT INTO walkwithmepermissions(w_c_user_emailid, randomstring, mode, stopflag, intrip)
			VALUES (inuser_emailid, inrandomno, inflag, instopped, true);

			INSERT INTO walkwithmepermissionsstop(w_s_user_emailid, u_randomstring)
			VALUES (inuser_emailid, inrandomno);

			UPDATE walkwithmepermissions
				SET user_link = inuserlink
      WHERE w_c_user_emailid = inuser_emailid 
      AND randomstring = inrandomno;
			
			SELECT w_c_id FROM walkwithmepermissions ORDER BY w_c_id DESC LIMIT 1 into tmpid;

			call splitStringToRows(infriend_emailid,',');
	
			INSERT INTO walkwithme_contacts
			(w_contact_user_emailid,w_contact_name,w_contact_emailid,w_creationdatetime)
			SELECT inuser_emailid,
			CASE
					WHEN INSTR(split_value,'<') > 0 THEN
						SUBSTR(split_value,1,INSTR(split_value,'<')-1)
					ELSE
						split_value
					END,
			 REPLACE(SUBSTR(split_value, 
					INSTR(split_value,'<') +1, 
					LENGTH(split_value)), '>', ''), increationdatetime FROM splitResults  ;
	
		IF(inlat IS NOT NULL AND inlon IS NOT NULL) THEN
			#INSERT INTO walkwithmelocadd(fk_w_c_id,user_emailid,imei,datetm,lat,lon,accuracy,serverdatetime,mode,intrip)
			#VALUES (tmpid,inuser_emailid,inimei,indatetime,inlat,inlon,inaccuracy,now(),inflag,true);
			
			UPDATE walkwithmelocupd
			SET intrip = FALSE
			WHERE user_emailid = inuser_emailid;
		
			INSERT INTO walkwithmelocupd(fk_w_c_id,user_emailid,imei,datetm,lat,lon,accuracy,
			serverdatetime,intrip)
			VALUES (tmpid,inuser_emailid,inimei,indatetime,inlat,inlon,inaccuracy,now(),true);
		END IF;	
	END IF;
	
  IF(inflag = 'U') THEN
			UPDATE walkwithmepermissions
				SET mode = inflag
      WHERE w_c_user_emailid = inuser_emailid
      AND intrip = true; 

      UPDATE walkwithmepermissions
				SET stopflag = instopped
			WHERE w_c_user_emailid = inuser_emailid
      AND intrip = true;

			UPDATE walkwithmepermissions
				SET user_link = inuserlink
      WHERE w_c_user_emailid = inuser_emailid 
      AND intrip = true;

			UPDATE walkwithmepermissions
			SET intrip = TRUE
			WHERE w_c_user_emailid = inuser_emailid
      AND intrip = true
			AND instopped = 'N';

      UPDATE walkwithmepermissions
			SET intrip = FALSE
			WHERE w_c_user_emailid = inuser_emailid
      AND intrip = true
			AND instopped = 'Y';

			SET @update_id := 0;
			UPDATE walkwithmepermissions SET  w_c_id = (SELECT @update_id := w_c_id)
			WHERE w_c_user_emailid = inuser_emailid 
      AND intrip = true;

			UPDATE walkwithmelocupd
			SET user_emailid = inuser_emailid,
					imei = inimei,
					datetm = indatetime,
					lat = inlat,
					lon = inlon,
					accuracy = inaccuracy,
					serverdatetime = now(),
					mode = inflag,
					intrip = TRUE
			WHERE user_emailid = inuser_emailid 
			AND fk_w_c_id = @update_id;

			/*UPDATE walkwithmelocupd
			SET intrip = TRUE
			WHERE user_emailid = inuser_emailid
			AND instopped = 'N';

			UPDATE walkwithmelocupd
			SET intrip = FALSE
			WHERE user_emailid = inuser_emailid
			AND instopped = 'Y';*/
	 
	END IF;

END;

call ws_getwalkwithme_update_position('soumenmaity.cse@gmail.com','Pritesh Panchigar , ','357965056292603','2016-4-26 11:56:50',"19.104611400","72.850674000",500,'A','VPaysL3Dic357965056292603sLmVjpzPIqv618TYM9oR','http://www.jumpinjumpout.com/alpha/cabeapp/v1/svr/php/passenger/ws/walkwithme_updateposition.php?usermail=soumenmaity.cse@gmail.com&friend_email=Pritesh Panchigar , &mode=A&lat="19.104611400"&lng="72.850674000"&name=soumenmaity.cse&cc=soumenmaity.cse@gmail.com,cab.e@yahoo.com&imei=357965056292603&y=2016&o=4&d=26&h=11&m=56&s=50&accuracy=500','N','2016-4-26 11:56:50');