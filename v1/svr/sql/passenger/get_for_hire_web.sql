DROP PROCEDURE IF EXISTS get_for_hire_web;
CREATE  PROCEDURE `get_for_hire_web`(IN `inlat` double, IN `inlng` double,  IN inclientdatetime datetime)
BEGIN

SET @inLat = inlat,
	@inLng = inlng,
	@inDistance = 5;



	UPDATE booked_driver bd
		JOIN trips t
			ON t.trip_id = bd.trip_id 
		SET trip_timed_out = 1 
			WHERE  TIMESTAMPDIFF(MINUTE, planned_start_datetime, inclientdatetime ) > 60
				AND NOT (trip_action = 'E' OR trip_action = 'A' OR trip_action IS NULL) 
				AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0;
	
	
SELECT a.appuser_id,  TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime)diff,  a.email, a.firstname, a.lastname, a.gender, a.phoneno, a.gcm_registration_id, is_for_hire,
(getDistanceBetweenPoints(@inLat, @inLng, l.lat, l.lng)) distance , 
getDistanceBetweenPoints(@inLat, @inLng, l.lat, l.lng) /20*60*2 eta, vehicleno,
sl.service_description, sl.service_code, sl.metered,  sl.rate_per_km, l.lat, l.lng, l.accuracy, l.clientaccessdatetime
FROM service_lookup sl
LEFT JOIN vwcommercialvehicle cv
ON sl.service_code = cv.service_code
AND is_login = 1
LEFT JOIN appuser_flag af
ON af.appuser_id = cv.appuser_id
AND is_for_hire = 1
LEFT JOIN appuser a
ON a.appuser_id = af.appuser_id
LEFT JOIN last_location l
ON l.appuser_id = a.appuser_id
AND ABS(@inLat - l.lat) < @inDistance / 100 
			AND  ABS(@inLng - l.lng) < @inDistance / 100
			AND TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime) < 3*60
	ORDER BY service_code , getDistanceBetweenPoints(@inLat, @inLng, l.lat, l.lng);


END;

call get_for_hire_web("19.104411","72.850618","2016-08-09 11:34:00");