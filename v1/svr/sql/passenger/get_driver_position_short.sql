DROP PROCEDURE IF EXISTS get_driver_position_short;
DELIMITER $$
CREATE  PROCEDURE `get_driver_position_short`(IN intripid INT, IN insendeverything INT)
BEGIN

DECLARE l DOUBLE;
DECLARE o DOUBLE;
DECLARE ac DOUBLE;
DECLARE ct datetime;
DECLARE vLLSdatetinme datetime ;
DECLARE cLocTime datetime;
DECLARE vAppUserID INT;



SELECT  lat , lng , accuracy, clientaccessdatetime, last_location_sent_datetime, loctime, l.appuser_id  FROM last_location l
INNER JOIN trips t
ON l.appuser_id = t.appuser_id
WHERE trip_id  = intripid
INTO l, o, ac, ct, vLLSdatetinme, cLocTime, vAppUserID;

IF vLLSdatetinme IS NOT NULL AND vLLSdatetinme = cLocTime AND insendeverything = 0 THEN
	SELECT 1 AS sl;
ELSE
	SELECT l, o, ct FROM DUAL;
	UPDATE  last_location
		SET last_location_sent_datetime = cLocTime
		WHERE appuser_id = vAppUserID;
END IF;


END$$
DELIMITER ;

call get_driver_position_short(1, 1);