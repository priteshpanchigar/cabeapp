DROP PROCEDURE IF EXISTS trip_history;
DELIMITER $$
CREATE  PROCEDURE `trip_history`(IN `inappuserid` INT)
BEGIN
SELECT t.trip_id, t.triptype, DATE_FORMAT(bd.booking_time,'%d/%m/%Y %h:%i %p') booking_time, bd.trip_comment, 
bd.trip_rating, bd.trip_cost, bd.trip_distance,
t.fromaddress, t.toaddress,t.fromlat, t.fromlng, 
(SELECT lat FROM location l  WHERE t.last_location_id = l.location_id) as tolat,
(SELECT  lng FROM location l  WHERE t.last_location_id = l.location_id) as tolng,
cw.image_name, cw.image_path, cw.firstname, cw.lastname,
CONCAT(cw.vehicle_company, ' ',cw.vehicle_model) vehicle,  vehicleno    
FROM trips t
INNER JOIN booked_driver bd
ON t.trip_id = bd.trip_id
LEFT JOIN vwcommercialvehicle cw
ON bd.driver_appuser_id = cw.appuser_id
WHERE bd.passenger_appuser_id = inappuserid
AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0 AND bd.trip_timed_out = 0
AND t.trip_action = 'E'

UNION

SELECT t.trip_id, t.triptype, DATE_FORMAT(bd.booking_time,'%d/%m/%Y %h:%i %p') booking_time, ps.trip_comment, 
ps.trip_rating, bd.trip_cost, ps.trip_distance,
t.fromaddress, t.toaddress,t.fromlat, t.fromlng, 
(SELECT lat FROM location l  WHERE t.last_location_id = l.location_id) as tolat,
(SELECT  lng FROM location l  WHERE t.last_location_id = l.location_id) as tolng,
cw.image_name, cw.image_path, cw.firstname, cw.lastname,
CONCAT(cw.vehicle_company, ' ',cw.vehicle_model) vehicle,  vehicleno    
FROM trips t
INNER JOIN booked_driver_shared bd
ON t.trip_id = bd.trip_id
LEFT JOIN vwcommercialvehicle cw
ON bd.driver_appuser_id = cw.appuser_id
LEFT JOIN passenger_shared_trip ps
ON t.trip_id = ps.trip_id
WHERE ps.passenger_appuser_id = inappuserid
AND bd.cancelled_driver = 0 AND ps.cancel_join = 0 AND bd.trip_timed_out = 0
AND t.trip_action = 'E'
ORDER BY trip_id desc;


END$$
DELIMITER ;

CALL trip_history(2)