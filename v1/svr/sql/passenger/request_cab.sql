DROP PROCEDURE IF EXISTS `request_cab`;
DELIMITER $$
CREATE PROCEDURE `request_cab`(
	IN `infromaddress` varchar(300), IN `infromshortaddress` varchar(300), 
	IN `intoaddress` varchar(300), IN `intoshortaddress` varchar(300), 
	IN infromsublocality varchar(200), IN intosublocality varchar(200), 
	IN in_trip_directions_polyline varchar(5000), 
	IN `infromlat` double, IN `infromlng` double, 
	IN `intolat` double, IN `intolng` double, 
	IN `intimems` bigint(20), IN `intripaction` varchar(1), 
	IN `intriptime` int, IN `intripdistance` double, 
	IN 	inappuserid INT,IN `inclientdatetime` datetime, 
	IN `inemailid` varchar(100),  IN `inplannedstartdatetime` datetime,
	IN inservicecode varchar(10))
proc_label:BEGIN
DECLARE driverAppuserid INT;
DECLARE ntripid INT DEFAULT 0;
DECLARE hastripid INT;
DECLARE nFoundDriver INT DEFAULT 0;
SET @inDistance = 5;

SELECT a.appuser_id
	FROM appuser_flag a
		INNER JOIN last_location l ON l.appuser_id =  a.appuser_id
				WHERE is_for_hire = 1 
				AND 	(SELECT service_code FROM vwcommercialvehicle vw 
								WHERE  vw.appuser_id = a.appuser_id) = inservicecode		
				AND ABS(infromlat - l.lat) < @inDistance / 100 
				AND ABS(infromlng - l.lng) < @inDistance / 100
				AND TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime) < 3*60
			ORDER BY getDistanceBetweenPoints(infromlat, infromlng, l.lat, l.lng)
			LIMIT 1 INTO driverAppuserid;

#SELECT driverAppuserid;
IF driverAppuserid IS NULL THEN 
	SELECT 0 driver_found;
	LEAVE proc_label;
END IF;
	UPDATE appuser_flag
		SET is_for_hire = 0
		WHERE appuser_id = driverAppuserid;
		
	INSERT INTO trips(appuser_id, triptype, fromaddress, fromshortaddress, 
		toaddress, toshortaddress, fromsublocality, 
		tosublocality, trip_directions_polyline, fromlat, fromlng, 
		tolat, tolng, trips_unique_ms, tripcreationtime,
		trip_action, trip_action_time , triptime, tripdistance,  planned_start_time, planned_start_datetime)
	VALUES (driverAppuserid, 'A', infromaddress, infromshortaddress, intoaddress, intoshortaddress,
		infromsublocality, intosublocality, in_trip_directions_polyline,
		infromlat, infromlng,
		intolat, intolng, intimems, inclientdatetime,
		'C', inclientdatetime,intriptime, intripdistance, 
		DATE_FORMAT(inplannedstartdatetime,'%H:%i') , 
		inplannedstartdatetime );
	SET ntripid = LAST_INSERT_ID(); 
#SELECT driverAppuserid, inappuserid, ntripid,inclientdatetime;
	INSERT  INTO booked_driver(driver_appuser_id, passenger_appuser_id, trip_id, booking_time)
		VALUES (driverAppuserid, inappuserid, ntripid,inclientdatetime);
	
	
	
	SELECT 1 driver_found, 
IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
	ct.trip_id, bd.driver_appuser_id,  bd.cancelled_driver, 
	vw.image_path, vw.image_name,
	vw.email, vw.country_code, vw.phoneno driver_phoneno, 	vw.firstname, vw.lastname, 
	bd.booking_time, ct.triptype,  ct.fromaddress, 
	ct.toaddress, ct.fromlat, ct.fromlng, ct.tolat, ct.tolng,
	ct.tripcreationtime, ct.planned_start_time, ct.planned_start_datetime, 
	ct.trip_action, ct.trip_action_time, ct.triptime, ct.tripdistance, 
	vehicleno, vw.vehicle_company,
	vehicle_category, vehicle_model, vehicle_type, vehicle_color,   service_code
	FROM trips ct
		INNER JOIN booked_driver bd	
		ON ct.trip_id = bd.trip_id
		INNER  JOIN vwcommercialvehicle vw
		ON vw.appuser_id = bd.driver_appuser_id
	INNER JOIN last_location l
ON l.appuser_id = bd.driver_appuser_id
WHERE ct.trip_id = ntripid 
AND passenger_appuser_id = inappuserid

ORDER BY bd.booked_driver_id DESC LIMIT 1; 

END$$
DELIMITER;

call request_cab('5, Hanuman Hedit Marg No 2 Om Shri Siddhivinayak Society, Paranjape Nagar, Vile Parle ',
NULL,NULL,NULL,'Paranjape Nagar',NULL,NULL,19.104411,72.850618,NULL,NULL,1467005655000,NULL,NULL,NULL,
16,"2016-06-27 11:09:40",'tommyvercetti1950@gmail.com','2016-07-07 16:52:40','CC');