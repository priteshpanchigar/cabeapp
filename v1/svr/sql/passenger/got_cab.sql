DROP PROCEDURE IF EXISTS got_cab;
DELIMITER $$
CREATE  PROCEDURE `got_cab`(IN inappuserid INT)
BEGIN
	
SELECT ct.trip_id, bd.driver_appuser_id,  bd.cancelled_driver, 
	vw.image_path, vw.image_name,
	vw.email, vw.country_code, vw.phoneno driver_phoneno, 	vw.firstname, vw.lastname, 
	bd.booking_time, ct.triptype,  ct.fromaddress, 
	ct.toaddress, ct.fromlat, ct.fromlng, ct.tolat, ct.tolng,
	ct.tripcreationtime, ct.planned_start_time, ct.planned_start_datetime, 
	ct.trip_action, ct.trip_action_time, ct.triptime, ct.tripdistance, 
	vehicleno, vw.vehicle_company,
	vehicle_category, vehicle_model, vehicle_type, vehicle_color,   service_code
	FROM trips ct
		INNER JOIN booked_driver bd	
		ON ct.trip_id = bd.trip_id
		INNER JOIN vw_appuser_flag ap 
		ON ap.appuser_id = bd.driver_appuser_id
		INNER  JOIN vwcommercialvehicle vw
		ON vw.appuser_id = bd.driver_appuser_id		
	WHERE  passenger_appuser_id = inappuserid
		AND cancelled_driver = 0 
		AND cancelled_passenger = 0 
		AND trip_timed_out = 0 
		AND something_went_wrong = 0
		AND trip_action IN ('B', 'C')
	ORDER BY bd.trip_id DESC LIMIT 1; 

END$$
DELIMITER; 
call got_cab(1);