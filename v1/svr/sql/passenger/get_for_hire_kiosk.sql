DROP PROCEDURE IF EXISTS get_for_hire_kiosk;
CREATE  PROCEDURE `get_for_hire_kiosk`(IN inimei VARCHAR(30), IN inclientdatetime datetime)
BEGIN

DECLARE vLat DOUBLE DEFAULT NULL;
DECLARE vLng DOUBLE DEFAULT NULL;
SET @inDistance = 5;

SELECT lat, lng  FROM kiosk
	WHERE imei = inimei INTO vLat, vLng;

#SELECT vLat, vLng;
	UPDATE booked_driver bd
		JOIN trips t
			ON t.trip_id = bd.trip_id 
		SET trip_timed_out = 1 
			WHERE  TIMESTAMPDIFF(MINUTE, planned_start_datetime, inclientdatetime ) > 60
				AND NOT (trip_action = 'E' OR trip_action = 'A' OR trip_action IS NULL) 
				AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0;
	
	
SELECT a.appuser_id, af.is_for_hire, TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime)diff,  a.email, 
a.firstname, a.lastname, a.gender, a.phoneno, 
(getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng)) distance , 
getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng) /20*60*2 eta, vehicleno,
sl.service_description, sl.service_code, sl.metered,  sl.rate_per_km, l.lat, l.lng, l.accuracy, 
l.clientaccessdatetime
FROM service_lookup sl
LEFT JOIN vwcommercialvehicle cv
ON sl.service_code = cv.service_code
AND is_login = 1
LEFT JOIN appuser_flag af
ON af.appuser_id = cv.appuser_id
AND is_for_hire = 1
LEFT JOIN appuser a
ON a.appuser_id = af.appuser_id
LEFT JOIN last_location l
ON l.appuser_id = a.appuser_id
AND ABS(vLat - l.lat) < @inDistance / 100 
			AND  ABS(vLng - l.lng) < @inDistance / 100
			AND TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime) < 3*60
ORDER BY service_code , is_for_hire DESC, eta, getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng);

END;

call get_for_hire_kiosk("542de3d712bccbb9","2016-07-15 14:32:00");