DROP PROCEDURE IF EXISTS get_for_hire_cab;
CREATE  PROCEDURE `get_for_hire_cab`(IN `inlat` double, IN `inlng` double,IN `intolat` double, IN `intolng` double,
IN inservicecode VARCHAR(20), IN inclientdatetime datetime)
BEGIN
SET @inLat = inlat,
	@inLng = inlng,
	@inToLat =  intolat,
	@inToLng =intolng,
	@inDistance = 3;



UPDATE booked_driver_shared bd
		JOIN trips t
			ON t.trip_id = bd.trip_id 
		SET trip_timed_out = 1 
			WHERE  TIMESTAMPDIFF(MINUTE, planned_start_datetime, inclientdatetime ) > 60
				AND NOT (trip_action = 'E' OR trip_action = 'A' OR trip_action IS NULL) 
				AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0;
	

	UPDATE booked_driver bd
		JOIN trips t
			ON t.trip_id = bd.trip_id 
		SET trip_timed_out = 1 
			WHERE  TIMESTAMPDIFF(MINUTE, planned_start_datetime, inclientdatetime ) > 60
				AND NOT (trip_action = 'E' OR trip_action = 'A' OR trip_action IS NULL) 
				AND bd.cancelled_driver = 0 AND bd.cancelled_passenger = 0;
	

	SELECT a.appuser_id, a.email, firstname, lastname, phoneno, 
(getDistanceBetweenPoints(@inLat, @inLng, l.lat, l.lng)) distance , 
getDistanceBetweenPoints(@inLat, @inLng, l.lat, l.lng) /20*60*2 eta,
	(SELECT vehicleno FROM vwcommercialvehicle vw
		INNER JOIN driver_shift ds
		ON vw.driver_vehicle_id = ds.driver_vehicle_id
		WHERE ds.is_login = 1 AND ds.appuser_id = a.appuser_id
		ORDER BY ds.shift_id DESC LIMIT 1)vehicleno,
	(SELECT service_code FROM vwcommercialvehicle WHERE a.appuser_id =appuser_id) service_code,
 		l.lat, l.lng, l.clientaccessdatetime
	FROM vw_appuser_flag a
	INNER JOIN last_location l ON l.appuser_id = a.appuser_id
			WHERE (is_for_hire = 1 OR is_for_hire = 3)
			AND ABS(@inLat - l.lat) < @inDistance / 100 
			AND  ABS(@inLng - l.lng) < @inDistance / 100
			AND TIMESTAMPDIFF(SECOND,l.clientaccessdatetime, inclientdatetime) < 3*60
	ORDER BY service_code , getDistanceBetweenPoints(@inLat, @inLng, l.lat, l.lng);


END;


CALL get_for_hire_cab("19.104411","72.850618",NULL,NULL,'ST',"2016-09-27 16:31:46");