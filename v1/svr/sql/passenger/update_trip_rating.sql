DROP PROCEDURE IF EXISTS `update_trip_rating`;
DELIMITER $$
CREATE  PROCEDURE `update_trip_rating`(IN intripid INT, IN inappuserid INT, IN intriptype VARCHAR(5), 
IN intriprating DOUBLE, IN intripcomment varchar(300))
BEGIN


IF intriptype = 'P' THEN

UPDATE passenger_shared_trip
	SET trip_rating = COALESCE(intriprating, trip_rating), 
			trip_comment = COALESCE(intripcomment, trip_comment)
		WHERE trip_id = intripid
		AND  passenger_appuser_id = inappuserid;

SELECT trip_id, trip_rating, trip_comment FROM passenger_shared_trip
	WHERE trip_id = intripid
	AND  passenger_appuser_id = inappuserid;

ELSE

UPDATE booked_driver
	SET trip_rating = COALESCE(intriprating, trip_rating), 
			trip_comment = COALESCE(intripcomment, trip_comment)
		WHERE trip_id = intripid 
		AND  passenger_appuser_id = inappuserid;

SELECT trip_id, trip_rating, trip_comment FROM booked_driver
	WHERE trip_id = intripid
	AND  passenger_appuser_id = inappuserid;

END IF;



END$$
DELIMITER ;

CALL update_trip_rating( 4,11 ,'P',3.5, 'Very Good');