DROP PROCEDURE IF EXISTS get_trip_status;
DELIMITER $$
CREATE  PROCEDURE `get_trip_status`(IN intriptype varchar(10), IN intripid INT, IN `inappuserid` INT, 
	IN `inclientdatetime` datetime, IN `inlatno` double, IN `inlngno` double, 
	IN `inacc` int, IN `inlocationdatetime` datetime, IN `inprovider` varchar(10),
	IN `inspeed` float, IN `inbearing` float, IN `inaltitude` double)
BEGIN
DECLARE v_location_id INT DEFAULT NULL;
DECLARE vCount INT DEFAULT 0;
DECLARE vLat DOUBLE DEFAULT NULL;
DECLARE vLng DOUBLE DEFAULT NULL;

	
SELECT COUNT(*), fromlat , fromlng FROM trips
	WHERE trip_id = intripid  INTO vCount, vLat, vLng;	


IF intriptype = 'P' THEN	

IF vCount > 0 THEN	
	SELECT 
	IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
	t.triptype, t.something_went_wrong, ps.has_joined,  ps.has_jumped_in,  ps.has_jumped_out, 
	#(getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng)) distance , 
	#getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng) /20*60 eta,
 t.trip_action, t.triptime, 
l.lat, l.lng, l.accuracy, 
		CASE WHEN l.clientaccessdatetime > a.clientlastaccessdatetime THEN
			 l.clientaccessdatetime ELSE a.clientlastaccessdatetime 
		END clientaccessdatetime,
(SELECT metered FROM vwcommercialvehicle vw WHERE vw.appuser_id = bd.driver_appuser_id) metered
	FROM trips t
	INNER JOIN last_location l 
		ON l.appuser_id =  t.appuser_id 
	INNER JOIN  booked_driver_shared bd 
		ON	t.trip_id = bd.trip_id 
	INNER JOIN vw_appuser_flag a 
		ON a.appuser_id = t.appuser_id
	INNER JOIN passenger_shared_trip ps
		ON ps.trip_id = t.trip_id								
	WHERE t.trip_id = intripid
		AND ps.passenger_appuser_id = inappuserid;
ELSE
	SELECT 
	IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
	t.triptype,  t.something_went_wrong, ps.has_joined,  ps.has_jumped_in,  ps.has_jumped_out, 
	#(getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng)) distance , 
	#getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng) /20*60 eta,
	t.trip_action, t.triptime,
	l.lat, l.lng, l.accuracy, 
				CASE WHEN l.clientaccessdatetime > a.clientlastaccessdatetime THEN
			 l.clientaccessdatetime ELSE a.clientlastaccessdatetime 
		END clientaccessdatetime,
(SELECT metered FROM vwcommercialvehicle vw WHERE vw.appuser_id = bd.driver_appuser_id) metered	  
	FROM trips t
	INNER JOIN last_location l 
		ON l.appuser_id =  t.appuser_id 
	INNER JOIN  booked_driver_shared bd  
		ON	t.trip_id = bd.trip_id 
	INNER JOIN vw_appuser_flag a 
		ON a.appuser_id = t.appuser_id
	INNER JOIN passenger_shared_trip ps
		ON ps.trip_id = t.trip_id								
	WHERE t.trip_id = intripid
AND ps.passenger_appuser_id = inappuserid;
END IF;	

ELSE


	
IF vCount > 0 THEN	
	SELECT 
	IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
	t.triptype, t.something_went_wrong,
	#(getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng)) distance , 
	#getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng) /20*60 eta,
 t.trip_action, t.triptime, 
l.lat, l.lng, l.accuracy, 
		CASE WHEN l.clientaccessdatetime > a.clientlastaccessdatetime THEN
			 l.clientaccessdatetime ELSE a.clientlastaccessdatetime 
		END clientaccessdatetime,
(SELECT metered FROM vwcommercialvehicle vw WHERE vw.appuser_id = bd.driver_appuser_id) metered
	FROM trips t
	INNER JOIN last_location l 
		ON l.appuser_id =  t.appuser_id 
	INNER JOIN  booked_driver bd 
	ON	t.trip_id = bd.trip_id 
	INNER JOIN vw_appuser_flag a ON a.appuser_id = t.appuser_id								
	WHERE t.trip_id = intripid;
ELSE
	SELECT 
	IF(TIMESTAMPDIFF(SECOND,l.clientaccessdatetime,  inclientdatetime) < 45, 1, 0) driver_connected,
	t.triptype,  t.something_went_wrong,
	#(getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng)) distance , 
	#getDistanceBetweenPoints(vLat, vLng, l.lat, l.lng) /20*60 eta,
	t.trip_action, t.triptime,
	l.lat, l.lng, l.accuracy, 
				CASE WHEN l.clientaccessdatetime > a.clientlastaccessdatetime THEN
			 l.clientaccessdatetime ELSE a.clientlastaccessdatetime 
		END clientaccessdatetime,
(SELECT metered FROM vwcommercialvehicle vw WHERE vw.appuser_id = bd.driver_appuser_id) metered	  
	FROM trips t
	INNER JOIN last_location l 
		ON l.appuser_id =  t.appuser_id 
	INNER JOIN  booked_driver bd  
	ON	t.trip_id = bd.trip_id 
	INNER JOIN vw_appuser_flag a ON a.appuser_id = t.appuser_id								
	WHERE t.trip_id = intripid;
END IF;

END IF;

SET v_location_id = insert_location(inappuserid, inclientdatetime, 
						inlatno, inlngno, inacc, inlocationdatetime,
						inprovider, inspeed, inbearing, inaltitude);

END$$
DELIMITER ;

call get_trip_status('P',6,11,"2016-10-21 13:08:00","19.104643700","72.850671000","50.0","1970-01-01 05:30:00","network","0.0","0.0","0.0")