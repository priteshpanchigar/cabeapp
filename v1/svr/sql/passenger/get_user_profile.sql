DROP PROCEDURE IF EXISTS get_user_profile;
DELIMITER $$
CREATE  PROCEDURE `get_user_profile`(IN `inappuserid` INT)
BEGIN

SELECT  phoneno , email , firstname , lastname , gender , dob, userprofileimagefilename 
FROM vw_passenger_info
	WHERE  appuser_id = inappuserid;

END$$
DELIMITER ;

CALL get_user_profile(1);