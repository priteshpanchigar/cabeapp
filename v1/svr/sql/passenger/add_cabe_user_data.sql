DROP PROCEDURE IF EXISTS add_cabe_user_data;
DELIMITER $$
CREATE  PROCEDURE `add_cabe_user_data`(IN inappuserid INT, IN infirstname VARCHAR(30), 
IN inlastname VARCHAR(30), IN ingender VARCHAR(20), 
IN inuserprofileimagefilename VARCHAR(1000),  IN indob date,
IN inhome VARCHAR(500),IN inwork VARCHAR(500),
IN inhomelat double, IN inhomelng double, IN inworklat double, IN inworklng double)
BEGIN

UPDATE appuser
	SET firstname =  COALESCE(proper(infirstname), firstname), 
		lastname = COALESCE(proper(inlastname), lastname), 
		gender = COALESCE(proper(ingender), gender) ,
		userprofileimagefilename = COALESCE(inuserprofileimagefilename,userprofileimagefilename),
		dob = COALESCE(indob,dob),
		home = COALESCE(inhome,home),
		work = COALESCE(inwork,work),
		home_lat = COALESCE(inhomelat, home_lat),
		home_lng = COALESCE(inhomelng, home_lng),
		work_lat = COALESCE(inworklat, work_lat),
		work_lng = COALESCE(inworklng, work_lng)
	WHERE appuser_id = inappuserid;	

	
SELECT firstname, lastname, gender, userprofileimagefilename, dob FROM appuser
	WHERE appuser_id = inappuserid;
END$$
DELIMITER;

call add_cabe_user_data(2,'John','Davis','MALE',NULL,'1970-02-02', NULL,NULL,NULL,NULL,NULL,NULL);