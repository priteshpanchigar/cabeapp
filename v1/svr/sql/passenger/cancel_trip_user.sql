DROP PROCEDURE IF EXISTS `cancel_trip_user`;
DELIMITER $$
CREATE  PROCEDURE `cancel_trip_user`(IN intriptype VARCHAR (10), IN inpassengerappuserid INT(5), IN intripid INT(5),
IN inclientdatetime datetime,  IN incancelreasoncode VARCHAR (10))
BEGIN

IF intriptype = 'P' THEN
	UPDATE passenger_shared_trip
		SET cancel_join = 1, 
			cancel_join_datetime = inclientdatetime,
			cancel_reason_code = incancelreasoncode
		WHERE trip_id = intripid
		AND passenger_appuser_id = inpassengerappuserid;
		
	SELECT  cancel_join, cancel_join_datetime, cancel_reason_code
			FROM passenger_shared_trip
			WHERE trip_id = intripid;


ELSE
	UPDATE booked_driver
		SET cancelled_passenger = 1, 
			cancelled_passenger_datetime = inclientdatetime,
			cancel_reason_code = incancelreasoncode
		WHERE trip_id = intripid
		AND passenger_appuser_id = inpassengerappuserid;
		
		UPDATE trips
		SET trip_action = "A",
			trip_action_time = inclientdatetime,
			trip_abort_time = inclientdatetime
		WHERE trip_id = intripid;

#it was 0 first 	
	UPDATE appuser_flag
		SET is_for_hire = 1
		WHERE appuser_id = (SELECT driver_appuser_id FROM booked_driver 
									WHERE trip_id = intripid);
		
		SELECT cancelled_passenger, cancelled_passenger_datetime
			FROM booked_driver
			WHERE trip_id = intripid;
		
END IF;

	
END$$
DELIMITER ;
CALL cancel_trip_user('P',1,38,"2016-07-06 12:54:52", "BAC");
