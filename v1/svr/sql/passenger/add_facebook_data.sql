DROP PROCEDURE IF EXISTS add_facebook_data;
DELIMITER $$
CREATE  PROCEDURE `add_facebook_data`(IN inappuserid INT, IN infacebookemail VARCHAR(30),
IN infirstname VARCHAR(30), IN inlastname VARCHAR(30), IN ingender VARCHAR(8), 
IN inuserprofileimagefilename VARCHAR(1000),  IN indob date)
BEGIN

INSERT INTO facebook_profile(appuser_id, facebook_email, firstname, lastname, 
	gender, userprofileimagefilename, dob)
VALUES (inappuserid, infacebookemail, proper(infirstname), proper(inlastname) , 
	proper(ingender), inuserprofileimagefilename, indob)
	ON DUPLICATE KEY UPDATE
	firstname = COALESCE(proper(infirstname),firstname),
	lastname = COALESCE(proper(inlastname),lastname),
	gender = COALESCE(proper(ingender),gender),
	dob = COALESCE(indob,dob),
	userprofileimagefilename =	 COALESCE(inuserprofileimagefilename,userprofileimagefilename);
	
SELECT facebook_id FROM facebook_profile
	where appuser_id = inappuserid;
	
END$$
DELIMITER;

call add_facebook_data(5,'swaprane08@gmail.com','Swapnil','Rane','male',NULL,NULL);