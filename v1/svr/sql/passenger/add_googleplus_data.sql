DROP PROCEDURE IF EXISTS add_googleplus_data;
DELIMITER $$
CREATE  PROCEDURE `add_googleplus_data`(IN inappuserid INT, IN inemail VARCHAR(30),
IN infirstname VARCHAR(30), IN inlastname VARCHAR(30), IN ingender VARCHAR(8), 
IN inuserprofileimagefilename VARCHAR(1000), IN indob date)
BEGIN

INSERT INTO googleplus_profile(appuser_id, email, firstname, lastname, 
	gender, userprofileimagefilename, dob)
VALUES (inappuserid, inemail, infirstname , inlastname , 
	ingender, inuserprofileimagefilename, indob)
	ON DUPLICATE KEY UPDATE
	firstname = COALESCE(infirstname,firstname),
	lastname = COALESCE(inlastname,lastname),
	gender = COALESCE(ingender,gender),
	dob = COALESCE(indob,dob),
	userprofileimagefilename =	 COALESCE(inuserprofileimagefilename,userprofileimagefilename);
	
SELECT googleplus_id FROM googleplus_profile
	WHERE appuser_id = inappuserid;
	
END$$
DELIMITER;

call add_googleplus_data(8,'soumenmaity.cse@gmail.com','Soumen','Maity','MALE',NULL, NULL);