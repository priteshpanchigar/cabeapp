DROP PROCEDURE IF EXISTS get_cars_device_location;
DELIMITER $$
CREATE  PROCEDURE get_cars_device_location()
BEGIN

SELECT 1 has_device,   dv.driver_vehicle_id, service_code , CONCAT(vehicleno_state,' ' ,vehicleno_city,' ' ,vehicleno_code,' ', vehicleno) vehicleno,
	(SELECT is_login FROM driver_shift ds 
		WHERE ds.driver_vehicle_id = dv.driver_vehicle_id 
		ORDER BY shift_id DESC LIMIT 1) is_login, 
device_imei, vehicle_company, vehicle_model, loctime, lat, lng , speed  FROM driver_vehicle dv
	INNER JOIN device_reading dr
	ON dv.reading_id = dr.reading_id
UNION
SELECT 0 has_device, driver_vehicle_id, service_code ,  vehicleno, is_login,
device_imei, vehicle_company, vehicle_model, loctime, lat, lng, speed   FROM vwcommercialvehicle vw
INNER JOIN last_location ll
ON ll.appuser_id = vw.appuser_id
WHERE device_imei IS NULL;

END$$
DELIMITER;
CALL get_cars_device_location();

