DROP PROCEDURE IF EXISTS get_journey;
DELIMITER $$
CREATE  PROCEDURE `get_journey`(IN `intripid` INT, IN intriptype VARCHAR (5), IN inappuserid INT)
BEGIN

	DECLARE v_appuser_id INT DEFAULT 0;
	DECLARE vJumpinLocationTime datetime;
	DECLARE vJumpoutLocationTime datetime;
	DECLARE vStartLocationTime datetime;
	DECLARE vLastLocationTime datetime;


IF intriptype = 'P' THEN

	SELECT  appuser_id,  
			(SELECT clientaccessdatetime from location l where clientaccessdatetime >= jumpin_datetime
			AND	l.appuser_id = t.appuser_id ORDER BY clientaccessdatetime LIMIT 1) slt,
			(SELECT clientaccessdatetime from location l where clientaccessdatetime <= jumpout_datetime
			AND	l.appuser_id = t.appuser_id ORDER BY clientaccessdatetime DESC LIMIT 1) llt
		FROM trips t
		INNER JOIN passenger_shared_trip ps
			ON ps.trip_id = t.trip_id
		WHERE ps.trip_id = intripid
			AND ps.passenger_appuser_id = inappuserid
		INTO v_appuser_id,  vJumpinLocationTime, vJumpoutLocationTime;
	

	
#SELECT v_appuser_id,  vJumpinLocationTime, vJumpoutLocationTime;

	SELECT location_id, lat, lng, accuracy, altitude, bearing, speed, provider, loctime, clientaccessdatetime	
	FROM location
	WHERE appuser_id = v_appuser_id
	AND clientaccessdatetime BETWEEN vJumpinLocationTime AND vJumpoutLocationTime
	#AND accuracy <30
	#GROUP BY clientaccessdatetime
	ORDER BY clientaccessdatetime;

ELSE 

	
	SELECT appuser_id, 
		(SELECT clientaccessdatetime from location l where clientaccessdatetime >= trip_start_time
			AND	l.appuser_id = t.appuser_id ORDER BY clientaccessdatetime LIMIT 1) slt,
		(SELECT clientaccessdatetime from location l where clientaccessdatetime <= trip_end_time
			AND		l.appuser_id = t.appuser_id ORDER BY clientaccessdatetime DESC LIMIT 1 ) llt
	FROM trips t
	WHERE trip_id = intripid
	INTO v_appuser_id,  vStartLocationTime, vLastLocationTime;
	
#SELECT v_appuser_id, vStartLocationTime, vLastLocationTime;



	SELECT location_id, lat, lng, accuracy, altitude, bearing, speed, provider, loctime, clientaccessdatetime
	FROM location
	WHERE  appuser_id = v_appuser_id
	AND clientaccessdatetime BETWEEN vStartLocationTime AND vLastLocationTime
	#AND	speed > 1 
	#AND provider = 'gps'
	#AND (accuracy <30 OR (accuracy IS NULL AND provider = 'D'))
	#AND accuracy IS NULL AND provider = 'D'
	#AND accuracy <30
	#GROUP BY clientaccessdatetime
	ORDER BY clientaccessdatetime;
#	LIMIT 10; 
 #(limit is there because of google query limit)
	END IF;
END$$
DELIMITER ;
-- Sample query
CALL get_journey(61,'P',5);