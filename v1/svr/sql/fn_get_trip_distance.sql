DROP FUNCTION IF EXISTS fn_get_trip_distance;
CREATE  FUNCTION `fn_get_trip_distance`(intripid INT, intriptype VARCHAR(5)) RETURNS DOUBLE
BEGIN
	DECLARE v_appuser_id INT DEFAULT 0;
	DECLARE vDistance DOUBLE ;
	DECLARE vStartLocationTime datetime;
	DECLARE vLastLocationTime datetime;

 IF intriptype = 'A' THEN


	SELECT appuser_id, 
		(SELECT clientaccessdatetime from location l where clientaccessdatetime >= trip_start_time
			AND	l.appuser_id = t.appuser_id ORDER BY clientaccessdatetime LIMIT 1) slt,
		(SELECT clientaccessdatetime from location l where clientaccessdatetime <= trip_end_time
			AND		l.appuser_id = t.appuser_id ORDER BY clientaccessdatetime DESC LIMIT 1 ) llt
	FROM trips t
	WHERE trip_id = intripid
	INTO v_appuser_id,  vStartLocationTime, vLastLocationTime;
	
#SELECT v_appuser_id, vStartLocationTime, vLastLocationTime;

DROP TEMPORARY TABLE IF EXISTS temp_dist;
CREATE TEMPORARY TABLE temp_dist AS	
	SELECT location_id, lat, lng, accuracy, clientaccessdatetime
	FROM location
	WHERE  appuser_id = v_appuser_id
	AND clientaccessdatetime BETWEEN vStartLocationTime AND vLastLocationTime
	AND accuracy <30
	GROUP BY clientaccessdatetime
	ORDER BY clientaccessdatetime;



	DROP TEMPORARY TABLE IF EXISTS temp_dist_2;
	CREATE TEMPORARY TABLE temp_dist_2 AS
	SELECT location_id, lat, lng, accuracy, clientaccessdatetime FROM temp_dist;


	DROP TEMPORARY TABLE IF EXISTS temp_dist_3;
	CREATE TEMPORARY TABLE  temp_dist_3 AS
	SELECT location_id, lat, lng, accuracy, clientaccessdatetime FROM temp_dist;




SELECT ROUND(SUM(GeoDistKM(A.lat, A.lng, b.lat, b.lng)),2) distance
	FROM temp_dist A 
CROSS JOIN temp_dist_2 B
WHERE B.location_id IN (SELECT MIN(C.location_id) FROM temp_dist_3 C WHERE C.location_id > A.location_id) 
	ORDER BY A.location_id ASC 
INTO vDistance;

END IF;


RETURN vDistance;
END;



SELECT fn_get_trip_distance (55, 'A');