
DROP PROCEDURE IF EXISTS set_appuser_flag;
DELIMITER $$
CREATE  PROCEDURE `set_appuser_flag`(IN `inflagname` varchar(300), IN `inflagvalue` INT, IN `inappuserid` INT)
BEGIN

SET @sql = CONCAT('UPDATE appuser_flag SET ', inflagname, '=', inflagvalue, 
	' WHERE appuser_id =', inappuserid);
#SELECT @sql;

PREPARE stmt FROM @sql;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;


END$$
DELIMITER ;

call set_appuser_flag('is_commercial', 1, 1)