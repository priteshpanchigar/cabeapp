DROP PROCEDURE IF EXISTS jtrack_company_registration;
DELIMITER $$
CREATE PROCEDURE `jtrack_company_registration`() 
BEGIN 

SELECT company_id, company_name, company_address, company_code, c_contact_person, c_mobile, c_email
	FROM company;
 
END$$ 
DELIMITER ;

CALL jtrack_company_registration();

