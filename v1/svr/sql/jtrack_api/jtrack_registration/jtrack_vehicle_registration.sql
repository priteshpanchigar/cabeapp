
DROP PROCEDURE IF EXISTS jtrack_vehicle_registration;
DELIMITER $$
CREATE PROCEDURE `jtrack_vehicle_registration`() 
BEGIN 

SELECT  jtrack_company_id, jtrack_franchise_id, jtrack_vno, 
	CONCAT(UPPER(vehicleno_state),' ' ,vehicleno_city,' ', UPPER(vehicleno_code),' ', vehicleno) vehicleno,
	gps_device_no, CONCAT(proper(vehicle_company),' ', proper(vehicle_model)) vehicle_model_name, 
	(SELECT phoneno FROM appuser a WHERE a.appuser_id = dv.owner_appuser_id) phoneno,
	jtrack_driver_id, vehicle_color, service_code, is_booked, c.company_id, f.franchise_id, d.driver_id, 
	owner_appuser_id, unique_id FROM driver_vehicle dv
	INNER JOIN company c 
ON c.company_id = dv.company_id
	INNER JOIN franchise f
ON dv.franchise_id = f.franchise_id
	INNER JOIN driver d
ON d.appuser_id = dv.owner_appuser_id;

 
END$$ 
DELIMITER ;

CALL jtrack_vehicle_registration();