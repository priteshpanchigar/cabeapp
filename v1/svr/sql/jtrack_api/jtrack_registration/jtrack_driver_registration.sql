
DROP PROCEDURE IF EXISTS jtrack_driver_registration;
DELIMITER $$
CREATE PROCEDURE `jtrack_driver_registration`() 
BEGIN 

SELECT driver_id, d.appuser_id, CONCAT(a.firstname,' ', a.lastname) driver_name, a.gender,  
	clientdatetime, d.country_code, d.phoneno, 
	d.email, d.company_id, f.franchise_id, 
	d.gcm_registration_driver_id, c.jtrack_company_id, jtrack_franchise_id, 
	franchise_name, franchise_code FROM driver d
	INNER JOIN franchise f
		ON d.franchise_id = f.franchise_id
	INNER JOIN company c
		ON c.company_id = f.company_id
	INNER JOIN appuser  a
		ON a.appuser_id = d.appuser_id;
 
END$$ 
DELIMITER ;

CALL jtrack_driver_registration();