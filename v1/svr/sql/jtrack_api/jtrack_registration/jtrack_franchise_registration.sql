DROP PROCEDURE IF EXISTS jtrack_franchise_registration;
DELIMITER $$
CREATE PROCEDURE `jtrack_franchise_registration`() 
BEGIN 

SELECT franchise_id, c.company_id, c.jtrack_company_id, franchise_name, franchise_code
	FROM franchise f
INNER JOIN company c
ON f.company_id = c.company_id;
 
END$$ 
DELIMITER ;

CALL jtrack_franchise_registration();
