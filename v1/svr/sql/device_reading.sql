DROP PROCEDURE IF EXISTS device_reading;
DELIMITER $$
CREATE  PROCEDURE `device_reading`(IN intype VARCHAR(9), IN inimei BIGINT(15), IN inpacket_length INT, IN inpacket_type INT, 
IN inclienttime varchar(6), IN inclientdate varchar(6), IN inloctime varchar(6),IN inlocdate varchar(6),  
IN ingps_data_valid INT, IN inlat double, IN inlatitude_direction varchar(1), IN inlng double, IN inlongitude_direction varchar(1), 
IN inspeed INT, IN inheading_angle double, 
IN intotal_satellites INT, IN ingsm_signal_strength INT, IN inpanic_status INT, IN invehicle_movement_status varchar(1), 
IN ingps_quality varchar(1), IN inpower_status INT,  IN inodometer_reading_pulse double, IN inodometer_reading_gps double,
IN inmetering_status varchar(1), IN intrip_id INT, IN inrunning_distance double, IN inGNX_ver varchar (8),IN incrc varchar (3))
quit_proc: BEGIN

DECLARE vAppUserID INT;
DECLARE v_location_id INT DEFAULT 0;
DECLARE vReadingID INT;


INSERT INTO device_reading(type, imei, packet_length, packet_type, clientdatetime, loctime, gps_data_valid, lat, latitude_direction, lng,
longitude_direction, speed, heading_angle, total_satellites, gsm_signal_strength, panic_status, vehicle_movement_status,
gps_quality, power_status, odometer_reading_pulse, odometer_reading_gps, metering_status, trip_id, running_distance, GNX_ver, crc)

VALUES(intype, inimei, inpacket_length, inpacket_type, STR_TO_DATE(CONCAT(inclientdate,inclienttime),"%d%c%y%H%i%s"), 
STR_TO_DATE(CONCAT(inlocdate,inloctime),"%d%c%y%H%i%s"), ingps_data_valid, inlat, inlatitude_direction, inlng, 
inlongitude_direction, inspeed, inheading_angle, intotal_satellites, 
ingsm_signal_strength, inpanic_status, invehicle_movement_status, 
ingps_quality, inpower_status, inodometer_reading_pulse, inodometer_reading_gps,
inmetering_status, intrip_id, inrunning_distance, inGNX_ver, incrc);

SELECT LAST_INSERT_ID() INTO vReadingID;


SELECT vw.appuser_id  FROM  vwcommercialvehicle vw
WHERE vw.device_imei = inimei
INTO vAppUserID;

#SELECT vAppUserID;
	IF vAppUserID IS NULL THEN		
		LEAVE quit_proc;
	END IF;

SET v_location_id = insert_location(vAppUserId, STR_TO_DATE(CONCAT(inclientdate,inclienttime),"%d%c%y%H%i%s"), 
									inlat, inlng, NULL, STR_TO_DATE(CONCAT(inlocdate,inloctime),"%d%c%y%H%i%s"),
									'D', inspeed, NULL, NULL);
	

UPDATE driver_vehicle a
	SET reading_id = vReadingID
		WHERE  device_imei = inimei;

#SELECT LAST_INSERT_ID() AS reading_id;


END$$
DELIMITER;
CALL  device_reading('$GNX_MLOC',868324022641257,155,1,'143832','220916','202103','210916',
1,19.087726,'N',072.902495,'E',023,1.04,14,99,0,'B','D',2,022300.0,000949.8,'I',0000,0011.80,
'GNX21002','38*')