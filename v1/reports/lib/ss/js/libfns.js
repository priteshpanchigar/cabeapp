// lib functions - can be used in other sites 
// not specific to this site
/*global $, jQuery, location, document, setTimeout, console, google, escape, onDeviceReady, window, handleNoGeoLocation, unescape, navigator */
/*global localStorage*/
"use strict";
var app = false;
var windowsApp = false;
var phpScriptsPath;
var trainAppVer;
var busAppVer;
var delhiMetro,
	LOCATION_TIMEOUT = 3000;
function removejscssfile(filename, filetype) {
	var targetelement = (filetype === "js") ? "script" : (filetype === "css") ? "link" : "none", //determine element type to create nodelist from
		targetattr = (filetype === "js") ? "src" : (filetype === "css") ? "href" : "none", //determine corresponding attribute to test for
		allsuspects = document.getElementsByTagName(targetelement),
		i;
	for (i = allsuspects.length; i >= 0; i = i - 1) { //search backwards within nodelist for matching elements to remove
		if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) !== null && allsuspects[i].getAttribute(targetattr).indexOf(filename) !== -1) {
			allsuspects[i].parentNode.removeChild(allsuspects[i]); //remove element by calling parentNode.removeChild()
		}
	}
}

// Load js or css file dynamicallly
function loadjscssfile(filename, filetype) {
	var fileref = null;
	if (filetype === "js") { //if filename is a external JavaScript file
		fileref = document.createElement('script');
		fileref.setAttribute("type", "text/javascript");
		fileref.setAttribute("src", filename);
	} else if (filetype === "css") { //if filename is an external CSS file
		fileref = document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", filename);
	}
	if (fileref !== "undefined") {
		document.getElementsByTagName("head")[0].appendChild(fileref);
	}
}

// Split the url and get the params

function gup(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)",
		regex = new RegExp(regexS),
		results = regex.exec(window.location.href);
	if (results) {
		return results[1].replace(/%20/g, ' ');
	}
	return null;
}

function handleNoGeoLocation(errorFlag) {
	var initialLocation;
    if (errorFlag === true) {
//     alert("Geolocation service failed.");
		initialLocation = null;
    } else {
//      alert("Your browser doesn't support geolocation");
		initialLocation = null;
    }
//    map.setCenter(initialLocation);
}

function getLocation(successCallBack, failureCallBack) {
	var gps, options, geo, watch, browserSupportFlag, trackerId;
	gps = navigator.geolocation;
	options = {timeout: LOCATION_TIMEOUT, maximumAge: 330000};
	if (gps) {
		browserSupportFlag = true;
		navigator.geolocation.getCurrentPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, function () {
			handleNoGeoLocation(browserSupportFlag);
		});
	// Try Google Gears Geolocation
	} else if (google.gears) {
		browserSupportFlag = true;
		geo = google.gears.factory.create('beta.geolocation');
		geo.getCurrentPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, function () {
			handleNoGeoLocation(browserSupportFlag);
		});
	// Browser doesn't support Geolocation
	} else {
		browserSupportFlag = false;
		handleNoGeoLocation(browserSupportFlag);
	}

	if (gps !== undefined) {
		trackerId = gps.watchPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, failureCallBack, options);
	} else if (geo !== undefined) {
		watch = geo.watchPosition(function (position) {
			if (successCallBack) {
				successCallBack(position);
			}
		}, failureCallBack, options);
	}
}

function getCachedLocation(successCallBack, errorCallback) {
    // Request a position. We only accept cached positions, no matter what 
    // their age is. If the user agent does not have a cached position at
    // all, it will immediately invoke the error callback.
    navigator.geolocation.getCurrentPosition(successCallback,
                                             errorCallback,
                                             {maximumAge:Infinity, timeout:0});

    function successCallback(position) {
      // By setting the 'maximumAge' to Infinity, the position
      // object is guaranteed to be a cached one.
      // By using a 'timeout' of 0 milliseconds, if there is
      // no cached position available at all, the user agent 
      // will immediately invoke the error callback with code
      // TIMEOUT and will not initiate a new position
      // acquisition process.
      //if (position.timestamp < freshness_threshold && 
       //   position.coords.accuracy < accuracy_threshold) {
		if (position) {	
			successCallBack(position);
		} else {
        // The position is quite old and/or inaccurate.
		successCallBack(position);
      }
    }

    function errorCallback(error) {
      switch(error.code) {
        case error.TIMEOUT:
          // Quick fallback when no cached position exists at all.
          doFallback();
          // Acquire a new position object.
          navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
          break;
//        case ... // treat the other error cases.
      };
    }

    function doFallback() {
		successCallBack(null);
	
      // No cached position available at all.
      // Fallback to a default position.
    }
    
}
function geoErrorHandler(err) {

/*  if(err.code == 1) {
    dBug("geo:", "Error: Access is denied!", 5);
  }else if( err.code == 2) {
*/
	console.log('Position error' + err);
//    dBug("geo", "Error: Position is unavailable!", 5);
//  }

}

function setCookie(c_name, value, exdays) {
//	alert('localStorage.setItem ' + c_name + ' value: ' + value);
	localStorage.setItem(c_name, value);
/*	
	if (hasLocalStorage()) {
		localStorage.setItem(c_name, value);
	} else {
		var c_value, exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
		document.cookie = c_name + "=" + c_value;
	}
*/
}

function getCookie(c_name) {
//	alert('localStorage.getItem ' + c_name + ' : ' + localStorage.getItem(c_name));
	return localStorage.getItem(c_name);
/*	
	if (hasLocalStorage()) {
		return localStorage.getItem(c_name);
	} else {
		var i, x, y, ARRcookies = document.cookie.split(";");
		for (i = 0; i < ARRcookies.length; i = i + 1) {
			x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
			y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
			x = x.replace(/^\s+|\s+$/g, "");
			if (x === c_name) {
				return unescape(y);
			}
		}
	}
	return '';
*/
}
function createCookie(name,value,days) {
if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
}
else var expires = "";
document.cookie = escape(name)+"="+escape(value)+expires+"; path=/";
}
function readCookie(name) {
var nameEQ = escape(name) + "=";
var ca = document.cookie.split(';');
for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length,c.length));
}
return null;
}
function eraseCookie(name) {
createCookie(name,"",-1);
}
// distance between 2 lat, lon pairs
function distanceLatLon(lat1, lon1, lat2, lon2) {
	var R = 6371, dLat, dLon, a, c, d;
	dLat = (lat2 - lat1) * Math.PI / 180;
	dLon = (lon2 - lon1) * Math.PI / 180;
	a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
			Math.sin(dLon / 2) * Math.sin(dLon / 2);
	c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	d = R * c;
	return Math.ceil(d * 1000);	// return distance in meters
}

var DIGITHEIGHT = 28,
	DIGITWIDTH = 16.5,
	DIGITIMAGE = '../lib/ss/images/digits_small.gif',
	DECIMAL = '../lib/ss/images/decimal_small.gif',
	IMAGETRANSPARENT = '"img_trans.gif';

function getDigitalHtml(num) {
	var hr = parseInt(num, 10),
		sHr = (hr < 10 ? '0' : '') + hr,
		min = (num * 100) % 100,
		sMin = (min < 10 ? '0' : '') + min,
		digit1 = parseInt(sHr.substring(0, 1), 10),
		digit2 = parseInt(sHr.substring(1, 2), 10),
		digit3 = parseInt(sMin.substring(0, 1), 10),
		digit4 = parseInt(sMin.substring(1, 2), 10),
		fixedLeft = '<img style="width:' + DIGITWIDTH + 'px;height:' + DIGITHEIGHT +
			'px;background:url(' + DIGITIMAGE + ') -',
		fixedRight = 'px 0"' + 'src="' + IMAGETRANSPARENT + '" width="1" height="1" />',
		html = '<img style="width:' + DIGITWIDTH + 'px;height:' + DIGITHEIGHT +
			'px;background:url(' + DIGITIMAGE + ') -' + DIGITWIDTH * digit1 + 'px 0"' +
			'src="' + IMAGETRANSPARENT + '" width="1" height="1" />';
	html += fixedLeft + DIGITWIDTH * digit2 + fixedRight;
	html += '<img src="' + DECIMAL + '"/>';
	html += fixedLeft + DIGITWIDTH * digit3 + fixedRight;
	html += fixedLeft + DIGITWIDTH * digit4 + fixedRight;
	return html;
}
function shownum() {
	document.getElementById('num').innerHTML = getDigitalHtml(12.34);

}
function getAddress(lat, lon, successCallBack) {
	var addrUrl = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' +
			lat + ',' + lon + '&sensor=true_or_false';
	$.ajax({
	    type: 'GET',
	    url: addrUrl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
	        console.log(textStatus + ', ' + errorThrown);
	    },
	    async: false
	});
}
// get the hour month of a time in hh:ss format 
function getTimeHHSS(dttm) {
	var hh = dttm.getHours(),
		mm = dttm.getMinutes();
	if (mm < 10) {
		mm = '0' + mm;
	}
	return hh + ':' + mm;
}
function isTouchDevice() {
	try {
		document.createEvent("TouchEvent");
		return true;
	} catch (e) {
		return false;
	}
}
function touchScroll(id) {
	if (isTouchDevice()) { //if touch events exist...
		var /* el = document.getElementById(id), */ scrollStartPos = 0;
		document.getElementById(id).addEventListener("touchstart", function (event) {
			scrollStartPos = this.scrollTop + event.touches[0].pageY;
			event.preventDefault();
		}, false);
		document.getElementById(id).addEventListener("touchmove", function (event) {
			this.scrollTop = scrollStartPos - event.touches[0].pageY;
			event.preventDefault();
		}, false);
	}
}

function checkDeviceReady() {
    window.isphone = false;
    if (document.URL.indexOf("http://") === -1) {
        window.isphone = true;
    }
	onDeviceReady();
/*	
    if (window.isphone) {
		document.addEventListener("deviceready", function () {
			alert('deviceReady');
			onDeviceReady();
		}, true);
    } else {
        onDeviceReady();
    }
*/
}

Date.prototype.yyyymmdd = function () {
	var yyyy = this.getFullYear().toString(),
		mm = (this.getMonth() + 1).toString(), // getMonth() is zero-based
		dd = this.getDate().toString();
	return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]); // padding
};

Date.prototype.monthNameShort = function () {
	var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
	return monthNames[this.getMonth()]; // padding
};
Date.prototype.dayNameShort = function () {
	var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	return days[this.getDay()]; // padding
};

Date.prototype.AMPM = function () {
	var strTime,
		hours = this.getHours(),
		minutes = this.getMinutes(),
		ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours !== 0 ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	strTime = hours + ':' + minutes + ' ' + ampm;
	return strTime;
};

function mySqlDate(dateString) {
	var t = dateString.split(/[- :]/),
		d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
	return d;
}

function getBaseURL() {
	return location.protocol + "//" + location.hostname +
		(location.port && ":" + location.port) + "/";
}
function toast(msg, millisec) {
	var msTimeOut = 2000;
	if (millisec) {
		msTimeOut = millisec;
	}
	$.blockUI({
		message: msg,
		css: {
			border: 'none',
			padding: '15px',
			backgroundColor: '#000',
			'-webkit-border-radius': '10px',
			'-moz-border-radius': '10px',
			opacity: 0.5,
			color: '#fff'
		}
	});
	setTimeout($.unblockUI, msTimeOut);
} // toast
function dateMinutesDifference(inMinutes) {
    var minutesEarlier = new Date();
    minutesEarlier.setTime(minutesEarlier.getTime() + inMinutes * 60000);
	return minutesEarlier.getFullYear() + '-' + (minutesEarlier.getMonth() + 1) + '-' +
				minutesEarlier.getDate() + ' ' + minutesEarlier.getHours() + ":" +
				minutesEarlier.getMinutes() + ":" + minutesEarlier.getSeconds();
}
// Check if browser supports localstorage
function hasLocalStorage() {
    var mod = 'modernizr';
    try {
        localStorage.setItem(mod, mod);
        localStorage.removeItem(mod);
        return true;
    } catch (e) {
        return false;
    }
} // hasLocalStorage
function loadScript(scriptName, successCallBack, failureCallBack) {
	var head, script;
	head = document.getElementsByTagName("head")[0];
	script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = scriptName;
	script.onload = function () {
		console.log("loaded: " + scriptName);
		//alert("loaded " + scriptName);
		if (successCallBack) {
			successCallBack();
		}
	};
	script.onerror = function (e) {
		console.log("failed to load: " + scriptName);
		if (failureCallBack) {
			failureCallBack();
		}
	};
	head.appendChild(script);
} // loadScript

function plotColorMarker(map, latlon, color, ttl) {
	if (!ttl) {
		ttl = '';
	}
	var pinColor = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png', 
		markerColor = color.toUpperCase();
	if (markerColor === "BLUE") {
		pinColor = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
	} else if (markerColor === "PURPLE") {
		pinColor = 'http://maps.google.com/mapfiles/ms/icons/purple-dot.png';
	} else if (markerColor === "YELLOW") {
		pinColor = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
	} else if (markerColor === "GREEN") {
		pinColor = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
	}
	var pinImage = new google.maps.MarkerImage(pinColor);

	var marker = new google.maps.Marker({
			position: latlon,
			icon: pinImage,
			map: map,
			title: ttl
		});
}

function libInit() {
	if (!window.console) console = {log: function() {}};
}
