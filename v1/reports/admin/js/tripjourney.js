/*global $, jQuery, gup, console, getWalkWithmeLoc, document, google, setTimeout */
//"use strict";
var map, infowindow, marker, trip_id = gup("tripid"),
    TRAVELSPEED = 10,
    triptype = gup("triptype"),
    appuserid = gup("appuserid"),
    current_index = -1,
    countrycode, phoneno, present_index = 1,
    RED = '#FF0000',
    BLUE = '#0000FF',
    firstTime = true,
    latitude, longitude,
    accuracy = 0,
    centerPosition, initialLat = 19.05856,
    initialLng = 72.8565,
    bounds = new google.maps.LatLngBounds();
var directionsService = new google.maps.DirectionsService();
var directionsDisplay = new google.maps.DirectionsRenderer();
/*
function getTripPath(trip_id, successCallBack) {
	successCallBack(null);
	return;
	var walkwithmeurl = getPhpScriptsPath() + 
	"get_simulation_location_sar_apk.php?tripid=" + trip_id;
	$.ajax({
	    type: 'GET',
	    url: walkwithmeurl,
	    dataType: 'json',
	    success: function (data) {
	        successCallBack(data);
	    },
	    error: function (xhr, textStatus, errorThrown) {
			console.log(textStatus + ', ' + errorThrown);
		}
		
	});
}
function displayTripPath() {
	getTripPath(trip_id, triptype, appuserid,  function (aoTripLoc) {
		var len = aoTripLoc.length, row, html = "", options;
		if (len === 0) {
			console.log("No Friend's Location...");
		}
		latitude = aoTripLoc[0].lat;
		longitude = aoTripLoc[0].lng;
		centerPosition = new google.maps.LatLng(latitude, longitude);
		plotTrip(aoTripLoc, RED);
	});
}*/
function getJourneyPath(trip_id, triptype, appuserid, successCallBack) {
    var journeyurl = getPhpScriptsPath() +
        "get_journey.php?tripid=" + trip_id + "&triptype=" + triptype + "&appuserid=" + appuserid;
    $.ajax({
        type: 'GET',
        url: journeyurl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus + ', ' + errorThrown);
        }
    });
}

function displayJourneyPath() {
    getJourneyPath(trip_id, triptype, appuserid, function(aoJourney) {
        var len = aoJourney.length,
            row, html = "",
            options;
        if (len === 0) {
            console.log("No Journey Location...");
            //return;
        }
        plotTrip(aoJourney, BLUE);
    });

}

function getCurrentPosition(appuserid, successCallBack) {
    var getPositionurl = getPhpScriptsPath() +
        "get_position_sar_apk.php?appuserid=" + appuserid;
    $.ajax({
        type: 'GET',
        url: getPositionurl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus + ', ' + errorThrown);
        }
    });
}

function getPosition(appuserid) {
    getCurrentPosition(appuserid, function(aoPosition) {
        var locationId, lat, lng, centerPosition;
        locationId = aoPosition.location_id;
        lat = aoPosition.lat;
        lng = aoPosition.lng;
        locationId = parseInt(locationId);
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        centerPosition = new google.maps.LatLng(lat, lng);
        marker = new google.maps.Marker({
            position: centerPosition,
            map: map
        });
        console.log("Got Current Position");
    });
}

function initialize() {
    trip_id = gup("tripid");
    triptype = gup("triptype");
    appuserid = gup("appuserid");
    delay = gup('delay');
    appuserid = gup('appuserid');
    delay = delay * 1000;
    countrycode = gup('countrycode');
    phoneno = gup('phoneno');
    var lat = initialLat,
        lng = initialLng;
    centerPosition = new google.maps.LatLng(lat, lng);
    options = {
        zoom: 16,
        center: centerPosition,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map($('#map')[0], options);
    //	displayTripPath();
    displayJourneyPath();
    //getPosition(appuserid);
}
$(document).ready(function() {
    google.maps.event.addDomListener(window, 'load', initialize);
});

function plotTrip(aoTripLoc, color) {
    var tripPathCoordinates = [];
    len = aoTripLoc.length,
        stroke = 0;
    if (color == BLUE) {
        stroke = 2;
    }
    if (color == RED) {
        stroke = 5;
    }
    var infoWindowContent = [];
    var distTraveled = 0;
    var prevLat, prevLng;
    for (i = 0; i < len; i++) {
        if (aoTripLoc[i].accuracy > 100) {
            continue;
        }

        if (i > 0) {
            distTraveled = distTraveled + getDistanceFromLatLonInKm(aoTripLoc[i].lat,
                aoTripLoc[i].lng, prevLat, prevLng);
				console.log(i + ". (" + aoTripLoc[i].lat + ", " +
                aoTripLoc[i].lng  + ", " + prevLat + ", " + prevLng + ") = " + distTraveled);
        }
        prevLat = aoTripLoc[i].lat;
        prevLng = aoTripLoc[i].lng;
        infoWindowContent[i] = getInfoWindowDetails(aoTripLoc[i]);
        tripPathCoordinates[tripPathCoordinates.length] =
            centerPosition = new google.maps.LatLng(aoTripLoc[i].lat, aoTripLoc[i].lng);
        var contentString = i + ". lat: " + aoTripLoc[i].lat + ", lng: " + aoTripLoc[i].lng;
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        var titleString = i + ". lat: " + aoTripLoc[i].lat + ", lng: " + aoTripLoc[i].lng +
            ", a: " + aoTripLoc[i].accuracy + ", s: " + aoTripLoc[i].speed +
            ", p: " + aoTripLoc[i].provider + ", lt: " + aoTripLoc[i].loctime +
            ", cdt: " + aoTripLoc[i].clientaccessdatetime;
        var marker = new google.maps.Marker({
            position: centerPosition,
            map: map,
            title: titleString
        });
        /*
		  google.maps.event.addListener(marker, 'click', (function(marker,i){
            return function(){
                infoWindow.setContent(infoWindowContent[index]);
                infoWindow.open(map, marker);
                map.setCenter(marker.getPosition());
                map.setZoom(15);
            }
        })(marker,i));
		*/
        bounds.extend(centerPosition);
    }
    alert("Dist: " + distTraveled + " km.");
    var tripPath = new google.maps.Polyline({
        path: tripPathCoordinates,
        geodesic: true,
        strokeColor: color,
        strokeOpacity: 1.0,
        strokeWeight: stroke
    });
    map.fitBounds(bounds);
    tripPath.setMap(map);
}

function getInfoWindowDetails(location) {
    var contentString = '<div id="content" style="width:270px;height:100px">' +
        '<h3 id="firstHeading" class="firstHeading">' + location.title + '</h3>' +
        '<div id="bodyContent">' +
        '<div style="float:left;width:100%">' + location.address + '</div>' +
        '</div>' +
        '</div>';
    return contentString;
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}