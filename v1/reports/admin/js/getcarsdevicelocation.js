var map, gMarkers = [];

function initMap() {
    var myLatLng = {
        lat: 19.104,
        lng: 72.85
    };

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: myLatLng
    });

    window.setInterval(function() {
        showVehicles();
    }, 5000);

}


function getVehicleLocation(successCallBack) {
    var locationurl = getPhpScriptsPath() +
        "get_cars_device_location.php";
    $.ajax({
        type: 'GET',
        url: locationurl,
        dataType: 'json',
        success: function(data) {
            successCallBack(data);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(textStatus + ', ' + errorThrown);
        }
    });
}

function showVehicles() {
    var bounds = new google.maps.LatLngBounds(),
        markerLatLng;


    getVehicleLocation(function(aoVehicleLoc) {
        var len = aoVehicleLoc.length,
            row, html = "",
            options, i, iconColor;
        removeMarkers();
        for (i = 0; i < len; i++) {
            markerLatLng = new google.maps.LatLng(aoVehicleLoc[i].lat, aoVehicleLoc[i].lng);
            iconColor = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
            if (aoVehicleLoc[i].has_device == 1) {
                if (aoVehicleLoc[i].is_login == 1) {
                    iconColor = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
                }
            } else {
                if (aoVehicleLoc[i].is_login == 1) {
                    iconColor = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
                }
            }
            var marker = new google.maps.Marker({
                position: markerLatLng,
                map: map,
                icon: iconColor,
                title: aoVehicleLoc[i].service_code + " - " + aoVehicleLoc[i].vehicleno
            });
            gMarkers.push(marker);
            bounds.extend(markerLatLng);
            map.fitBounds(bounds);
        }

    });
}

function removeMarkers() {
    for (i = 0; i < gMarkers.length; i++) {
        gMarkers[i].setMap(null);
    }
}